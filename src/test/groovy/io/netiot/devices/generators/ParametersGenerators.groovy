package io.netiot.devices.generators

import com.google.gson.Gson
import io.netiot.devices.entities.Parameters


class ParametersGenerators {

    static aParameters(Map overrides = [:]) {
        Map values = [
                "param1" : "value1"
        ]
        values << overrides
        return Parameters.builder()
                .parametersMap(values).build()
    }

    static aParametersJson(Map overrides = [:]) {
        def gson = new Gson()
        def parametersMap = aParameters(overrides).parametersMap
        return gson.toJson(parametersMap)
    }

}
