package io.netiot.devices.generators

import io.netiot.devices.models.DataSampleModel

class SensorDataModelGenerator {
    static aSensorDataModel(Map overrides = [:]) {
        Map values = [
                id: 1,
                timestamp: System.currentTimeMillis(),
                timestampR: System.currentTimeMillis(),
                values: "test"
        ]
        values << overrides
        return DataSampleModel.newInstance(values)
    }
}
