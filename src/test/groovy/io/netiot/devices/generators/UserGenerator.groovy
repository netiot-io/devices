package io.netiot.devices.generators

import io.netiot.devices.entities.User
import io.netiot.devices.models.UserInternalModel

class UserGenerator {

    static aUser(Map overrides = [:]) {
        Map values = [
                id            : 1L,
        ]
        values << overrides
        return User.newInstance(values)
    }

    static aUserInternalModel(Map overrides = [:]) {
        Map values = [
                id: 1L
        ]
        values << overrides
        return UserInternalModel.newInstance(values)
    }

}
