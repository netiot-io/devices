package io.netiot.devices.generators

import io.netiot.devices.entities.Decoder
import io.netiot.devices.models.DecoderListModel
import io.netiot.devices.models.DecoderModel

import static io.netiot.devices.generators.ProtocolGenerator.aProtocol

class DecoderGenerator {

    static aDecoder(Map overrides = [:]) {
        Map values = [
                id               : 1L,
                name             : "decoderName",
                description      : "description",
                fields           : ["field1", "field2", "field3"],
                protocol         : aProtocol(),
                code             : "jscode const fields = [\"field1\", \"field2\", \"field3\"];",
                definedByPlatform: Boolean.FALSE,
                organizationId   : 1L
        ]
        values << overrides
        return Decoder.newInstance(values)
    }

    static aDecoderModel(Map overrides = [:]) {
        Map values = [
                id               : 1L,
                name             : "decoderName",
                description      : "description",
                protocolId       : 1L,
                code             : "jscode const fields = [\"field1\", \"field2\", \"field3\"];",
                definedByPlatform: Boolean.FALSE,
                organizationId   : 1L
        ]
        values << overrides
        return DecoderModel.newInstance(values)
    }

    static aDecoderListModel(Map overrides = [:]) {
        Map values = [
                decoders : [ aDecoderModel() ]
        ]
        values << overrides
        return DecoderListModel.newInstance(values)
    }

}
