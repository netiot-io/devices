package io.netiot.devices.generators

import io.netiot.devices.entities.Protocol
import io.netiot.devices.models.ProtocolListModel
import io.netiot.devices.models.ProtocolModel

class ProtocolGenerator {

    static aProtocol(Map overrides = [:]) {
        Map values = [
                id          : 1L,
                name        : "protocol",
                gatewayTopic: "topicProtocol"
        ]
        values << overrides
        return Protocol.newInstance(values)
    }

    static aProtocolModel(Map overrides = [:]) {
        Map values = [
                id  : 1L,
                name: "protocol"
        ]
        values << overrides
        return ProtocolModel.newInstance(values)
    }

    static aProtocolListModel(Map overrides = [:]) {
        Map values = [
                protocols: [aProtocolModel()]
        ]
        values << overrides
        return ProtocolListModel.newInstance(values)
    }

}
