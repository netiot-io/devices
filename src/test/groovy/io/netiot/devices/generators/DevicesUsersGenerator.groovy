package io.netiot.devices.generators


import io.netiot.devices.entities.DevicesUsers

class DevicesUsersGenerator {

    static aDevicesUsers(Map overrides = [:]) {
        Map values = [
                id : null,
                userId: 1L
        ]
        values << overrides
        return DevicesUsers.newInstance(values)
    }
}
