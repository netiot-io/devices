package io.netiot.devices.generators

import io.netiot.devices.entities.DeviceType
import io.netiot.devices.models.DeviceTypeListModel
import io.netiot.devices.models.DeviceTypeModel

import static io.netiot.devices.generators.DecoderGenerator.aDecoder
import static io.netiot.devices.generators.ProtocolGenerator.aProtocol

class DeviceTypeGenerator {

    static aDeviceType(Map overrides = [:]) {
        Map values = [
                id : 1L,
                name : "deviceTypeName",
                description : "description",
                protocol : aProtocol(),
                decoder : aDecoder(),
                definedByPlatform : Boolean.FALSE,
                organizationId: 1L,
                activate: Boolean.FALSE
        ]
        values << overrides
        return DeviceType.newInstance(values)
    }

    static aDeviceTypeModel(Map overrides = [:]) {
        Map values = [
                id : 1L,
                name : "deviceTypeName",
                description : "description",
                protocolId : aProtocol().getId(),
                decoderId : aDecoder().getId(),
                definedByPlatform : Boolean.FALSE,
                organizationId: 1L,
                activate: Boolean.FALSE
        ]
        values << overrides
        return DeviceTypeModel.newInstance(values)
    }

    static aDeviceTypeListModel(Map overrides = [:]) {
        Map values = [
                deviceTypes : [ aDeviceTypeModel() ]
        ]
        values << overrides
        return DeviceTypeListModel.newInstance(values)
    }

}
