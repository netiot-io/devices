package io.netiot.devices.generators

import io.netiot.devices.entities.Decoder
import io.netiot.devices.entities.Profile
import io.netiot.devices.models.DecoderListModel
import io.netiot.devices.models.DecoderModel
import io.netiot.devices.models.ProfileListModel
import io.netiot.devices.models.ProfileModel

import static io.netiot.devices.generators.DeviceTypeGenerator.aDeviceType
import static io.netiot.devices.generators.ProtocolGenerator.aProtocol

class ProfileGenerator {

    static aProfile(Map overrides = [:]) {
        Map values = [
                id : 1L,
                name: "profileName",
                parameters: [ param : "param"],
                deviceType: aDeviceType(),
                organizationId: 1L
        ]
        values << overrides
        return Profile.newInstance(values)
    }

    static aProfileModel(Map overrides = [:]) {
        Map values = [
                id : 1L,
                name: "profileName",
                protocolParameters: [ param : "param"],
                organizationId: 1L
        ]
        values << overrides
        return ProfileModel.newInstance(values)
    }

    static aProfileListModel(Map overrides = [:]) {
        Map values = [
                profiles : [ aProfileModel() ]
        ]
        values << overrides
        return ProfileListModel.newInstance(values)
    }

}
