package io.netiot.devices.generators

import io.netiot.devices.entities.Alert
import io.netiot.devices.models.AlertModel
import io.netiot.devices.models.events.AlertEvent

import java.time.Instant
import java.time.ZoneId

class AlertGenerator {

    static def timestampInMillis = 1550689185418

    static aAlertEvent(Map overrides = [:]) {
        Map values = [
                deviceId : 1L,
                code: "code",
                timestamp: timestampInMillis
        ]
        values << overrides
        return AlertEvent.newInstance(values)
    }

    static aAlert(Map overrides = [:]) {
        def timestamp = Instant.ofEpochMilli(timestampInMillis)
                .atZone(ZoneId.systemDefault()).toLocalDateTime()
        Map values = [
                id: 1L,
                deviceId : 1L,
                code: "code",
                timestamp: timestamp
        ]
        values << overrides
        return Alert.newInstance(values)
    }

    static aAlertModel(Map overrides = [:]) {
        def timestamp = Instant.ofEpochMilli(aAlertEvent().getTimestamp())
                .atZone(ZoneId.systemDefault()).toLocalDateTime()
        Map values = [
                id: 1L,
                deviceId : 1L,
                code: "code",
                timestamp: timestamp
        ]
        values << overrides
        return AlertModel.newInstance(values)
    }

}
