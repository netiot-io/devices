package io.netiot.devices.generators

import io.netiot.devices.entities.Device
import io.netiot.devices.models.DeviceApplicationModel
import io.netiot.devices.models.events.DeviceEvent
import io.netiot.devices.models.DeviceListModel
import io.netiot.devices.models.DeviceModel
import io.netiot.devices.entities.DeviceProperties

import io.netiot.devices.models.events.EventType
import io.netiot.devices.models.events.StatusEventType

import static io.netiot.devices.generators.DeviceTypeGenerator.aDeviceType
import static io.netiot.devices.generators.ProfileGenerator.aProfile
import static io.netiot.devices.generators.DevicesUsersGenerator.aDevicesUsers

class DeviceGenerator {

    static aDevice(Map overrides = [:]) {
        Map values = [
                id : 1L,
                name: "deviceName",
                parameters: [ param : "param"],
                deviceType: aDeviceType(),
                profile: aProfile(),
                activate : Boolean.TRUE,
                users: [aDevicesUsers(), aDevicesUsers(userId: 2L)]
        ]
        values << overrides
        return Device.newInstance(values)
    }

    static aDeviceModel(Map overrides = [:]) {
        Map values = [
                id : 1L,
                name: "deviceName",
                deviceParameters: [ param : "param"],
                deviceTypeId : aDeviceType().getId(),
                profileId: aProfile().getId(),
                activate : Boolean.TRUE,
                userIds: [1L, 2L]
        ]
        values << overrides
        return DeviceModel.newInstance(values)
    }

    static aDeviceListModel(Map overrides = [:]) {
        Map values = [
                devices : [ aDeviceModel() ]
        ]
        values << overrides
        return DeviceListModel.newInstance(values)
    }

    static aDeviceProperties(Map overrides = [:]) {
        Map values = [
                decoderCode : aDevice().getDeviceType().getDecoder().getCode()
        ]
        values << overrides
        return DeviceProperties.newInstance(values)
    }


    static aDeviceEvent(Map overrides = [:]) {
        Map values = [
                id : aDevice().getId(),
                eventType : EventType.CREATED,
                statusEventType: StatusEventType.ACTIVE,
                parameters: aDevice().getProfile().getParameters().getParametersMap() +
                        aDevice().getParameters().getParametersMap()
        ]
        values << overrides
        return DeviceEvent.newInstance(values)
    }

    static aDeviceApplicationModel(Map overrides = [:]) {
        Map values = [
                id : 1L,
                name: "deviceName",
                decoderFields: aDeviceType().getDecoder().getFields(),
                deviceTypeId : aDeviceType().getId(),
                deviceTypeName : aDeviceType().getName(),
        ]
        values << overrides
        return DeviceApplicationModel.newInstance(values)
    }

}
