package io.netiot.devices.UT.converters

import io.netiot.devices.converters.DecoderFieldsConverter
import spock.lang.Specification

class DecoderFieldsConverterSpec extends Specification {

    def decodeFieldsConverter = new DecoderFieldsConverter()

    def 'convertToDatabaseColumn'(){
        given:
        def fieldsList = [ "field1", "field2", "field3"]
        def expectedResult = "field1,field2,field3"

        when:
        def result = decodeFieldsConverter.convertToDatabaseColumn(fieldsList)

        then:
        result == expectedResult
    }

    def 'convertToEntityAttribute'(){
        given:
        def fieldsConcatenatedString = "field1,field2,field3"
        def expectedResult = [ "field1", "field2", "field3"]

        when:
        def result = decodeFieldsConverter.convertToEntityAttribute(fieldsConcatenatedString)

        then:
        result == expectedResult
    }

}
