package io.netiot.devices.UT.converters

import io.netiot.devices.converters.ParametersConverter
import spock.lang.Specification

import static io.netiot.devices.generators.ParametersGenerators.aParameters
import static io.netiot.devices.generators.ParametersGenerators.aParametersJson

class ParametersConverterSpec extends Specification {

    def parametersConverter = new ParametersConverter()

    def 'convertToDatabaseColumn'(){
        given:
        def parametersJson = aParametersJson()
        def parametersObject = aParameters()

        when:
        def result = parametersConverter.convertToDatabaseColumn(parametersObject)

        then:
        result == parametersJson
    }

    def 'convertToEntityAttribute'(){
        given:
        def parametersJson = aParametersJson()
        def parametersObject = aParameters()

        when:
        def result = parametersConverter.convertToEntityAttribute(parametersJson)

        then:
        result == parametersObject
    }

}
