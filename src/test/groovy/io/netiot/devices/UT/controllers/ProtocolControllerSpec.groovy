package io.netiot.devices.UT.controllers

import io.netiot.devices.controllers.ProtocolController
import io.netiot.devices.services.ProtocolService
import org.springframework.http.ResponseEntity
import spock.lang.Specification

import static io.netiot.devices.generators.ProtocolGenerator.aProtocolListModel
import static io.netiot.devices.generators.ProtocolGenerator.aProtocolModel

class ProtocolControllerSpec extends Specification {

    def protocolService = Mock(ProtocolService)
    def protocolController = new ProtocolController(protocolService)

    def 'getAllProtocols'() {
        given:
        def protocolModels = [aProtocolModel()]
        def protocolListModel = aProtocolListModel()

        when:
        def result = protocolController.getProtocols()

        then:
        1 * protocolService.getProtocols() >> protocolModels
        0 * _
        result == ResponseEntity.ok(protocolListModel)
    }

}
