package io.netiot.devices.UT.controllers

import io.netiot.devices.controllers.AlertController
import io.netiot.devices.services.AlertService
import org.springframework.http.ResponseEntity
import spock.lang.Specification

import static io.netiot.devices.generators.AlertGenerator.aAlertModel

class AlertControllerSpec extends Specification {

    def alertService = Mock(AlertService)

    def alertController = new AlertController(alertService)

    def 'getAlerts'(){
        given:
        def deviceId = 1L
        def alertModels = [ aAlertModel() ]

        when:
        def result = alertController.getAlerts(deviceId)

        then:
        1 * alertService.getAlerts(deviceId) >> alertModels
        0 * _
        result == ResponseEntity.ok(alertModels)
    }

    def 'getLast10Alerts'(){
        given:
        def deviceId = 1L
        def alertModels = [ aAlertModel() ]

        when:
        def result = alertController.getLast10Alerts(deviceId)

        then:
        1 * alertService.getLast10Alerts(deviceId) >> alertModels
        0 * _
        result == ResponseEntity.ok(alertModels)
    }
}
