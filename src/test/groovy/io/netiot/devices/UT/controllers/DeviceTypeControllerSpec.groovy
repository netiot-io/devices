package io.netiot.devices.UT.controllers

import io.netiot.devices.controllers.DeviceTypeController
import io.netiot.devices.services.DeviceTypeService
import io.netiot.devices.validators.ValidatorUtil
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import spock.lang.Specification
import spock.lang.Subject

import static io.netiot.devices.generators.DecoderGenerator.aDecoder
import static io.netiot.devices.generators.DeviceTypeGenerator.aDeviceType
import static io.netiot.devices.generators.DeviceTypeGenerator.aDeviceTypeListModel
import static io.netiot.devices.generators.DeviceTypeGenerator.aDeviceTypeModel
import static io.netiot.devices.generators.ProfileGenerator.aProfileListModel
import static io.netiot.devices.generators.ProfileGenerator.aProfileModel

class DeviceTypeControllerSpec extends Specification {

    def bindingResult = Mock(BindingResult)
    def deviceTypeService = Mock(DeviceTypeService)
    def validatorUtil = Mock(ValidatorUtil)

    @Subject
    def deviceTypeController = new DeviceTypeController(deviceTypeService, validatorUtil)

    def 'getDeviceType'() {
        given:
        def deviceTypeModel = aDeviceTypeModel()
        def deviceTypeId = deviceTypeModel.getId()

        when:
        def result = deviceTypeController.getDeviceType(deviceTypeId)

        then:
        1 * deviceTypeService.getDeviceType(deviceTypeId) >> deviceTypeModel
        0 * _

        result == ResponseEntity.ok(deviceTypeModel)
    }

    def 'getDeviceTypes'() {
        given:
        def decoder = aDecoder()
        def protocol = decoder.getProtocol()
        def protocolId = protocol.getId()
        def deviceTypeModels = [ aDeviceTypeModel() ]
        def deviceTypeListModel = aDeviceTypeListModel()

        when:
        def result = deviceTypeController.getDeviceTypes(protocolId)

        then:
        1 * deviceTypeService.getDeviceTypes(protocolId) >> deviceTypeModels
        0 * _
        result == ResponseEntity.ok(deviceTypeListModel)
    }

    def 'saveDeviceTypes'() {
        given:
        def deviceTypeModel = aDeviceTypeModel()

        when:
        def result = deviceTypeController.saveDeviceType(deviceTypeModel, bindingResult)

        then:
        1 * validatorUtil.checkBindingResultErrors(bindingResult)
        1 * deviceTypeService.saveDeviceType(deviceTypeModel) >> deviceTypeModel
        0 * _
        result == ResponseEntity.ok([ "id" : deviceTypeModel.getId()])
    }

    def 'updateDeviceTypes'() {
        given:
        def deviceTypeId = 1L
        def deviceTypeModel = aDeviceTypeModel()

        when:
        def result = deviceTypeController.updateDeviceType(deviceTypeId, deviceTypeModel, bindingResult)

        then:
        1 * validatorUtil.checkBindingResultErrors(bindingResult)
        1 * deviceTypeService.updateDeviceType(deviceTypeId, deviceTypeModel)
        0 * _
        result == ResponseEntity.ok().build()
    }

    def 'deleteDeviceType'() {
        given:
        def deviceTypeId =1L

        when:
        def result = deviceTypeController.deleteDeviceType(deviceTypeId)

        then:
        1 * deviceTypeService.deleteDeviceType(deviceTypeId)
        0 * _

        result == ResponseEntity.ok().build()
    }

    def 'getDeviceTypeProfiles'() {
        given:
        def deviceTypeId = 1L
        def profileModel = aProfileModel()
        def profileModels = [ profileModel ]
        def profileListModel = aProfileListModel()


        when:
        def result = deviceTypeController.getDeviceTypeProfiles(deviceTypeId)

        then:
        1 * deviceTypeService.getDeviceTypeProfiles(deviceTypeId) >> profileModels
        0 * _

        result == ResponseEntity.ok(profileListModel)
    }

    def 'getDeviceTypeProfile'() {
        given:
        def deviceTypeId = 1L
        def profileModel = aProfileModel()
        def profileModelId = profileModel.getId()

        when:
        def result = deviceTypeController.getDeviceTypeProfile(deviceTypeId, profileModelId)

        then:
        1 * deviceTypeService.getDeviceTypeProfile(deviceTypeId, profileModelId) >> profileModel
        0 * _

        result == ResponseEntity.ok(profileModel)
    }

    def 'saveDeviceTypeProfile'() {
        given:
        def deviceTypeId = 1L
        def profileModel = aProfileModel()

        when:
        def result = deviceTypeController.saveDeviceTypeProfile(deviceTypeId, profileModel, bindingResult)

        then:
        1 * validatorUtil.checkBindingResultErrors(bindingResult)
        1 * deviceTypeService.saveDeviceTypeProfile(deviceTypeId, profileModel) >> profileModel
        0 * _

        result == ResponseEntity.ok([ "id" : profileModel.getId()])
    }

    def 'updateDeviceTypeProfile'() {
        given:
        def deviceTypeId = 1L
        def profileModel = aProfileModel()
        def profileModelId = profileModel.getId()

        when:
        def result = deviceTypeController.updateDeviceTypeProfile(deviceTypeId, profileModelId, profileModel, bindingResult)

        then:
        1 * validatorUtil.checkBindingResultErrors(bindingResult)
        1 * deviceTypeService.updateDeviceTypeProfile(deviceTypeId, profileModelId, profileModel)
        0 * _

        result == ResponseEntity.ok().build()
    }

    def 'deleteDeviceTypeProfile'() {
        given:
        def deviceTypeId = 1L
        def profileModel = aProfileModel()
        def profileModelId = profileModel.getId()

        when:
        def result = deviceTypeController.deleteDeviceTypeProfile(deviceTypeId, profileModelId)

        then:
        1 * deviceTypeService.deleteDeviceTypeProfile(deviceTypeId, profileModelId)
        0 * _

        result == ResponseEntity.ok().build()
    }

}
