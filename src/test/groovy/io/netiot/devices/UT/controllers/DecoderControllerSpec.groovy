package io.netiot.devices.UT.controllers

import io.netiot.devices.controllers.DecoderController
import io.netiot.devices.services.DecoderService
import io.netiot.devices.validators.ValidatorUtil
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import spock.lang.Specification
import spock.lang.Unroll

import static io.netiot.devices.generators.DecoderGenerator.aDecoderListModel
import static io.netiot.devices.generators.DecoderGenerator.aDecoderModel

class DecoderControllerSpec extends Specification {

    def bindingResult = Mock(BindingResult)
    def decoderService = Mock(DecoderService)
    def validatorUtil = Mock(ValidatorUtil)
    def decoderController = new DecoderController(decoderService, validatorUtil)

    @Unroll
    def 'getDecoders'(){
        given:
        def protocolId = 1L
        def decoderModels = [ aDecoderModel()]
        def decoderListModels = aDecoderListModel()

        when:
        def result = decoderController.getDecoders(optionalProtocolId)

        then:
        callGetDecoder * decoderService.getDecoders() >> decoderModels
        callGetDecodersWithProtocolId * decoderService.getDecoders(protocolId) >> decoderModels
        0 * _
        result == ResponseEntity.ok(decoderListModels)

        where:
        optionalProtocolId | callGetDecodersWithProtocolId | callGetDecoder
        Optional.of(1L)    | 1                             | 0
        Optional.empty()   | 0                             | 1
    }

    def 'saveDecoder'(){
        given:
        def decoderModel = aDecoderModel()

        when:
        def result = decoderController.saveDecoder(decoderModel, bindingResult)

        then:
        1 * validatorUtil.checkBindingResultErrors(bindingResult)
        1 * decoderService.saveDecoder(decoderModel) >> decoderModel
        0 * _

        result == ResponseEntity.ok([ "decoder_id" : decoderModel.getId()])
    }

    def 'updateDecoder'(){
        given:
        def decoderModel = aDecoderModel()
        def decoderId = decoderModel.getId()

        when:
        def result = decoderController.updateDecoder(decoderId, decoderModel, bindingResult)

        then:
        1 * validatorUtil.checkBindingResultErrors(bindingResult)
        1 * decoderService.updateDecoder(decoderId, decoderModel)
        0 * _

        result == ResponseEntity.ok().build()
    }

    def 'deleteDecoder'(){
        given:
        def decoderId =1L

        when:
        def result = decoderController.deleteDecoder(decoderId)

        then:
        1 * decoderService.deleteDecoder(decoderId)
        0 * _

        result == ResponseEntity.ok().build()
    }

    def 'getDecoder'(){
        given:
        def decoderId =1L
        def decoderModel = aDecoderModel()

        when:
        def result = decoderController.getDecoder(decoderId)

        then:
        1 * decoderService.getDecoder(decoderId) >> decoderModel
        0 * _

        result == ResponseEntity.ok(decoderModel)
    }

}
