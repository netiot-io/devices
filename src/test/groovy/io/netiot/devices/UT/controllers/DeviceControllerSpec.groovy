package io.netiot.devices.UT.controllers

import io.netiot.devices.controllers.DeviceController
import io.netiot.devices.feign.IOServerEndpoints
import io.netiot.devices.generators.SensorDataModelGenerator
import io.netiot.devices.models.events.AlertEvent
import io.netiot.devices.services.AlertService
import io.netiot.devices.services.DeviceService
import io.netiot.devices.validators.ValidatorUtil
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import spock.lang.Specification

import static io.netiot.devices.generators.DeviceGenerator.*

class DeviceControllerSpec extends Specification {

    def bindingResult = Mock(BindingResult)
    def deviceService = Mock(DeviceService)
    def alertService = Mock(AlertService)
    def validatorUtil = Mock(ValidatorUtil)
    def ioServerEndpoints = Mock(IOServerEndpoints)

    def deviceController = new DeviceController(deviceService, alertService, validatorUtil, ioServerEndpoints)

    def 'getDevices'() {
        given:
        def deviceModel = aDeviceModel()
        def deviceModelList = [ deviceModel ]
        def deviceListModel = aDeviceListModel()

        when:
        def response = deviceController.getDevices()

        then:
        1 * deviceService.getDevices() >> deviceModelList
        0 * _
        response == ResponseEntity.ok(deviceListModel)
    }

    def 'getDevice'() {
        given:
        def deviceId = 1L
        def deviceModel = aDeviceModel()

        when:
        def response = deviceController.getDevice(deviceId)

        then:
        1 * deviceService.getDevice(deviceId) >> deviceModel
        0 * _
        response == ResponseEntity.ok(deviceModel)
    }

    def 'saveDevice'() {
        given:
        def deviceModel = aDeviceModel()

        when:
        def response = deviceController.saveDevice(deviceModel, bindingResult)

        then:
        1 * validatorUtil.checkBindingResultErrors(bindingResult)
        1 * deviceService.saveDevice(deviceModel) >> deviceModel
        1 * alertService.save(_ as AlertEvent)
        0 * _
        response == ResponseEntity.ok([id : deviceModel.getId()])
    }

    def 'updateDevice'() {
        given:
        def deviceId = 1L
        def deviceModel = aDeviceModel()
        when:
        def response = deviceController.updateDevice(deviceId, deviceModel, bindingResult)

        then:
        1 * validatorUtil.checkBindingResultErrors(bindingResult)
        1 * deviceService.getDevice(deviceId) >> deviceModel
        1 * deviceService.updateDevice(deviceId, deviceModel)
        0 * _
        response == ResponseEntity.ok().build()
    }

    def 'updateDevice change activated'() {
        given:
        def deviceId = 1L
        def deviceModel = aDeviceModel()
        def newDeviceModel = aDeviceModel(activate: Boolean.FALSE)
        when:
        def response = deviceController.updateDevice(deviceId, newDeviceModel, bindingResult)

        then:
        1 * validatorUtil.checkBindingResultErrors(bindingResult)
        1 * deviceService.getDevice(deviceId) >> deviceModel
        1 * deviceService.updateDevice(deviceId, newDeviceModel)
        1 * alertService.save(_ as AlertEvent)
        0 * _
        response == ResponseEntity.ok().build()
    }

    def 'deleteDevice'() {
        given:
        def deviceId = 1L

        when:
        def response = deviceController.deleteDevice(deviceId)

        then:
        1 * deviceService.deleteDevice(deviceId)
        0 * _
        response == ResponseEntity.ok().build()
    }

    def 'getDeviceData' () {
        given:
            def deviceId = 1L
            def millisFrom = 0L
            def millisTo = System.currentTimeMillis()
            def sensorDataModel = SensorDataModelGenerator.aSensorDataModel()

        when:
            def response = deviceController.getDeviceData(deviceId, millisFrom, millisTo)

        then:
            1 * ioServerEndpoints.getDeviceData(deviceId, millisFrom, millisTo) >> [sensorDataModel]
            0 * _

            response == ResponseEntity.ok([sensorDataModel])
    }

    def 'getDeviceDataCount' () {
        given:
        def deviceId = 1L
        def deviceDataCount = 0L

        when:
        def response = deviceController.getDeviceDataCount(deviceId)

        then:
        1 * ioServerEndpoints.getDeviceDataCount(deviceId) >> deviceDataCount
        0 * _

        response == ResponseEntity.ok(deviceDataCount)
    }

    def 'getDeviceName'(){
        given:
        def deviceId = 1L
        def deviceModels = aDeviceApplicationModel().name

        when:
        def response = deviceController.getDeviceName(deviceId)

        then:
        1 * deviceService.getDeviceName(deviceId) >> deviceModels
        0 * _
        response == ResponseEntity.ok(deviceModels)
    }

}
