package io.netiot.devices.UT.repositories

import com.google.gson.Gson
import io.netiot.devices.repositories.DeviceRedisRepository
import org.springframework.data.redis.core.HashOperations
import org.springframework.data.redis.core.RedisTemplate
import spock.lang.Specification

import static io.netiot.devices.generators.DeviceGenerator.aDevice
import static io.netiot.devices.generators.DeviceGenerator.aDeviceProperties
import static io.netiot.devices.repositories.DeviceRedisRepository.DEVICES_REDIS_HASH

class DeviceRedisRepositorySpec extends Specification{

    def redisTemplate = Mock(RedisTemplate)
    def hashOperations = Mock(HashOperations)
    def gson = new Gson()

    def deviceRedisRepository = new DeviceRedisRepository(redisTemplate, gson)

    def 'saveDevicePropertiesModel'(){
        given:
        def device = aDevice()
        def aDeviceProperties = aDeviceProperties()
        def json = gson.toJson(aDeviceProperties)

        when:
        deviceRedisRepository.save(device)

        then:
        1 * redisTemplate.opsForHash() >> hashOperations
        1 * hashOperations.put(DEVICES_REDIS_HASH, device.getId(), json)
        0 * _
    }

    def 'deleteDevicePropertiesModel'(){
        given:
        def device = aDevice()
        def deviceId = device.getId()

        when:
        deviceRedisRepository.delete(deviceId)

        then:
        1 * redisTemplate.opsForHash() >> hashOperations
        1 * hashOperations.delete(DEVICES_REDIS_HASH, deviceId)
        0 * _
    }
}
