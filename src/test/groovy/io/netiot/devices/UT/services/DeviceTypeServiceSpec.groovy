package io.netiot.devices.UT.services


import io.netiot.devices.entities.DeviceType
import io.netiot.devices.entities.Role
import io.netiot.devices.exceptions.DecoderException
import io.netiot.devices.exceptions.DeviceTypeException
import io.netiot.devices.exceptions.ProfileException
import io.netiot.devices.exceptions.ProtocolException
import io.netiot.devices.mappers.DecoderMapper
import io.netiot.devices.repositories.DeviceTypeRepository
import io.netiot.devices.repositories.ProfileRepository
import io.netiot.devices.services.DecoderService
import io.netiot.devices.services.DeviceTypeService
import io.netiot.devices.services.ProtocolService

import io.netiot.devices.services.helpers.DeviceHelperService
import io.netiot.devices.utils.AuthenticatedUserInfo
import io.netiot.devices.validators.DeviceTypeValidator
import io.netiot.devices.validators.ProfileValidator
import spock.lang.Specification
import spock.lang.Unroll

import java.util.stream.Collectors

import static io.netiot.devices.generators.DecoderGenerator.aDecoder
import static io.netiot.devices.generators.DeviceTypeGenerator.aDeviceType
import static io.netiot.devices.generators.DeviceTypeGenerator.aDeviceTypeModel
import static io.netiot.devices.generators.ProfileGenerator.aProfile
import static io.netiot.devices.generators.ProfileGenerator.aProfileModel
import static io.netiot.devices.generators.ProtocolGenerator.aProtocol
import static io.netiot.devices.utils.ErrorMessages.DECODER_NOT_EXIST_ERROR_CODE

class DeviceTypeServiceSpec extends Specification {

    def authenticatedUserInfo = Mock(AuthenticatedUserInfo)
    def deviceTypeRepository = Mock(DeviceTypeRepository)
    def profileRepository = Mock(ProfileRepository)
    def protocolService = Mock(ProtocolService)
    def deviceTypeValidator = Mock(DeviceTypeValidator)
    def profileValidator = Mock(ProfileValidator)
    def decoderService = Mock(DecoderService)
    def deviceHelperService = Mock(DeviceHelperService)

    def deviceTypeService = new DeviceTypeService(authenticatedUserInfo, deviceTypeRepository, profileRepository,
            protocolService, deviceTypeValidator, profileValidator, decoderService, deviceHelperService)

    @Unroll
    def 'getDeviceTypes'() {
        given:
        def decoder = aDecoder()
        def protocol = decoder.getProtocol()
        def protocolId = protocol.getId()
        def organizationId = 1L
        def deviceTypes = [ aDeviceType() ]
        def deviceTypeModels = [ aDeviceTypeModel() ]

        when:
        def result = deviceTypeService.getDeviceTypes(protocolId)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        findByOrganizationIdOrDefinedByPlatformIsTrueCalls * authenticatedUserInfo.getOrganizationId() >> organizationId
        findByOrganizationIdOrDefinedByPlatformIsTrueCalls * deviceTypeRepository.findByProtocolIsAndOrganizationIdOrDefinedByPlatformIsTrue(protocolId, organizationId) >> deviceTypes
        findAllCalls * deviceTypeRepository.findByProtocolId(protocolId) >> deviceTypes
        0 * _
        result == deviceTypeModels

        where:
        role               | findByOrganizationIdOrDefinedByPlatformIsTrueCalls | findAllCalls
        Role.ADMIN         | 0                                                  | 1
        Role.PARTNER_ADMIN | 1                                                  | 0
        Role.PARTNER_USER  | 1                                                  | 0
        Role.PREMIUM_USER  | 1                                                  | 0
        Role.FREE_USER     | 1                                                  | 0
    }

    @Unroll
    def 'saveDeviceType'() {
        given:
        def protocol = aProtocol()
        def decoder = aDecoder(definedByPlatform : definedByPlatform, organizationId: organizationId)
        def decoderModel = DecoderMapper.toModel(decoder)
        def deviceTypeModelReceived = aDeviceTypeModel(definedByPlatform : null, organizationId : null)
        def deviceTypeModelReturned = aDeviceTypeModel(definedByPlatform : definedByPlatform, organizationId: organizationId)
        def deviceType = aDeviceType(
                decoder: aDecoder(definedByPlatform : definedByPlatform, organizationId: organizationId, protocol: null),
                definedByPlatform : definedByPlatform,
                organizationId: organizationId)
        when:
        def result = deviceTypeService.saveDeviceType(deviceTypeModelReceived)

        then:
        1 * protocolService.getProtocol(deviceTypeModelReceived.getProtocolId()) >> Optional.of(protocol)
        1 * decoderService.getDecoder(deviceTypeModelReceived.getDecoderId()) >> decoderModel
        1 * authenticatedUserInfo.getRole() >> role
        getOrganizationIdCalls * authenticatedUserInfo.getOrganizationId() >> 1L
        1 * deviceTypeValidator.validateOnSave(_ as DeviceType)
        1 * deviceTypeRepository.save(deviceType) >> deviceType
        0 * _
        result == deviceTypeModelReturned

        where:
        role               | definedByPlatform | organizationId | getOrganizationIdCalls
        Role.ADMIN         | Boolean.TRUE      | null           | 0
        Role.PARTNER_ADMIN | Boolean.FALSE     | 1L             | 1
        Role.PARTNER_USER  | Boolean.FALSE     | 1L             | 1
        Role.FREE_USER     | Boolean.FALSE     | 1L             | 1
        Role.PREMIUM_USER  | Boolean.FALSE     | 1L             | 1
    }

    def 'saveDeviceType thrown ProtocolException - protocol not exist'() {
        given:
        def deviceTypeModelReceived = aDeviceTypeModel()

        when:
        deviceTypeService.saveDeviceType(deviceTypeModelReceived)

        then:
        1 * protocolService.getProtocol(deviceTypeModelReceived.getProtocolId()) >> Optional.empty()
        0 * _
        thrown(ProtocolException)
    }

    def 'saveDeviceType thrown DecoderException - decoder not exist'() {
        given:
        def protocol = aProtocol()
        def deviceTypeModelReceived = aDeviceTypeModel()

        when:
        deviceTypeService.saveDeviceType(deviceTypeModelReceived)

        then:
        1 * protocolService.getProtocol(deviceTypeModelReceived.getProtocolId()) >> Optional.of(protocol)
        1 * decoderService.getDecoder(deviceTypeModelReceived.getDecoderId()) >> { throw new DecoderException(DECODER_NOT_EXIST_ERROR_CODE) }
        0 * _
        thrown(DecoderException)
    }

    def 'updateDeviceType'() {
        given:
        def protocol = aProtocol()
        def decoder = aDecoder()
        def decoderModel = DecoderMapper.toModel(decoder)
        def deviceTypeModelReceived = aDeviceTypeModel()
        def deviceType = aDeviceType()

        when:
        deviceTypeService.updateDeviceType(deviceTypeModelReceived.getId(), deviceTypeModelReceived)

        then:
        1 * deviceTypeRepository.findById(deviceTypeModelReceived.getId()) >> Optional.of(deviceType)
        1 * protocolService.getProtocol(deviceTypeModelReceived.getProtocolId()) >> Optional.of(protocol)
        1 * decoderService.getDecoder(deviceTypeModelReceived.getDecoderId()) >> decoderModel
        1 * deviceTypeValidator.validateOnUpdate(_ as DeviceType)
        1 * deviceTypeRepository.save(deviceType) >> deviceType
        1 * deviceHelperService.updateDevicesWithDeviceType(deviceType.getId())
        0 * _
    }

    def 'updateDeviceType thrown ProtocolException - protocol not exist'() {
        given:
        def deviceTypeModelReceived = aDeviceTypeModel()
        def deviceType = aDeviceType()

        when:
        deviceTypeService.updateDeviceType(deviceTypeModelReceived.getId(), deviceTypeModelReceived)

        then:
        1 * deviceTypeRepository.findById(deviceTypeModelReceived.getId()) >> Optional.of(deviceType)
        1 * protocolService.getProtocol(deviceTypeModelReceived.getProtocolId()) >> Optional.empty()
        0 * _
        thrown(ProtocolException)
    }

    def 'updateDeviceType thrown DecoderException - decoder not exist'() {
        given:
        def protocol = aProtocol()
        def deviceTypeModelReceived = aDeviceTypeModel()
        def deviceType = aDeviceType()

        when:
        deviceTypeService.updateDeviceType(deviceTypeModelReceived.getId(), deviceTypeModelReceived)

        then:
        1 * deviceTypeRepository.findById(deviceTypeModelReceived.getId()) >> Optional.of(deviceType)
        1 * protocolService.getProtocol(deviceTypeModelReceived.getProtocolId()) >> Optional.of(protocol)
        1 * decoderService.getDecoder(deviceTypeModelReceived.getDecoderId()) >> { throw new DecoderException(DECODER_NOT_EXIST_ERROR_CODE) }
        0 * _
        thrown(DecoderException)
    }

    def 'deleteDeviceType'() {
        given:
        def deviceType = aDeviceType()
        def deviceTypeId = deviceType.getId()

        when:
        deviceTypeService.deleteDeviceType(deviceTypeId)

        then:
        1 * deviceTypeRepository.findById(deviceTypeId) >> Optional.of(deviceType)
        1 * deviceTypeValidator.validateOnDelete(deviceType)
        1 * deviceTypeRepository.deleteById(deviceType.getId())
        0 * _
    }

    def 'deleteDeviceType thrown DeviceTypeException - deviceType not exist'() {
        given:
        def deviceType = aDeviceType()
        def deviceTypeId = deviceType.getId()

        when:
        deviceTypeService.deleteDeviceType(deviceTypeId)

        then:
        1 * deviceTypeRepository.findById(deviceTypeId) >> Optional.empty()
        0 * _
        thrown(DeviceTypeException)
    }

    def 'getDeviceType'() {
        given:
        def deviceType = aDeviceType()
        def deviceTypeId = deviceType.getId()

        when:
        deviceTypeService.getDeviceType(deviceTypeId)

        then:
        1 * deviceTypeRepository.findById(deviceTypeId) >> Optional.of(deviceType)
        1 * deviceTypeValidator.validateOnGet(deviceType)
        0 * _
    }

    def 'getDeviceType thrown DeviceTypeException - deviceType not exist'() {
        given:
        def deviceTypeId = 1L

        when:
        deviceTypeService.getDeviceType(deviceTypeId)

        then:
        1 * deviceTypeRepository.findById(deviceTypeId) >> Optional.empty()
        0 * _
        thrown(DeviceTypeException)
    }

    @Unroll
    def 'getDeviceTypeProfiles'() {
        given:
        def organizationId = 1L
        def deviceType = aDeviceType()
        def deviceTypeId = deviceType.getId()
        def profiles = [ aProfile() ]
        def profileModels = [ aProfileModel()]

        when:
        def result = deviceTypeService.getDeviceTypeProfiles(deviceTypeId)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        1 * deviceTypeRepository.findById(deviceTypeId) >> Optional.of(deviceType)
        findByDeviceType_IdCalls * profileRepository.findByDeviceType_Id(deviceTypeId) >> profiles
        findByDeviceType_IdAndOrganizationIdCalls * profileRepository.findByDeviceType_IdAndOrganizationId(deviceTypeId, organizationId) >> profiles
        findByDeviceType_IdAndOrganizationIdCalls * authenticatedUserInfo.getOrganizationId() >> organizationId
        0 * _
        result == profileModels

        where:
        role               | findByDeviceType_IdAndOrganizationIdCalls | findByDeviceType_IdCalls
        Role.ADMIN         | 0                                         | 1
        Role.PARTNER_ADMIN | 1                                         | 0
        Role.PARTNER_USER  | 1                                         | 0
        Role.PREMIUM_USER  | 1                                         | 0
        Role.FREE_USER     | 1                                         | 0
    }

    def 'getDeviceTypeProfiles thrown DeviceTypeException - deviceType not exist'() {
        given:
        def role = Role.ADMIN
        def deviceType = aDeviceType()
        def deviceTypeId = deviceType.getId()

        when:
        deviceTypeService.getDeviceTypeProfiles(deviceTypeId)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        1 * deviceTypeRepository.findById(deviceTypeId) >> Optional.empty()
        0 * _
        thrown(DeviceTypeException)
    }

    def 'getDeviceTypeProfile'() {
        given:
        def profile = aProfile()
        def profileModel = aProfileModel()
        def deviceType = aDeviceType()
        def deviceTypeId = deviceType.getId()
        def profileId = profile.getId()

        when:
        def result = deviceTypeService.getDeviceTypeProfile(deviceTypeId, profileId)

        then:
        1 * deviceTypeRepository.findById(deviceTypeId) >> Optional.of(deviceType)
        1 * profileRepository.findById(profileId) >> Optional.of(profile)
        1 * profileValidator.validateOnGet(profile)
        0 * _
        result == profileModel
    }

    def 'getDeviceTypeProfile thrown DeviceTypeException - deviceType not exist'() {
        given:
        def profile = aProfile()
        def deviceType = aDeviceType()
        def deviceTypeId = deviceType.getId()
        def profileId = profile.getId()

        when:
        deviceTypeService.getDeviceTypeProfile(deviceTypeId, profileId)

        then:
        1 * deviceTypeRepository.findById(deviceTypeId) >> Optional.empty()
        0 * _
        thrown(DeviceTypeException)
    }

    def 'getDeviceTypeProfile thrown DeviceTypeException - deviceTypeProfile not exist'() {
        given:
        def profile = aProfile()
        def deviceType = aDeviceType()
        def deviceTypeId = deviceType.getId()
        def profileId = profile.getId()

        when:
        deviceTypeService.getDeviceTypeProfile(deviceTypeId, profileId)

        then:
        1 * deviceTypeRepository.findById(deviceTypeId) >> Optional.of(deviceType)
        1 * profileRepository.findById(profileId) >> Optional.empty()
        0 * _
        thrown(DeviceTypeException)
    }

    @Unroll
    def 'saveDeviceTypeProfile'() {
        given:
        def profile = aProfile(organizationId: organizationId)
        def profileModel = aProfileModel(organizationId: organizationId)
        def deviceType = aDeviceType()
        def deviceTypeId = deviceType.getId()

        when:
        def result = deviceTypeService.saveDeviceTypeProfile(deviceTypeId, profileModel)

        then:
        1 * deviceTypeRepository.findById(deviceTypeId) >> Optional.of(deviceType)
        1 * authenticatedUserInfo.getOrganizationId() >> organizationId
        1 * profileValidator.validateOnSave(profile)
        1 * profileRepository.save(profile) >> profile
        0 * _
        result == profileModel

        where:
        role               | organizationId
        Role.PARTNER_ADMIN | 1L
        Role.PARTNER_USER  | 1L
        Role.FREE_USER     | 1L
        Role.PREMIUM_USER  | 1L
    }

    def 'saveDeviceTypeProfile thrown DeviceTypeException - deviceType not exist'() {
        given:
        def profileModel = aProfileModel()
        def deviceType = aDeviceType()
        def deviceTypeId = deviceType.getId()

        when:
        deviceTypeService.saveDeviceTypeProfile(deviceTypeId, profileModel)

        then:
        1 * deviceTypeRepository.findById(deviceTypeId) >> Optional.empty()
        0 * _
        thrown(DeviceTypeException)
    }

    def 'updatedDeviceTypeProfile'() {
        given:
        def profile = aProfile()
        def profileModel = aProfileModel()
        def deviceType = aDeviceType()
        def profileId = profile.getId()
        def deviceTypeId = deviceType.getId()

        when:
        deviceTypeService.updateDeviceTypeProfile(deviceTypeId, profileId, profileModel)

        then:
        1 * deviceTypeRepository.findById(deviceTypeId) >> Optional.of(deviceType)
        1 * profileRepository.findById(profileId) >> Optional.of(profile)
        1 * profileValidator.validateOnUpdate(profile)
        1 * profileRepository.save(profile) >> profile
        1 * deviceHelperService.updateDevicesWithProfile(profile)
        0 * _
    }

    def 'updatedDeviceTypeProfile thrown DeviceTypeException - deviceType not exist'() {
        given:
        def profile = aProfile()
        def profileModel = aProfileModel()
        def deviceType = aDeviceType()
        def profileId = profile.getId()
        def deviceTypeId = deviceType.getId()

        when:
        deviceTypeService.updateDeviceTypeProfile(deviceTypeId, profileId, profileModel)

        then:
        1 * deviceTypeRepository.findById(deviceTypeId) >> Optional.empty()
        0 * _
        thrown(DeviceTypeException)
    }

    def 'updatedDeviceTypeProfile thrown DeviceTypeException - deviceTypeProfile not exist'() {
        given:
        def profile = aProfile()
        def profileModel = aProfileModel()
        def deviceType = aDeviceType()
        def profileId = profile.getId()
        def deviceTypeId = deviceType.getId()

        when:
        deviceTypeService.updateDeviceTypeProfile(deviceTypeId, profileId, profileModel)

        then:
        1 * deviceTypeRepository.findById(deviceTypeId) >> Optional.of(deviceType)
        1 * profileRepository.findById(profileId) >> Optional.empty()
        0 * _
        thrown(ProfileException)
    }

    def 'deleteDeviceTypeProfile'() {
        given:
        def profileId = 1L
        def deviceType = aDeviceType()
        def profile = aProfile()
        def deviceTypeId = deviceType.getId()

        when:
        deviceTypeService.deleteDeviceTypeProfile(deviceTypeId, profileId)

        then:
        1 * deviceTypeRepository.findById(deviceTypeId) >> Optional.of(deviceType)
        1 * profileRepository.findById(profileId) >> Optional.of(profile)
        1 * profileValidator.validateOnDelete(profile)
        1 * profileRepository.deleteById(profile.getId())
        0 * _
    }

    def 'deleteDeviceTypeProfile thrown DeviceTypeException - deviceType not exist'() {
        given:
        def deviceType = aDeviceType()
        def deviceTypeId = deviceType.getId()

        when:
        deviceTypeService.deleteDeviceType(deviceTypeId)

        then:
        1 * deviceTypeRepository.findById(deviceTypeId) >> Optional.empty()
        0 * _
        thrown(DeviceTypeException)
    }

    def 'deleteDeviceTypeProfile thrown DeviceTypeException - deviceTypeProfile not exist'() {
        given:
        def profileId = 1L
        def deviceType = aDeviceType()
        def deviceTypeId = deviceType.getId()

        when:
        deviceTypeService.deleteDeviceTypeProfile(deviceTypeId, profileId)

        then:
        1 * deviceTypeRepository.findById(deviceTypeId) >> Optional.of(deviceType)
        1 * profileRepository.findById(profileId) >> Optional.empty()
        0 * _
        thrown(ProfileException)
    }

}
