package io.netiot.devices.UT.services

import io.netiot.devices.entities.Decoder
import io.netiot.devices.entities.Role
import io.netiot.devices.exceptions.DecoderException
import io.netiot.devices.exceptions.ProtocolException
import io.netiot.devices.repositories.DecoderRepository
import io.netiot.devices.services.DecoderService
import io.netiot.devices.services.ProtocolService
import io.netiot.devices.services.helpers.DeviceTypeHelperService
import io.netiot.devices.utils.AuthenticatedUserInfo
import io.netiot.devices.validators.DecoderValidator
import spock.lang.Specification
import spock.lang.Unroll

import static io.netiot.devices.generators.DecoderGenerator.aDecoder
import static io.netiot.devices.generators.DecoderGenerator.aDecoderModel
import static io.netiot.devices.generators.ProtocolGenerator.aProtocol

class DecoderServiceSpec extends Specification {

    def authenticatedUserInfo = Mock(AuthenticatedUserInfo)
    def decoderRepository = Mock(DecoderRepository)
    def protocolService = Mock(ProtocolService)
    def decoderValidator = Mock(DecoderValidator)
    def deviceTypeHelperService = Mock(DeviceTypeHelperService)
    def decoderService = new DecoderService(authenticatedUserInfo, decoderRepository, protocolService,
            decoderValidator, deviceTypeHelperService)

    @Unroll
    def 'getDecoders'() {
        given:
        def decoders = [ aDecoder() ]
        def decoderModels = [ aDecoderModel() ]

        when:
        def result = decoderService.getDecoders()

        then:
        1 * authenticatedUserInfo.getRole() >> role
        callfindDecodersOrganization * authenticatedUserInfo.getOrganizationId() >> organizationId
        callfindAllDecoders * decoderRepository.findAll() >> decoders
        callfindDecodersOrganization * decoderRepository.findByOrganizationIdOrDefinedByPlatformIsTrue(organizationId) >> decoders
        0 * _
        result == decoderModels

        where:
        role               | organizationId | callfindAllDecoders | callfindDecodersOrganization
        Role.ADMIN         | null           | 1                   | 0
        Role.PARTNER_USER  | 1L             | 0                   | 1
        Role.PARTNER_ADMIN | 1L             | 0                   | 1
        Role.FREE_USER     | 1L             | 0                   | 1
        Role.PREMIUM_USER  | 1L             | 0                   | 1
    }

    @Unroll
    def 'getDecoder with given protocolId '() {
        given:
        def decoder = aDecoder()
        def protocol = decoder.getProtocol()
        def protocolId = protocol.getId()
        def decoders = [ decoder ]
        def decoderModels = [ aDecoderModel() ]


        when:
        def result = decoderService.getDecoders(protocolId)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        callfindDecodersOrganization * authenticatedUserInfo.getOrganizationId() >> organizationId
        callfindAllDecoders * decoderRepository.findByProtocolId(protocolId) >> decoders
        callfindDecodersOrganization * decoderRepository.findByProtocolIsAndOrganizationIdOrDefinedByPlatformIsTrue(protocolId, organizationId) >> decoders
        0 * _
        result == decoderModels

        where:
        role               | organizationId | callfindAllDecoders | callfindDecodersOrganization
        Role.ADMIN         | null           | 1                   | 0
        Role.PARTNER_USER  | 1L             | 0                   | 1
        Role.PARTNER_ADMIN | 1L             | 0                   | 1
        Role.FREE_USER     | 1L             | 0                   | 1
        Role.PREMIUM_USER  | 1L             | 0                   | 1
    }

    @Unroll
    def 'saveDecoder'() {
        given:
        def decoderModelReceived = aDecoderModel(definedByPlatform : null, organizationId : null)
        def decoderModelReturned = aDecoderModel(definedByPlatform : definedByPlatform, organizationId: organizationId)
        def decoder = aDecoder(definedByPlatform : definedByPlatform, organizationId: organizationId)
        def protocol = aProtocol()

        when:
        def result = decoderService.saveDecoder(decoderModelReceived)

        then:
        1 * protocolService.getProtocol(decoderModelReceived.getProtocolId()) >> Optional.of(protocol)
        1 * decoderValidator.validateOnSave(_ as Decoder)
        1 * authenticatedUserInfo.getRole() >> role
        getOrganizationIdCalls * authenticatedUserInfo.getOrganizationId() >> 1L
        1 * decoderRepository.save(decoder) >> decoder
        0 * _
        result == decoderModelReturned

        where:
        role               | definedByPlatform | organizationId | getOrganizationIdCalls
        Role.ADMIN         | Boolean.TRUE      | null           | 0
        Role.PARTNER_ADMIN | Boolean.FALSE     | 1L             | 1
        Role.PARTNER_USER  | Boolean.FALSE     | 1L             | 1
        Role.FREE_USER     | Boolean.FALSE     | 1L             | 1
        Role.PREMIUM_USER  | Boolean.FALSE     | 1L             | 1
    }

    def 'saveDecoder thrown ProtocolException - protocol not exist'() {
        given:
        def decoderModel = aDecoderModel()

        when:
        decoderService.saveDecoder(decoderModel)

        then:
        1 * protocolService.getProtocol(decoderModel.getProtocolId()) >> Optional.empty()
        0 * _
        thrown(ProtocolException)
    }

    def 'updateDecoder'() {
        given:
        def decoderModelReceived = aDecoderModel()
        def decoderId = decoderModelReceived.getId()
        def decoder = aDecoder()
        def protocol = aProtocol()

        when:
        decoderService.updateDecoder(decoderId, decoderModelReceived)

        then:
        1 * decoderRepository.findById(decoderId) >> Optional.of(decoder)
        1 * protocolService.getProtocol(decoderModelReceived.getProtocolId()) >> Optional.of(protocol)
        1 * decoderValidator.validateOnUpdate(_ as Decoder)
        1 * decoderRepository.save(decoder) >> decoder
        1 * deviceTypeHelperService.updateDeviceTypesWithDecoder(decoder)
        0 * _
    }

    def 'updateDecoder thrown ProtocolException - protocol not exist'() {
        given:
        def decoderModelReceived = aDecoderModel()
        def decoder = aDecoder()
        def decoderId = decoderModelReceived.getId()

        when:
        decoderService.updateDecoder(decoderId, decoderModelReceived)

        then:
        1 * decoderRepository.findById(decoderId) >> Optional.of(decoder)
        1 * protocolService.getProtocol(decoderModelReceived.getProtocolId()) >> Optional.empty()
        0 * _
        thrown(ProtocolException)
    }

    def 'updateDecoder thrown DecoderException - decoder not exist'() {
        given:
        def decoderModelReceived = aDecoderModel()
        def decoderId = decoderModelReceived.getId()

        when:
        decoderService.updateDecoder(decoderId, decoderModelReceived)

        then:
        1 * decoderRepository.findById(decoderId) >> Optional.empty()
        0 * _
        thrown(DecoderException)
    }

    def 'deleteDecoder'() {
        given:
        def decoder = aDecoder()
        def decoderId = decoder.getId()

        when:
        decoderService.deleteDecoder(decoderId)

        then:
        1 * decoderRepository.findById(decoderId) >> Optional.of(decoder)
        1 * decoderValidator.validateOnDelete(decoder)
        1 * decoderRepository.deleteById(decoder.getId())
        0 * _
    }

    def 'deleteDecoder thrown DecoderException - decoder not exist'() {
        given:
        def decoderId = 1L

        when:
        decoderService.deleteDecoder(decoderId)

        then:
        1 * decoderRepository.findById(decoderId) >> Optional.empty()
        0 * _
        thrown(DecoderException)
    }

    def 'getDecoder'() {
        given:
        def decoder = aDecoder()
        def decoderId = decoder.getId()

        when:
        decoderService.getDecoder(decoderId)

        then:
        1 * decoderRepository.findById(decoderId) >> Optional.of(decoder)
        1 * decoderValidator.validateOnGet(decoder)
        0 * _
    }

    def 'getDecoder thrown DecoderException - decoder not exist'() {
        given:
        def decoderId = 1L

        when:
        decoderService.getDecoder(decoderId)

        then:
        1 * decoderRepository.findById(decoderId) >> Optional.empty()
        0 * _
        thrown(DecoderException)
    }

}
