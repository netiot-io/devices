package io.netiot.devices.UT.services


import io.netiot.devices.services.AlertService
import io.netiot.devices.services.KafkaMessageReceiverService
import io.netiot.devices.validators.AlertValidator
import spock.lang.Specification
import spock.lang.Unroll

import static io.netiot.devices.generators.AlertGenerator.aAlertEvent

class KafkaMessageReceiverServiceSpec extends Specification {

    def alertService = Mock(AlertService)
    def alertValidator = Mock(AlertValidator)


    def kafkaMessageReceiverService = new KafkaMessageReceiverService(alertService, alertValidator)

    @Unroll
    def 'receiveEvent AlertEvent'(){
        given:
        def alertEvent = aAlertEvent()

        when:
        kafkaMessageReceiverService.receiveEvent(alertEvent)

        then:
        1 * alertValidator.validateAlertEvent(alertEvent) >> validateAlertResult
        saveCalls * alertService.save(alertEvent)
        0 * _

        where:
        validateAlertResult | saveCalls
        true                | 1
        false               | 0
    }

}
