package io.netiot.devices.UT.services

import io.netiot.devices.repositories.ProtocolRepository
import io.netiot.devices.services.ProtocolService
import spock.lang.Specification

import static io.netiot.devices.generators.ProtocolGenerator.*

class ProtocolServiceSpec extends Specification {

    def protocolRepository = Mock(ProtocolRepository)
    def protocolService = new ProtocolService(protocolRepository)

    def 'getProtocols'(){
        given:
        def protocols = [ aProtocol() ]
        def protocolModels = [ aProtocolModel() ]

        when:
        def result = protocolService.getProtocols()

        then:
        1 * protocolRepository.findAll() >> protocols
        0 * _

        result == protocolModels
    }

    def 'getProtocol'(){
        given:
        def protocolName = "protocolName"

        when:
        def result = protocolService.getProtocol(protocolName)

        then:
        1 * protocolRepository.findByName(protocolName) >> Optional.of(aProtocol())
        0 * _

        result == Optional.of(aProtocol())
    }

    def 'getProtocol by id'(){
        given:
        def protocol = aProtocol()
        def protocolId = protocol.getId()

        when:
        def result = protocolService.getProtocol(protocolId)

        then:
        1 * protocolRepository.findById(protocolId) >> Optional.of(aProtocol())
        0 * _

        result == Optional.of(aProtocol())
    }

}
