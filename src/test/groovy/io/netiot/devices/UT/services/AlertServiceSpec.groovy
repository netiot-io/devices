package io.netiot.devices.UT.services

import io.netiot.devices.repositories.AlertRepository
import io.netiot.devices.services.AlertService
import io.netiot.devices.validators.AlertValidator
import spock.lang.Specification
import spock.lang.Unroll

import static io.netiot.devices.generators.AlertGenerator.aAlert
import static io.netiot.devices.generators.AlertGenerator.aAlertEvent
import static io.netiot.devices.generators.AlertGenerator.aAlertModel

class AlertServiceSpec extends Specification {

    def alertRepository = Mock(AlertRepository)
    def alertValidator = Mock(AlertValidator)

    def alertService = new AlertService(alertRepository, alertValidator)

    @Unroll
    def 'save'() {
        given:
        def alertEvent = aAlertEvent()
        def alert = aAlert(id: null)

        when:
        alertService.save(alertEvent)

        then:
        1 * alertRepository.findByDeviceIdAndTimestamp(alert.getDeviceId(), alert.getTimestamp()) >> optional
        saveAlertCalls * alertRepository.save(alert)
        0 * _

        where:
        optional              | saveAlertCalls
        Optional.of(aAlert()) | 0
        Optional.empty()      | 1
    }

    def 'getAlerts'(){
        given:
        def deviceId = 1L
        def alertModels = [ aAlertModel() ]
        def alerts = [ aAlert() ]

        when:
        def result = alertService.getAlerts(deviceId)

        then:
        1 * alertValidator.validateOnGet(deviceId)
        1 * alertRepository.findByDeviceId(deviceId) >> alerts
        0 * _
        result == alertModels
    }

    def 'getLast10Alerts'(){
        given:
        def deviceId = 1L
        def alertModels = [ aAlertModel() ]
        def alerts = [ aAlert() ]

        when:
        def result = alertService.getLast10Alerts(deviceId)

        then:
        1 * alertValidator.validateOnGet(deviceId)
        1 * alertRepository.findTop10ByDeviceIdInOrderByIdDesc(deviceId) >> alerts
        0 * _
        result == alertModels
    }

}
