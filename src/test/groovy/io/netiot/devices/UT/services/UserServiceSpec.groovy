package io.netiot.devices.UT.services

import feign.FeignException
import io.netiot.devices.exceptions.InternalErrorException
import io.netiot.devices.feign.UsersClient
import io.netiot.devices.services.UserService
import org.springframework.http.ResponseEntity
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import static io.netiot.devices.generators.UserGenerator.aUser
import static io.netiot.devices.generators.UserGenerator.aUserInternalModel

class UserServiceSpec extends Specification {

    def usersClient = Mock(UsersClient)

    def userService = new UserService(usersClient)

    @Shared
    def errorMessage = "error"

    def 'findUsersByIds'(){
        given:
        def user1 = aUser()
        def user2 = aUser(id : 2L)
        def userIds = [user1.id, user2.id]
        def users = [user1, user2]
        def userInternalModel1 = aUserInternalModel()
        def userInternalModel2 = aUserInternalModel(id: 2L)
        def userResponseEntity = ResponseEntity.ok([userInternalModel1, userInternalModel2])

        when:
        def result = userService.findUsersByIds(userIds)

        then:
        1 * usersClient.getUsers(userIds) >> userResponseEntity
        0 * _

        result == Optional.of(users)
    }

    def 'findUsersByIds return empty optional'(){
        given:
        def userId = 1L

        when:
        def result = userService.findUsersByIds([userId])

        then:
        1 * usersClient.getUsers([userId]) >> { throw new FeignException(404, errorMessage) }
        0 * _

        result == Optional.empty()
    }

    @Unroll
    def 'findUsersByIds - throw InternalErrorException'(){
        given:
        def userId = 1L


        when:
        userService.findUsersByIds([userId])

        then:
        1 * usersClient.getUsers([userId]) >> exception
        0 * _
        thrown(InternalErrorException)

        where:
        exception << [ { throw new FeignException(errorMessage) }, { throw new Exception() } ]
    }

}
