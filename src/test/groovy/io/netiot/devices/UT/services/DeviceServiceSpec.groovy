package io.netiot.devices.UT.services

import io.netiot.devices.entities.Role
import io.netiot.devices.exceptions.DeviceException
import io.netiot.devices.exceptions.DeviceTypeException
import io.netiot.devices.exceptions.UserException
import io.netiot.devices.exceptions.ProfileException
import io.netiot.devices.models.events.EventType
import io.netiot.devices.repositories.DeviceRedisRepository
import io.netiot.devices.repositories.DeviceRepository
import io.netiot.devices.repositories.DevicesUsersRepository
import io.netiot.devices.services.DeviceService
import io.netiot.devices.services.DeviceTypeService
import io.netiot.devices.services.KafkaMessageSenderService
import io.netiot.devices.services.UserService
import io.netiot.devices.utils.AuthenticatedUserInfo
import io.netiot.devices.validators.DeviceValidator
import spock.lang.Specification

import static io.netiot.devices.generators.DeviceGenerator.aDevice
import static io.netiot.devices.generators.DeviceGenerator.aDeviceEvent
import static io.netiot.devices.generators.DeviceGenerator.aDeviceModel
import static io.netiot.devices.generators.DeviceGenerator.aDeviceApplicationModel
import static io.netiot.devices.generators.DeviceTypeGenerator.aDeviceType
import static io.netiot.devices.generators.ProfileGenerator.aProfile
import static io.netiot.devices.generators.UserGenerator.aUser

class DeviceServiceSpec extends Specification {

    def authenticatedUserInfo = Mock(AuthenticatedUserInfo)
    def deviceRepository = Mock(DeviceRepository)
    def devicesUsersRepository = Mock(DevicesUsersRepository)
    def deviceValidator = Mock(DeviceValidator)
    def deviceTypeService = Mock(DeviceTypeService)
    def kafkaSenderService = Mock(KafkaMessageSenderService)
    def deviceRedisRepository = Mock(DeviceRedisRepository)
    def userService = Mock(UserService)

    def deviceService = new DeviceService(authenticatedUserInfo, deviceRepository, devicesUsersRepository, deviceValidator,
            deviceTypeService, kafkaSenderService, deviceRedisRepository,userService)

    def 'getDevices'() {
        given:
        def userId = 1L
        def devices = [ aDevice() ]
        def deviceModels = [ aDeviceModel() ]

        when:
        def result = deviceService.getDevices()

        then:
        1 * authenticatedUserInfo.getRole() >> role
        findAllCalls * deviceRepository.findAll() >> devices
        findByUserId * authenticatedUserInfo.getId() >> userId
        findByUserId * deviceRepository.findByUsers_UserId(userId) >> devices
        0 * _
        result == deviceModels

        where:
        role                    | findByUserId | findAllCalls
        Role.ADMIN              | 0            | 1
        Role.PARTNER_ADMIN      | 1            | 0
        Role.PARTNER_USER       | 1            | 0
        Role.ENTERPRISE_ADMIN   | 1            | 0
        Role.ENTERPRISE_USER    | 1            | 0
        Role.FREE_USER          | 1            | 0
        Role.PREMIUM_USER       | 1            | 0
    }

    def 'getDevice'(){
        given:
        def deviceId = 1L
        def device = aDevice(id: deviceId)
        def deviceModel = aDeviceModel()

        when:
        def result = deviceService.getDevice(deviceId)

        then:
        1 * deviceRepository.findById(deviceId) >> Optional.of(device)
        1 * deviceValidator.validateOnGet(device)
        0 * _
        result == deviceModel
    }

    def 'getDevice thrown DeviceException - device not exist'(){
        given:
        def deviceId = 1L

        when:
        deviceService.getDevice(deviceId)

        then:
        1 * deviceRepository.findById(deviceId) >> Optional.empty()
        0 * _
        thrown(DeviceException)
    }

    def 'saveDevice'() {
        given:
        def device = aDevice()
        def gatewayTopic = device.getDeviceType().getProtocol().getGatewayTopic()
        def deviceModel = aDeviceModel()
        def deviceEvent = aDeviceEvent()
        def user1 = aUser()
        def user2 = aUser(id: 2L)

        println device

        when:
        def result = deviceService.saveDevice(deviceModel)

        then:
        1 * deviceTypeService.findDeviceTypeById(deviceModel.getDeviceTypeId()) >> Optional.of(aDeviceType())
        1 * deviceTypeService.findProfileById(deviceModel.getProfileId()) >> Optional.of(aProfile())
        1 * userService.findUsersByIds(deviceModel.getUserIds()) >> Optional.of([user1, user2])
        1 * devicesUsersRepository.deleteByDevice_Id(device.getId())
        1 * deviceValidator.validateOnSave(device)
        1 * deviceRepository.saveAndFlush(device) >> device
        1 * deviceRedisRepository.save(device)
        1 * kafkaSenderService.sendDeviceEvent(deviceEvent, gatewayTopic)
        0 * _
        result == deviceModel
    }

    def 'saveDevice thrown DeviceTypeException - deviceType not exist'() {
        given:
        def deviceModel = aDeviceModel()

        when:
        deviceService.saveDevice(deviceModel)

        then:
        1 * deviceTypeService.findDeviceTypeById(deviceModel.getDeviceTypeId()) >> Optional.empty()
        0 * _
        thrown(DeviceTypeException)
    }

    def 'saveDevice thrown ProfileException - profile not exist'() {
        given:
        def deviceModel = aDeviceModel()

        when:
        deviceService.saveDevice(deviceModel)

        then:
        1 * deviceTypeService.findDeviceTypeById(deviceModel.getDeviceTypeId()) >> Optional.of(aDeviceType())
        1 * deviceTypeService.findProfileById(deviceModel.getProfileId()) >> Optional.empty()
        0 * _
        thrown(ProfileException)
    }

    def 'updateDevice'() {
        given:
        def oldDevice = aDevice()
        def gatewayTopic = oldDevice.getDeviceType().getProtocol().getGatewayTopic()
        def deviceModel = aDeviceModel()
        def deviceId = deviceModel.getId()
        def user = aUser()
        def deviceEvent = aDeviceEvent(eventType: EventType.CHANGED)

        when:
        deviceService.updateDevice(deviceId, deviceModel)

        then:
        1 * deviceRepository.findById(deviceId) >> Optional.of(oldDevice)
        1 * deviceTypeService.findDeviceTypeById(deviceModel.getDeviceTypeId()) >> Optional.of(aDeviceType())
        1 * deviceTypeService.findProfileById(deviceModel.getProfileId()) >> Optional.of(aProfile())
        1 * userService.findUsersByIds(deviceModel.getUserIds()) >> Optional.of([user])
        1 * devicesUsersRepository.deleteByDevice_Id(deviceId)
        1 * deviceValidator.validateOnUpdate(oldDevice)
        1 * deviceRepository.save(oldDevice) >> oldDevice
        1 * deviceRedisRepository.save(oldDevice)
        1 * kafkaSenderService.sendDeviceEvent(deviceEvent, gatewayTopic)
        0 * _
    }

    def 'updateDevice thrown DeviceTypeException - deviceType not exist'() {
        given:
        def device = aDevice()
        def deviceModel = aDeviceModel()
        def deviceId = deviceModel.getId()

        when:
        deviceService.updateDevice(deviceId, deviceModel)

        then:
        1 * deviceRepository.findById(deviceId) >> Optional.of(device)
        1 * deviceTypeService.findDeviceTypeById(deviceModel.getDeviceTypeId()) >> Optional.empty()
        0 * _
        thrown(DeviceTypeException)
    }

    def 'updateDevice thrown ProfileException - profile not exist'() {
        given:
        def device = aDevice()
        def deviceModel = aDeviceModel()
        def deviceId = deviceModel.getId()

        when:
        deviceService.updateDevice(deviceId, deviceModel)

        then:
        1 * deviceRepository.findById(deviceId) >> Optional.of(device)
        1 * deviceTypeService.findDeviceTypeById(deviceModel.getDeviceTypeId()) >> Optional.of(aDeviceType())
        1 * deviceTypeService.findProfileById(deviceModel.getProfileId()) >> Optional.empty()
        0 * _
        thrown(ProfileException)
    }

    def 'updateDevice thrown DeviceException - device not exist'() {
        given:
        def deviceModel = aDeviceModel()
        def deviceId = deviceModel.getId()

        when:
        deviceService.updateDevice(deviceId, deviceModel)

        then:
        1 * deviceRepository.findById(deviceId) >> Optional.empty()
        0 * _
        thrown(DeviceException)
    }

    def 'deleteDevice'() {
        given:
        def deviceId = 1L
        def device = aDevice(id: deviceId)
        def gatewayTopic = device.getDeviceType().getProtocol().getGatewayTopic()
        def eventType = EventType.DELETED
        def deviceEvent = aDeviceEvent(eventType: eventType)

        when:
        deviceService.deleteDevice(deviceId)

        then:
        1 * deviceRepository.findById(deviceId) >> Optional.of(device)
        1 * deviceValidator.validateOnDelete(device)
        1 * deviceRepository.deleteById(deviceId)
        1 * deviceRedisRepository.delete(deviceId)
        1 * kafkaSenderService.sendDeviceEvent(deviceEvent, gatewayTopic)
        0 * _
    }

    def 'deleteDevice thrown DeviceException - device not exist'() {
        given:
        def deviceId = 1L

        when:
        deviceService.deleteDevice(deviceId)

        then:
        1 * deviceRepository.findById(deviceId) >> Optional.empty()
        0 * _
        thrown(DeviceException)
    }

    def 'getDeviceName'() {
        given:
        def deviceId = 1L
        def devices = [ aDevice() ]
        def expectedResult = aDeviceApplicationModel().name

        when:
        def result = deviceService.getDeviceName(deviceId)

        then:
        1 * deviceRepository.findById(deviceId) >> devices
        0 * _

        result == expectedResult
    }

}
