package io.netiot.devices.UT.services

import io.netiot.devices.repositories.DeviceRedisRepository
import io.netiot.devices.repositories.DeviceRepository
import io.netiot.devices.services.helpers.DeviceHelperService
import spock.lang.Specification
import spock.lang.Unroll

import static io.netiot.devices.generators.DeviceGenerator.aDevice

class DeviceHelperServiceSpec extends Specification {

    def deviceRepository = Mock(DeviceRepository)
    def deviceRedisRepository = Mock(DeviceRedisRepository)

    def deviceHelperService = new DeviceHelperService(deviceRepository, deviceRedisRepository)

    def 'updateDevicesWithDeviceType'(){
        given:
        def deviceTypeId = 1L
        def device = aDevice()
        def devices = [ device ]

        when:
        deviceHelperService.updateDevicesWithDeviceType(deviceTypeId)

        then:
        1 * deviceRepository.findByDeviceType_Id(deviceTypeId) >> devices
        1 * deviceRedisRepository.save(device)
        0 * _
    }
}
