package io.netiot.devices.UT.services

import io.netiot.devices.configurations.KafkaChannels
import io.netiot.devices.services.KafkaMessageSenderService
import org.springframework.cloud.stream.binding.BinderAwareChannelResolver
import org.springframework.messaging.Message
import org.springframework.messaging.MessageChannel
import spock.lang.Specification

import static io.netiot.devices.generators.DeviceGenerator.aDeviceEvent

class KafkaMessageSenderServiceSpec extends Specification {

    def binderAwareChannelResolver = Mock(BinderAwareChannelResolver)
    def messageChannel = Mock(MessageChannel)

    def kafkaMessageSenderService = new KafkaMessageSenderService(binderAwareChannelResolver)


    def 'sendDeviceEvent'() {
        given:
        def deviceEvent = aDeviceEvent()
        def topic = "topic"

        when:
        kafkaMessageSenderService.sendDeviceEvent(deviceEvent, topic)

        then:
        1 * binderAwareChannelResolver.resolveDestination(topic) >> messageChannel
        1 * messageChannel.send(_ as Message)
        0 * _
    }

}