package io.netiot.devices.UT.validators

import io.netiot.devices.entities.Role
import io.netiot.devices.exceptions.DecoderException
import io.netiot.devices.exceptions.DeviceTypeException
import io.netiot.devices.repositories.DeviceTypeRepository
import io.netiot.devices.utils.AuthenticatedUserInfo
import io.netiot.devices.validators.DeviceTypeValidator
import spock.lang.Specification
import spock.lang.Unroll

import static io.netiot.devices.generators.DecoderGenerator.aDecoder
import static io.netiot.devices.generators.DeviceTypeGenerator.aDeviceType

class DeviceTypeValidatorSpec extends Specification {

    def authenticatedUserInfo = Mock(AuthenticatedUserInfo)
    def deviceTypeRepository = Mock(DeviceTypeRepository)
    def deviceTypeValidator = new DeviceTypeValidator(authenticatedUserInfo, deviceTypeRepository)

    @Unroll
    def 'validateOnSave'() {
        given:
        def decoder = aDecoder(definedByPlatform: decoderDefinedByPlatform, organizationId: decoderOrganizationId)
        def deviceType = aDeviceType(decoder: decoder, definedByPlatform: deviceTypeDefinedByPlatform, organizationId: deviceTypeOrganizationId)

        when:
        deviceTypeValidator.validateOnSave(deviceType)

        then:
        1 * deviceTypeRepository.findByName(deviceType.getName()) >> Optional.empty()
        0 * _

        where:
        deviceTypeDefinedByPlatform | deviceTypeOrganizationId | decoderDefinedByPlatform | decoderOrganizationId
        Boolean.FALSE               | 1L                       | Boolean.TRUE             | null
        Boolean.TRUE                | null                     | Boolean.TRUE             | null
        Boolean.FALSE               | 1L                       | Boolean.FALSE            | 1L
    }

    def 'validateOnSave thrown DeviceTypeException - device already exist'() {
        given:
        def deviceType = aDeviceType()

        when:
        deviceTypeValidator.validateOnSave(deviceType)

        then:
        1 * deviceTypeRepository.findByName(deviceType.getName()) >> Optional.of(deviceType)
        0 * _
        thrown(DeviceTypeException)
    }

    def 'validateOnSave thrown DeviceTypeException - decoder not owned by user'() {
        given:
        def otherOrganizationId = 2L
        def userOrganizationId = 1L
        def decoder = aDecoder(organizationId: otherOrganizationId)
        def deviceType = aDeviceType(organizationId: userOrganizationId, decoder: decoder)

        when:
        deviceTypeValidator.validateOnSave(deviceType)

        then:
        1 * deviceTypeRepository.findByName(deviceType.getName()) >> Optional.empty()
        0 * _
        thrown(DeviceTypeException)
    }

    def 'validateOnSave thrown DeviceTypeException - deviceType defined by platform and decoder defined by a other users, not by platform'() {
        given:
        def otherOrganizationId = 2L
        def decoder = aDecoder(definedByPlatform: Boolean.FALSE, organizationId: otherOrganizationId)
        def deviceType = aDeviceType(definedByPlatform: Boolean.TRUE, organizationId: null, decoder: decoder)

        when:
        deviceTypeValidator.validateOnSave(deviceType)

        then:
        1 * deviceTypeRepository.findByName(deviceType.getName()) >> Optional.empty()
        0 * _
        thrown(DeviceTypeException)
    }

    @Unroll
    def 'validateOnDelete'(){
        given:
        def deviceType = aDeviceType()

        when:
        deviceTypeValidator.validateOnDelete(deviceType)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        getOrganizationIdCalls * authenticatedUserInfo.getOrganizationId() >> deviceType.getOrganizationId()
        0 * _

        where:
        role               | getOrganizationIdCalls
        Role.ADMIN         | 0
        Role.PREMIUM_USER  | 1
        Role.FREE_USER     | 1
        Role.PARTNER_USER  | 1
        Role.PARTNER_ADMIN | 1
    }

    @Unroll
    def 'validateOnDelete thrown DeviceTypeException - unauthorized action'(){
        given:
        def deviceType = aDeviceType(organizationId : 1L)

        when:
        deviceTypeValidator.validateOnDelete(deviceType)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        1 * authenticatedUserInfo.getOrganizationId() >> organizationId
        0 * _
        thrown(DeviceTypeException)

        where:
        role << [Role.PREMIUM_USER, Role.FREE_USER, Role.PARTNER_USER, Role.PARTNER_ADMIN]
        organizationId = 2L
    }

    @Unroll
    def 'validateOnDelete thrown DeviceTypeException - decoder defined by platform'(){
        given:
        def deviceType = aDeviceType(definedByPlatform : Boolean.TRUE, organizationId : null)

        when:
        deviceTypeValidator.validateOnDelete(deviceType)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        0 * _
        thrown(DeviceTypeException)

        where:
        role << [Role.PREMIUM_USER, Role.FREE_USER, Role.PARTNER_USER, Role.PARTNER_ADMIN]
    }

    @Unroll
    def 'validateOnGet'(){
        given:
        def userOrganizationId = 1L
        def deviceType = aDeviceType(definedByPlatform : Boolean.FALSE, organizationId : organizationId)

        when:
        deviceTypeValidator.validateOnGet(deviceType)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        1 * authenticatedUserInfo.getOrganizationId() >> userOrganizationId
        0 * _

        where:
        role              | definedByPlatforme | organizationId
        Role.PREMIUM_USER | true               | 1L
        Role.FREE_USER    | true               | 1L
        Role.PARTNER_USER | true               | 1L
        Role.ADMIN        | true               | null
        Role.ADMIN        | false              | 1L
    }

    @Unroll
    def 'validateOnGet thrown DeviceTypeException - unauthorized action'(){
        given:
        def organizationId = 2L
        def userOrganizationId = 1L
        def deviceType = aDeviceType(definedByPlatform : Boolean.FALSE, organizationId : organizationId)

        when:
        deviceTypeValidator.validateOnGet(deviceType)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        1 * authenticatedUserInfo.getOrganizationId() >> userOrganizationId
        0 * _
        thrown(DeviceTypeException)

        where:
        role << [Role.PREMIUM_USER, Role.FREE_USER, Role.PARTNER_USER]
    }

    @Unroll
    def 'validateOnUpdate'() {
        given:
        def decoder = aDecoder(definedByPlatform: decoderDefinedByPlatform, organizationId: decoderOrganizationId)
        def deviceType = aDeviceType(decoder: decoder, definedByPlatform: deviceTypeDefinedByPlatform, organizationId: deviceTypeOrganizationId)

        when:
        deviceTypeValidator.validateOnUpdate(deviceType)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        1 * deviceTypeRepository.findByName(deviceType.getName()) >> Optional.empty()
        getOrganizationIdCalls * authenticatedUserInfo.getOrganizationId() >> userOganizationId
        0 * _

        where:
        role               | deviceTypeDefinedByPlatform | deviceTypeOrganizationId | decoderDefinedByPlatform | decoderOrganizationId | getOrganizationIdCalls | userOganizationId
        Role.ADMIN         | Boolean.TRUE                | null                     | Boolean.TRUE             | null                  | 0                      | null
        Role.PARTNER_USER  | Boolean.FALSE               | 1L                       | Boolean.TRUE             | null                  | 1                      | 1L
        Role.PARTNER_ADMIN | Boolean.FALSE               | 1L                       | Boolean.TRUE             | null                  | 1                      | 1L
        Role.FREE_USER     | Boolean.FALSE               | 1L                       | Boolean.TRUE             | null                  | 1                      | 1L
        Role.PREMIUM_USER  | Boolean.FALSE               | 1L                       | Boolean.TRUE             | null                  | 1                      | 1L
        Role.PARTNER_USER  | Boolean.FALSE               | 1L                       | Boolean.FALSE            | 1L                    | 1                      | 1L
        Role.PARTNER_ADMIN | Boolean.FALSE               | 1L                       | Boolean.FALSE            | 1L                    | 1                      | 1L
        Role.FREE_USER     | Boolean.FALSE               | 1L                       | Boolean.FALSE            | 1L                    | 1                      | 1L
        Role.PREMIUM_USER  | Boolean.FALSE               | 1L                       | Boolean.FALSE            | 1L                    | 1                      | 1L
    }

    @Unroll
    def 'validateOnUpdate thrown DeviceTypeException - deviceType created by Platform'(){
        given:
        def deviceType = aDeviceType(definedByPlatform: Boolean.TRUE, organizationId : null)

        when:
        deviceTypeValidator.validateOnUpdate(deviceType)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        0 * _
        thrown(DeviceTypeException)

        where:
        role << [Role.PREMIUM_USER, Role.FREE_USER, Role.PARTNER_USER, Role.PARTNER_ADMIN]
    }

    @Unroll
    def 'validateOnUpdate thrown DeviceTypeException - unauthorized action'(){
        given:
        def userOrganizationId = 2L
        def deviceTypeCreatedByOrganization = 1L
        def deviceType = aDeviceType(organizationId : deviceTypeCreatedByOrganization)

        when:
        deviceTypeValidator.validateOnUpdate(deviceType)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        1 * authenticatedUserInfo.getOrganizationId() >> userOrganizationId
        0 * _
        thrown(DeviceTypeException)

        where:
        role << [Role.PREMIUM_USER, Role.FREE_USER, Role.PARTNER_USER, Role.PARTNER_ADMIN]
    }

    @Unroll
    def 'validateOnUpdate thrown DeviceTypeException - decoder not owned by user'() {
        given:
        def otherOrganizationId = 2L
        def userOrganizationId = 1L
        def decoder = aDecoder(definedByPlatform: false, organizationId: otherOrganizationId)
        def deviceType = aDeviceType(definedByPlatform: false, organizationId: userOrganizationId, decoder: decoder)

        when:
        deviceTypeValidator.validateOnUpdate(deviceType)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        1 * authenticatedUserInfo.getOrganizationId() >> userOrganizationId
        0 * _
        thrown(DeviceTypeException)
        where:
        role << [Role.PREMIUM_USER, Role.FREE_USER, Role.PARTNER_USER]
    }

    def 'validateOnUpdate thrown DeviceTypeException - deviceType defined by platform and decoder defined by a other users, not by platform'() {
        given:
        def otherOrganizationId = 2L
        def decoder = aDecoder(definedByPlatform: Boolean.FALSE, organizationId: otherOrganizationId)
        def deviceType = aDeviceType(definedByPlatform: Boolean.TRUE, organizationId: null, decoder: decoder)

        when:
        deviceTypeValidator.validateOnUpdate(deviceType)

        then:
        1 * authenticatedUserInfo.getRole() >> Role.ADMIN
        0 * _
        thrown(DeviceTypeException)
    }

}
