package io.netiot.devices.UT.validators

import io.netiot.devices.entities.Role
import io.netiot.devices.exceptions.DecoderException
import io.netiot.devices.repositories.DecoderRepository
import io.netiot.devices.utils.AuthenticatedUserInfo
import io.netiot.devices.validators.DecoderValidator
import spock.lang.Specification
import spock.lang.Unroll

import static io.netiot.devices.generators.DecoderGenerator.aDecoder

class DecoderValidatorSpec extends Specification{

    def authenticatedUserInfo = Mock(AuthenticatedUserInfo)
    def decoderRepository = Mock(DecoderRepository)
    def decoderValidator = new DecoderValidator(authenticatedUserInfo, decoderRepository)

    def 'validateOnSave'(){
        given:
        def decoder = aDecoder()

        when:
        decoderValidator.validateOnSave(decoder)

        then:
        1 * decoderRepository.findByName(decoder.getName()) >> Optional.empty()
        0 * _
    }

    def 'validateOnSave thrown DecoderException'(){
        given:
        def decoder = aDecoder()

        when:
        decoderValidator.validateOnSave(decoder)

        then:
        1 * decoderRepository.findByName(decoder.getName()) >> Optional.ofNullable(aDecoder())
        0 * _

        thrown(DecoderException)
    }

    @Unroll
    def 'validateOnUpdate'(){
        given:
        def decoder = aDecoder()
        def userOrganizationId = decoder.getOrganizationId()

        when:
        decoderValidator.validateOnUpdate(decoder)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        getOrganizationIdCalls * authenticatedUserInfo.getOrganizationId() >> userOrganizationId
        1 * decoderRepository.findByName(decoder.getName()) >> Optional.empty()
        0 * _

        where:
        role               | getOrganizationIdCalls
        Role.ADMIN         | 0
        Role.PREMIUM_USER  | 1
        Role.FREE_USER     | 1
        Role.PARTNER_USER  | 1
        Role.PARTNER_ADMIN | 1
    }

    @Unroll
    def 'validateOnUpdate thrown DecoderException - decoder name already exist'(){
        given:
        def decoder = aDecoder()
        def foundDecoder = aDecoder(id : 2L)

        def userOrganizationId = decoder.getOrganizationId()

        when:
        decoderValidator.validateOnUpdate(decoder)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        getOrganizationIdCalls * authenticatedUserInfo.getOrganizationId() >> userOrganizationId
        1 * decoderRepository.findByName(decoder.getName()) >> Optional.of(foundDecoder)
        0 * _
        thrown(DecoderException)

        where:
        role               | getOrganizationIdCalls
        Role.ADMIN         | 0
        Role.PREMIUM_USER  | 1
        Role.FREE_USER     | 1
        Role.PARTNER_USER  | 1
        Role.PARTNER_ADMIN | 1
    }

    @Unroll
    def 'validateOnUpdate thrown DecoderException - decoder created by Platform'(){
        given:
        def decoder = aDecoder(definedByPlatform: Boolean.TRUE, organizationId : null)

        when:
        decoderValidator.validateOnUpdate(decoder)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        0 * _
        thrown(DecoderException)

        where:
        role << [Role.PREMIUM_USER, Role.FREE_USER, Role.PARTNER_USER, Role.PARTNER_ADMIN]
    }

    @Unroll
    def 'validateOnUpdate thrown DecoderException - unauthorized action'(){
        given:
        def userOrganizationId = 2L
        def decoderCreatedByOrganization = 1L
        def decoder = aDecoder(organizationId : decoderCreatedByOrganization)

        when:
        decoderValidator.validateOnUpdate(decoder)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        1 * authenticatedUserInfo.getOrganizationId() >> userOrganizationId
        0 * _
        thrown(DecoderException)

        where:
        role << [Role.PREMIUM_USER, Role.FREE_USER, Role.PARTNER_USER, Role.PARTNER_ADMIN]

    }

    @Unroll
    def 'validateOnDelete'(){
        given:
        def decoder = aDecoder()

        when:
        decoderValidator.validateOnDelete(decoder)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        getOrganizationIdCalls * authenticatedUserInfo.getOrganizationId() >> decoder.getOrganizationId()
        0 * _

        where:
        role               | getOrganizationIdCalls
        Role.ADMIN         | 0
        Role.PREMIUM_USER  | 1
        Role.FREE_USER     | 1
        Role.PARTNER_USER  | 1
        Role.PARTNER_ADMIN | 1
    }

    @Unroll
    def 'validateOnDelete thrown DecoderException - unauthorized action'(){
        given:
        def decoder = aDecoder(organizationId : 1L)

        when:
        decoderValidator.validateOnDelete(decoder)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        1 * authenticatedUserInfo.getOrganizationId() >> organizationId
        0 * _
        thrown(DecoderException)

        where:
        role << [Role.PREMIUM_USER, Role.FREE_USER, Role.PARTNER_USER, Role.PARTNER_ADMIN]
        organizationId = 2L
    }

    @Unroll
    def 'validateOnDelete thrown DecoderException - decoder defined by platform'(){
        given:
        def decoder = aDecoder(definedByPlatform : Boolean.TRUE, organizationId : null)

        when:
        decoderValidator.validateOnDelete(decoder)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        0 * _
        thrown(DecoderException)

        where:
        role << [Role.PREMIUM_USER, Role.FREE_USER, Role.PARTNER_USER, Role.PARTNER_ADMIN]
    }

    @Unroll
    def 'validateOnGet'(){
        given:
        def userOrganizationId = 1L
        def decoder = aDecoder(definedByPlatform : Boolean.FALSE, organizationId : organizationId)

        when:
        decoderValidator.validateOnGet(decoder)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        1 * authenticatedUserInfo.getOrganizationId() >> userOrganizationId
        0 * _

        where:
        role              | definedByPlatforme | organizationId
        Role.PREMIUM_USER | true               | 1L
        Role.FREE_USER    | true               | 1L
        Role.PARTNER_USER | true               | 1L
        Role.ADMIN        | true               | null
        Role.ADMIN        | false              | 1L
    }

    @Unroll
    def 'validateOnGet thrown DecoderException - unauthorized action'(){
        given:
        def organizationId = 2L
        def userOrganizationId = 1L
        def decoder = aDecoder(definedByPlatform : Boolean.FALSE, organizationId : organizationId)

        when:
        decoderValidator.validateOnGet(decoder)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        1 * authenticatedUserInfo.getOrganizationId() >> userOrganizationId
        0 * _
        thrown(DecoderException)

        where:
        role << [Role.PREMIUM_USER, Role.FREE_USER, Role.PARTNER_USER]
    }

}
