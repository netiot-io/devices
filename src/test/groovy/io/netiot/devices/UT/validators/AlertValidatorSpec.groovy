package io.netiot.devices.UT.validators

import io.netiot.devices.exceptions.InternalServerErrorException
import io.netiot.devices.exceptions.UnauthorizedOperationException
import io.netiot.devices.services.helpers.DeviceHelperService
import io.netiot.devices.utils.AuthenticatedUserInfo
import io.netiot.devices.validators.AlertValidator
import spock.lang.Specification
import spock.lang.Unroll

import static io.netiot.devices.generators.AlertGenerator.aAlertEvent

class AlertValidatorSpec extends Specification {

    def authenticatedUserInfo = Mock(AuthenticatedUserInfo)
    def deviceHelperService = Mock(DeviceHelperService)

    def alertValidator = new AlertValidator(authenticatedUserInfo, deviceHelperService)

    @Unroll
    def 'validateAlert'() {
        when:
        def result = alertValidator.validateAlertEvent(alertEvent)

        then:
        result == expectedResult

        where:
        expectedResult | alertEvent
        true  | aAlertEvent()
        false | aAlertEvent(deviceId: null)
        false | aAlertEvent(code : null)
        false | aAlertEvent(timestamp : null)
    }

    def 'validateOnGet'() {
        given:
        def deviceId = 1L
        def userId = 1L

        when:
        alertValidator.validateOnGet(deviceId)

        then:
        1 * authenticatedUserInfo.getId() >> userId
        1 * deviceHelperService.checkAuthorization(deviceId, userId) >> true
        0 * _
    }

    def 'validateOnGet thrown UnauthorizedOperationException'() {
        given:
        def deviceId = 1L
        def userId = 2L

        when:
        alertValidator.validateOnGet(deviceId)

        then:
        1 * authenticatedUserInfo.getId() >> userId
        1 * deviceHelperService.checkAuthorization(deviceId, userId) >> false
        0 * _
        thrown(UnauthorizedOperationException)
    }
}
