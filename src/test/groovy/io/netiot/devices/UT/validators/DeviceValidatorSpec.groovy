package io.netiot.devices.UT.validators

import io.netiot.devices.entities.Role
import io.netiot.devices.exceptions.DeviceException
import io.netiot.devices.repositories.DeviceRepository
import io.netiot.devices.utils.AuthenticatedUserInfo
import io.netiot.devices.validators.DeviceValidator
import spock.lang.Specification
import spock.lang.Unroll

import static io.netiot.devices.generators.DeviceGenerator.aDevice
import static io.netiot.devices.generators.ProfileGenerator.aProfile


class DeviceValidatorSpec extends Specification {

    def authenticatedUserInfo = Mock(AuthenticatedUserInfo)

    def deviceValidator = new DeviceValidator(authenticatedUserInfo)

    @Unroll
    def 'validateOnGet'() {
        given:
        def userId = 1L
        def device = aDevice()

        when:
        deviceValidator.validateOnGet(device)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        getUserIdCalls * authenticatedUserInfo.getId() >> userId
        0 * _

        where:
        role              | getUserIdCalls
        Role.PREMIUM_USER | 1
        Role.FREE_USER    | 1
        Role.PARTNER_USER | 1
        Role.ADMIN        | 0
    }

    @Unroll
    def 'validateOnGet thrown DeviceException - unauthorized action'() {
        given:
        def userId = 3L
        def device = aDevice()

        when:
        deviceValidator.validateOnGet(device)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        1 * authenticatedUserInfo.getId() >> userId
        0 * _
        thrown(DeviceException)

        where:
        role << [ Role.PREMIUM_USER, Role.FREE_USER, Role.PARTNER_USER, Role.PARTNER_ADMIN ]
    }

    @Unroll
    def 'validateOnSave'() {
        given:
        def device = aDevice()
        def userId = 1L
        def userOrganizationId = 1L

        when:
        deviceValidator.validateOnSave(device)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        getUserIdCalls * authenticatedUserInfo.getId() >> userId
        1 * authenticatedUserInfo.getOrganizationId() >> userOrganizationId
        0 * _

        where:
        role               | getUserIdCalls
        Role.ADMIN         | 0
        Role.PREMIUM_USER  | 1
        Role.FREE_USER     | 1
        Role.PARTNER_USER  | 1
        Role.PARTNER_ADMIN | 1
    }

    @Unroll
    def 'validateOnSave thrown DeviceException - unauthorized action'() {
        given:
        def userId = 1L
        def userOrganizationId = 2L
        def device = aDevice()

        when:
        deviceValidator.validateOnSave(device)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        1 * authenticatedUserInfo.getId() >> userId
        1 * authenticatedUserInfo.getOrganizationId() >> userOrganizationId
        0 * _
        thrown(DeviceException)

        where:
        role << [Role.PREMIUM_USER, Role.FREE_USER, Role.PARTNER_USER, Role.PARTNER_ADMIN]
    }

    @Unroll
    def 'validateOnSave thrown DeviceException - deviceType or Profiler owned by other organization'() {
        given:
        def userId = 1L
        def profileOrganizationId = 2L
        def userOrganizationId = 1L
        def profile = aProfile(organizationId: profileOrganizationId)
        def device = aDevice(profile: profile)

        when:
        deviceValidator.validateOnSave(device)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        getUserIdCalls * authenticatedUserInfo.getId() >> userId
        1 * authenticatedUserInfo.getOrganizationId() >> userOrganizationId
        0 * _
        thrown(DeviceException)

        where:
        role               | getUserIdCalls
        Role.ADMIN         | 0
        Role.PREMIUM_USER  | 1
        Role.FREE_USER     | 1
        Role.PARTNER_USER  | 1
        Role.PARTNER_ADMIN | 1
    }

    @Unroll
    def 'validateOnUpdate'() {
        given:
        def userId = 1L
        def device = aDevice()
        def userOrganizationId = 1L

        when:
        deviceValidator.validateOnUpdate(device)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        getUserIdCalls * authenticatedUserInfo.getId() >> userId
        1 * authenticatedUserInfo.getOrganizationId() >> userOrganizationId
        0 * _

        where:
        role               | getUserIdCalls
        Role.ADMIN         | 0
        Role.PREMIUM_USER  | 1
        Role.FREE_USER     | 1
        Role.PARTNER_USER  | 1
        Role.PARTNER_ADMIN | 1
    }

    @Unroll
    def 'validateOnUpdate thrown DeviceException - unauthorized action'() {
        given:
        def userId = 1L
        def userOrganizationId = 2L
        def device = aDevice()

        when:
        deviceValidator.validateOnUpdate(device)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        1 * authenticatedUserInfo.getId() >> userId
        1 * authenticatedUserInfo.getOrganizationId() >> userOrganizationId
        0 * _
        thrown(DeviceException)

        where:
        role << [Role.PREMIUM_USER, Role.FREE_USER, Role.PARTNER_USER, Role.PARTNER_ADMIN]
    }

    @Unroll
    def 'validateOnUpdate thrown DeviceException - deviceType or Profiler owned by other organization'() {
        given:
        def userId = 1L
        def profileOrganizationId = 2L
        def userOrganizationId = 1L
        def profile = aProfile(organizationId: profileOrganizationId)
        def device = aDevice(profile: profile)

        when:
        deviceValidator.validateOnUpdate(device)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        getUserIdCalls * authenticatedUserInfo.getId() >> userId
        1 * authenticatedUserInfo.getOrganizationId() >> userOrganizationId
        0 * _
        thrown(DeviceException)

        where:
        role               | getUserIdCalls
        Role.ADMIN         | 0
        Role.PREMIUM_USER  | 1
        Role.FREE_USER     | 1
        Role.PARTNER_USER  | 1
        Role.PARTNER_ADMIN | 1
    }

    @Unroll
    def 'validateOnDelete'() {
        given:
        def userId = 1L
        def device = aDevice()

        when:
        deviceValidator.validateOnDelete(device)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        getUserIdCalls * authenticatedUserInfo.getId() >> userId
        0 * _

        where:
        role              | getUserIdCalls
        Role.PREMIUM_USER | 1
        Role.FREE_USER    | 1
        Role.PARTNER_USER | 1
        Role.ADMIN        | 0
    }

    @Unroll
    def 'validateOnDelete thrown DeviceException - unauthorized action'() {
        given:
        def userId = 3L
        def device = aDevice()

        when:
        deviceValidator.validateOnDelete(device)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        1 * authenticatedUserInfo.getId() >> userId
        0 * _
        thrown(DeviceException)

        where:
        role << [ Role.PREMIUM_USER, Role.FREE_USER, Role.PARTNER_USER, Role.PARTNER_ADMIN ]
    }

}
