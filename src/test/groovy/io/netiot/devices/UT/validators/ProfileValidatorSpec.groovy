package io.netiot.devices.UT.validators

import io.netiot.devices.entities.Role
import io.netiot.devices.exceptions.ProfileException
import io.netiot.devices.services.DeviceService
import io.netiot.devices.utils.AuthenticatedUserInfo
import io.netiot.devices.validators.ProfileValidator
import spock.lang.Specification
import spock.lang.Unroll

import static io.netiot.devices.generators.ProfileGenerator.aProfile

class ProfileValidatorSpec extends Specification{

    def authenticatedUserInfo = Mock(AuthenticatedUserInfo)
    def deviceService = Mock(DeviceService)

    def profileValidator = new ProfileValidator(authenticatedUserInfo)

    @Unroll
    def 'validateOnUpdate'() {
        given:
        def profile = aProfile()
        def userOrganizationId = profile.getOrganizationId()

        when:
        profileValidator.validateOnUpdate(profile)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        getOrganizationIdCalls * authenticatedUserInfo.getOrganizationId() >> userOrganizationId
        0 * _

        where:
        role               | getOrganizationIdCalls
        Role.ADMIN         | 0
        Role.PREMIUM_USER  | 1
        Role.FREE_USER     | 1
        Role.PARTNER_USER  | 1
        Role.PARTNER_ADMIN | 1
    }

    @Unroll
    def 'validateOnUpdate thrown ProfileException - unauthorized action'() {
        given:
        def userOrganizationId = 2L
        def profileCreatedByOrganization = 1L
        def profile = aProfile(organizationId : profileCreatedByOrganization)

        when:
        profileValidator.validateOnUpdate(profile)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        1 * authenticatedUserInfo.getOrganizationId() >> userOrganizationId
        0 * _
        thrown(ProfileException)

        where:
        role << [Role.PREMIUM_USER, Role.FREE_USER, Role.PARTNER_USER, Role.PARTNER_ADMIN]

    }

    @Unroll
    def 'validateOnGet'() {
        given:
        def userOrganizationId = 1L
        def profileOrganizationId = 1L
        def profile = aProfile(organizationId : profileOrganizationId)

        when:
        profileValidator.validateOnGet(profile)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        getOrganizationIdCalls * authenticatedUserInfo.getOrganizationId() >> userOrganizationId
        0 * _

        where:
        role               | getOrganizationIdCalls
        Role.PREMIUM_USER  | 1
        Role.FREE_USER     | 1
        Role.PARTNER_USER  | 1
        Role.PARTNER_ADMIN | 1
        Role.ADMIN         | 0
    }

    @Unroll
    def 'validateOnGet thrown ProfileException - unauthorized action'() {
        given:
        def userOrganizationId = 1L
        def profileOrganizationId = 2L
        def profile = aProfile(organizationId : userOrganizationId)

        when:
        profileValidator.validateOnGet(profile)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        1 * authenticatedUserInfo.getOrganizationId() >> profileOrganizationId
        0 * _
        thrown(ProfileException)

        where:
        role << [ Role.PREMIUM_USER, Role.FREE_USER, Role.PARTNER_USER, Role.PARTNER_ADMIN ]
    }

    @Unroll
    def 'validateOnSave'() {
        given:
        def profile = aProfile()

        when:
        profileValidator.validateOnSave(profile)

        then:
        1 *  authenticatedUserInfo.getRole() >> role
        0 * _

        where:
        role << [ Role.PREMIUM_USER, Role.FREE_USER, Role.PARTNER_USER, Role.PARTNER_ADMIN ]
    }

    def 'validateOnSave thrown ProfileException - unauthorized action'() {
        given:
        def role = Role.ADMIN
        def profile = aProfile()

        when:
        profileValidator.validateOnSave(profile)

        then:
        1 *  authenticatedUserInfo.getRole() >> role
        0 * _
        thrown(ProfileException)
    }

    @Unroll
    def 'validateOnDelete'() {
        given:
        def userOrganizationId = 1L
        def profileOrganizationId = 1L
        def profile = aProfile(organizationId : userOrganizationId)

        when:
        profileValidator.validateOnDelete(profile)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        getOrganizationIdCalls * authenticatedUserInfo.getOrganizationId() >> profileOrganizationId
        0 * _

        where:
        role              | getOrganizationIdCalls
        Role.PREMIUM_USER | 1
        Role.FREE_USER    | 1
        Role.PARTNER_USER | 1
        Role.ADMIN        | 0
    }

    @Unroll
    def 'validateOnDelete thrown ProfileException - unauthorized action'() {
        given:
        def userOrganizationId = 1L
        def profileOrganizationId = 2L
        def profile = aProfile(organizationId : userOrganizationId)

        when:
        profileValidator.validateOnDelete(profile)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        1 * authenticatedUserInfo.getOrganizationId() >> profileOrganizationId
        0 * _
        thrown(ProfileException)

        where:
        role << [ Role.PREMIUM_USER, Role.FREE_USER, Role.PARTNER_USER ]
    }

}