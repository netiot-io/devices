package io.netiot.devices.UT.mappers

import io.netiot.devices.mappers.ProtocolMapper
import spock.lang.Specification

import static io.netiot.devices.generators.ProtocolGenerator.aProtocol
import static io.netiot.devices.generators.ProtocolGenerator.aProtocolListModel
import static io.netiot.devices.generators.ProtocolGenerator.aProtocolModel

class ProtocolMapperSpec extends Specification{

    def 'toModel'() {
        given:
        def entity = aProtocol()
        def model = aProtocolModel()

        when:
        def result = ProtocolMapper.toModel(entity)

        then:
        result == model
    }

    def 'toListModel'(){
        given:
        def models = [ aProtocolModel() ]


        when:
        def result = ProtocolMapper.toListModel(models)

        then:
        result == aProtocolListModel()
    }

}
