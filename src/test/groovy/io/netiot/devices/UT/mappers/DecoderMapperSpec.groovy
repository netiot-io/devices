package io.netiot.devices.UT.mappers

import io.netiot.devices.mappers.DecoderMapper
import spock.lang.Specification

import static io.netiot.devices.generators.DecoderGenerator.aDecoder
import static io.netiot.devices.generators.DecoderGenerator.aDecoderListModel
import static io.netiot.devices.generators.DecoderGenerator.aDecoderModel

class DecoderMapperSpec extends Specification{

    def 'toModel'() {
        given:
        def entity = aDecoder()
        def model = aDecoderModel()

        when:
        def result = DecoderMapper.toModel(entity)

        then:
        result == model
    }

    def 'toEntity'() {
        given:
        def entity = aDecoder(protocol : null, definedByPlatform : false, organizationId : 1, fields : ["field1", "field2", "field3"])
        def model = aDecoderModel()

        when:
        def result = DecoderMapper.toEntity(model)

        then:
        result == entity
    }

    def 'toListModel'(){
        given:
        def models = [ aDecoderModel() ]


        when:
        def result = DecoderMapper.toListModel(models)

        then:
        result == aDecoderListModel()
    }

}
