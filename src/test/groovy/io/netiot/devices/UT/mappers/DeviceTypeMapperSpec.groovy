package io.netiot.devices.UT.mappers

import io.netiot.devices.mappers.DeviceTypeMapper
import spock.lang.Specification

import static io.netiot.devices.generators.DeviceTypeGenerator.*

class DeviceTypeMapperSpec extends Specification{

    def 'toModel'() {
        given:
        def entity = aDeviceType()
        def model = aDeviceTypeModel()

        when:
        def result = DeviceTypeMapper.toModel(entity)

        then:
        result == model
    }

    def 'toEntity'() {
        given:
        def entity = aDeviceType(protocol : null, decoder: null, definedByPlatform : null, organizationId : null)
        def model = aDeviceTypeModel()

        when:
        def result = DeviceTypeMapper.toEntity(model)

        then:
        result == entity
    }

    def 'toListModel'(){
        given:
        def models = [ aDeviceTypeModel() ]


        when:
        def result = DeviceTypeMapper.toListModel(models)

        then:
        result == aDeviceTypeListModel()
    }

}
