package io.netiot.devices.UT.mappers

import io.netiot.devices.mappers.ProfileMapper
import spock.lang.Specification

import static io.netiot.devices.generators.ProfileGenerator.aProfile
import static io.netiot.devices.generators.ProfileGenerator.aProfileListModel
import static io.netiot.devices.generators.ProfileGenerator.aProfileModel

class ProfileMapperSpec extends Specification{

    def 'toModel'() {
        given:
        def entity = aProfile()
        def model = aProfileModel()

        when:
        def result = ProfileMapper.toModel(entity)

        then:
        result == model
    }

    def 'toEntity'() {
        given:
        def entity = aProfile("deviceType" : null, organizationId : null)
        def model = aProfileModel()

        when:
        def result = ProfileMapper.toEntity(model)

        then:
        result == entity
    }

    def 'toListModel'(){
        given:
        def models = [ aProfileModel() ]


        when:
        def result = ProfileMapper.toListModel(models)

        then:
        result == aProfileListModel()
    }

}
