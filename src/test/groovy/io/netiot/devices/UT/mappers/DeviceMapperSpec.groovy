package io.netiot.devices.UT.mappers

import io.netiot.devices.mappers.DeviceMapper
import io.netiot.devices.models.events.EventType
import spock.lang.Specification

import static io.netiot.devices.generators.DeviceGenerator.aDevice
import static io.netiot.devices.generators.DeviceGenerator.aDeviceApplicationModel
import static io.netiot.devices.generators.DeviceGenerator.aDeviceEvent
import static io.netiot.devices.generators.DeviceGenerator.aDeviceListModel
import static io.netiot.devices.generators.DeviceGenerator.aDeviceModel
import static io.netiot.devices.generators.DeviceGenerator.aDeviceProperties

class DeviceMapperSpec extends Specification{

    def 'toModel'() {
        given:
        def entity = aDevice()
        def model = aDeviceModel()

        when:
        def result = DeviceMapper.toModel(entity)

        then:
        result == model
    }

    def 'toEntity'() {
        given:
        def entity = aDevice(profile: null, deviceType: null)
        def model = aDeviceModel()

        when:
        def result = DeviceMapper.toEntity(model)

        then:
        result == entity
    }

    def 'toListModel'(){
        given:
        def models = [ aDeviceModel() ]


        when:
        def result = DeviceMapper.toListModel(models)

        then:
        result == aDeviceListModel()
    }

    def 'toDevicePropertiesModel'(){
        given:
        def entity = aDevice()
        def devicePropertiesModel = aDeviceProperties()

        when:
        def result = DeviceMapper.toDevicePropertiesModel(entity)

        then:
        result == devicePropertiesModel
    }

    def 'toDeviceEvent'(){
        given:
        def entity = aDevice()
        def eventType = EventType.CREATED
        def devicePropertiesModel = aDeviceEvent(eventType: eventType)

        when:
        def result = DeviceMapper.toDeviceEvent(entity, eventType)

        then:
        result == devicePropertiesModel
    }

    def 'toApplicationModel'() {
        given:
        def entity = aDevice()
        def model = aDeviceApplicationModel()

        when:
        def result = DeviceMapper.toApplicationModel(entity)

        then:
        result == model
    }

}
