package io.netiot.devices.configurations;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface KafkaChannels {

    String ALERTS_INPUT = "decoders-alerts";

    @Input(KafkaChannels.ALERTS_INPUT)
    SubscribableChannel alerts();

}
