package io.netiot.devices.validators;

import io.netiot.devices.entities.Decoder;
import io.netiot.devices.entities.DeviceType;
import io.netiot.devices.entities.Role;
import io.netiot.devices.exceptions.DeviceTypeException;
import io.netiot.devices.repositories.DeviceTypeRepository;
import io.netiot.devices.utils.AuthenticatedUserInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static io.netiot.devices.utils.ErrorMessages.*;

@Slf4j
@Component
@RequiredArgsConstructor
public class DeviceTypeValidator {

    private final AuthenticatedUserInfo authenticatedUserInfo;
    private final DeviceTypeRepository deviceTypeRepository;

    public void validateOnGet(final DeviceType deviceType) {
        Role userRole = authenticatedUserInfo.getRole();

        if (deviceType.getDefinedByPlatform() != Boolean.TRUE) {
            Long userOrganizationId = authenticatedUserInfo.getOrganizationId();
            if (userRole != Role.ADMIN && !userOrganizationId.equals(deviceType.getOrganizationId())) {
                throw new DeviceTypeException(UNAUTHORIZED_ACTION_ERROR_CODE);
            }
        }
    }

    public void validateOnSave(final DeviceType deviceType) {
        deviceTypeRepository.findByName(deviceType.getName())
                .ifPresent( decoder1 -> { throw new DeviceTypeException(DEVICE_TYPE_ALREADY_EXIST_ERROR_CODE); });

        checkDecoderIfCanBeUsed(deviceType);
    }

    public void validateOnUpdate(final DeviceType deviceType) {
        checkOperationPermission(deviceType);
        checkDecoderIfCanBeUsed(deviceType);
        deviceTypeRepository.findByName(deviceType.getName()).ifPresent((foundDecoder) -> {
            if(!foundDecoder.getId().equals(deviceType.getId())){
                throw new DeviceTypeException(DEVICE_TYPE_NAME_ALREADY_EXIST_ERROR_CODE);
            }
        });
    }

    public void validateOnDelete(final DeviceType deviceType) {
        checkOperationPermission(deviceType);
    }

    private void checkOperationPermission(final DeviceType deviceType) {
        final Role userRole = authenticatedUserInfo.getRole();
        if(userRole != Role.ADMIN){
            if(deviceType.getDefinedByPlatform()){
                throw new DeviceTypeException(DEVICE_TYPE_DEFINED_BY_PLATFORM_ERROR_CODE);
            }

            Long userOrganizationId = authenticatedUserInfo.getOrganizationId();
            if(!userOrganizationId.equals(deviceType.getOrganizationId())){
                throw new DeviceTypeException(UNAUTHORIZED_ACTION_ERROR_CODE);
            }
        }
    }

    private void checkDecoderIfCanBeUsed(final DeviceType deviceType) {
        Decoder decoder = deviceType.getDecoder();
        if(!decoder.getDefinedByPlatform()) {
            if (deviceType.getDefinedByPlatform()) {
                throw new DeviceTypeException(DEVICE_TYPE_NOT_DEFINED_BY_PLATFORM_ERROR_CODE);
            } else {
                Boolean isDecoderDefinedByOtherOrganization = !decoder.getOrganizationId().equals(deviceType.getOrganizationId());
                if (!decoder.getDefinedByPlatform() && isDecoderDefinedByOtherOrganization) {
                    throw new DeviceTypeException(DEVICE_TYPE_DEFINED_BY_OTHER_ORGANIZATION_ERROR_CODE);
                }
            }
        }
    }

}
