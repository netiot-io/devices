package io.netiot.devices.validators;

import io.netiot.devices.entities.Device;
import io.netiot.devices.entities.Role;
import io.netiot.devices.exceptions.DeviceException;
import io.netiot.devices.utils.AuthenticatedUserInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static io.netiot.devices.utils.ErrorMessages.*;

@Slf4j
@Component
@RequiredArgsConstructor
public class DeviceValidator {

    private final AuthenticatedUserInfo authenticatedUserInfo;

    public void validateOnGet(final Device device) {
        checkOperationPermission(device);
    }

    public void validateOnSave(final Device device) {
        checkOperationPermission(device);
        checkDeviceTypeAndProfileOrganizationOwner(device);
    }

    public void validateOnUpdate(final Device device) {
        checkOperationPermission(device);
        checkDeviceTypeAndProfileOrganizationOwner(device);
    }

    public void validateOnDelete(final Device device) {
        checkOperationPermission(device);
    }

    private void checkDeviceTypeAndProfileOrganizationOwner(final Device device) {
        Long deviceTypeOrganizationId = device.getDeviceType()
                .getOrganizationId();
        Long profileOrganizationId = device.getProfile()
                .getOrganizationId();
        Long deviceOrganizationId = authenticatedUserInfo.getOrganizationId();
        boolean deviceOrganizationIdAreSameWithDeviceTypeAndProfileOrganizationOwner = deviceTypeOrganizationId.equals(deviceOrganizationId)
                && profileOrganizationId.equals(deviceOrganizationId);
        if(!deviceOrganizationIdAreSameWithDeviceTypeAndProfileOrganizationOwner){
            throw new DeviceException(DEVICE_TYPE_OR_PROFILE_OWNED_BY_OTHER_ORGANIZATION_ERROR_CODE);
        }
    }

    private void checkOperationPermission(final Device device) {
        final Role userRole = authenticatedUserInfo.getRole();
        if(userRole != Role.ADMIN){
            Long userId = authenticatedUserInfo.getId();
            if (device.getUsers().stream().noneMatch(user -> user.getUserId().compareTo(userId) == 0)) {
                throw new DeviceException(UNAUTHORIZED_ACTION_ERROR_CODE);
            }
        }
    }
}
