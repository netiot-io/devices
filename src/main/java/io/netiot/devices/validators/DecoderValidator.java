package io.netiot.devices.validators;

import io.netiot.devices.entities.Decoder;
import io.netiot.devices.entities.Role;
import io.netiot.devices.exceptions.DecoderException;
import io.netiot.devices.repositories.DecoderRepository;
import io.netiot.devices.services.helpers.DeviceHelperService;
import io.netiot.devices.utils.AuthenticatedUserInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static io.netiot.devices.utils.ErrorMessages.*;

@Slf4j
@Component
@RequiredArgsConstructor
public class DecoderValidator {

    private final AuthenticatedUserInfo authenticatedUserInfo;
    private final DecoderRepository decoderRepository;


    public void validateOnGet(final Decoder decoder) {
        Role userRole = authenticatedUserInfo.getRole();

        if (decoder.getDefinedByPlatform() != Boolean.TRUE) {
            Long userOrganizationId = authenticatedUserInfo.getOrganizationId();
            if (userRole != Role.ADMIN && !userOrganizationId.equals(decoder.getOrganizationId())) {
                throw new DecoderException(UNAUTHORIZED_ACTION_ERROR_CODE);
            }
        }
    }

    public void validateOnSave(final Decoder decoder) {
        decoderRepository.findByName(decoder.getName())
                .ifPresent( decoder1 -> { throw new DecoderException(DECODER_ALREADY_EXIST_ERROR_CODE); });
    }

    public void validateOnUpdate(final Decoder decoder) {
        //TODO check if a decoder its used already by one or more devices
        checkOperationPermission(decoder);
        decoderRepository.findByName(decoder.getName()).ifPresent((foundDecoder) -> {
            if(!foundDecoder.getId().equals(decoder.getId())){
                throw new DecoderException(DECODER_NAME_ALREADY_EXIST_ERROR_CODE);
            }
        });
    }

    public void validateOnDelete(final Decoder decoder) {
        //TODO check if a decoder its used already by one or more devices
        checkOperationPermission(decoder);
    }

    private void checkOperationPermission(final Decoder decoder) {
        final Role userRole = authenticatedUserInfo.getRole();
        if(userRole != Role.ADMIN){
            if(decoder.getDefinedByPlatform()){
                throw new DecoderException(DECODER_DEFINED_BY_PLATFORM_ERROR_CODE);
            }

            Long userOrganizationId = authenticatedUserInfo.getOrganizationId();
            if(!userOrganizationId.equals(decoder.getOrganizationId())){
                throw new DecoderException(UNAUTHORIZED_ACTION_ERROR_CODE);
            }
        }
    }

}
