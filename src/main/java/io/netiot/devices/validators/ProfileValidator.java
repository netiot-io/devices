package io.netiot.devices.validators;

import io.netiot.devices.entities.Profile;
import io.netiot.devices.entities.Role;
import io.netiot.devices.exceptions.ProfileException;
import io.netiot.devices.utils.AuthenticatedUserInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static io.netiot.devices.utils.ErrorMessages.*;

@Slf4j
@Component
@RequiredArgsConstructor
public class ProfileValidator {

    private final AuthenticatedUserInfo authenticatedUserInfo;

    public void validateOnGet(final Profile profile) {
        checkOperationPermission(profile);
    }

    public void validateOnSave(final Profile profile) {
        final Role userRole = authenticatedUserInfo.getRole();
        if(userRole == Role.ADMIN) {
            throw new ProfileException(UNAUTHORIZED_ACTION_ERROR_CODE);
        }
    }

    public void validateOnUpdate(final Profile profile) {
        checkOperationPermission(profile);
    }

    public void validateOnDelete(final Profile profile) {
        checkOperationPermission(profile);
        //TODO Check if profile are used by active devices
    }

    private void checkOperationPermission(final Profile profile) {
        final Role userRole = authenticatedUserInfo.getRole();
        if(userRole != Role.ADMIN){
            Long userOrganizationId = authenticatedUserInfo.getOrganizationId();
            if(!userOrganizationId.equals(profile.getOrganizationId())){
                throw new ProfileException(UNAUTHORIZED_ACTION_ERROR_CODE);
            }
        }
    }

}
