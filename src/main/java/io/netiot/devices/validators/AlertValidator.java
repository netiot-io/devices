package io.netiot.devices.validators;

import io.netiot.devices.exceptions.UnauthorizedOperationException;
import io.netiot.devices.models.events.AlertEvent;
import io.netiot.devices.services.helpers.DeviceHelperService;
import io.netiot.devices.utils.AuthenticatedUserInfo;
import io.netiot.devices.utils.ErrorMessages;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class AlertValidator {

    private final AuthenticatedUserInfo authenticatedUserInfo;
    private final DeviceHelperService deviceHelperService;


    public boolean validateAlertEvent(final AlertEvent alertEvent){
        return alertEvent.getDeviceId() != null && alertEvent.getTimestamp() != null
                && alertEvent.getCode() != null && !alertEvent.getCode().isEmpty();
    }

    public void validateOnGet(final long deviceId) {
        boolean deviceAuthorized = deviceHelperService.checkAuthorization(deviceId, authenticatedUserInfo.getId());
        if(!deviceAuthorized){
            throw new UnauthorizedOperationException(ErrorMessages.UNAUTHORIZED_ACTION_ERROR_CODE);
        }
    }
}
