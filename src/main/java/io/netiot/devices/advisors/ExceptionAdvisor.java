package io.netiot.devices.advisors;

import io.netiot.devices.exceptions.*;
import io.netiot.devices.models.ErrorModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;

@RestControllerAdvice
public final class ExceptionAdvisor {

    @ExceptionHandler(value = {BindingResultException.class})
    public ResponseEntity errorHandle(final BindingResultException bindingResultRuntimeException) {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ErrorModel
                        .builder()
                        .error(bindingResultRuntimeException.getErrors())
                        .build());
    }

    @ExceptionHandler(value = {UserException.class})
    public ResponseEntity errorHandle(final UserException userException) {
        HashMap<String, String> body = new HashMap<>();
        body.put("error", userException.getMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);
    }

    @ExceptionHandler(value = {DecoderException.class})
    public ResponseEntity errorHandle(final DecoderException decoderException) {
        HashMap<String, String> body = new HashMap<>();
        body.put("error", decoderException.getMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);
    }

    @ExceptionHandler(value = {DeviceException.class})
    public ResponseEntity errorHandle(final DeviceException deviceException) {
        HashMap<String, String> body = new HashMap<>();
        body.put("error", deviceException.getMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);
    }

    @ExceptionHandler(value = {DeviceTypeException.class})
    public ResponseEntity errorHandle(final DeviceTypeException deviceTypeException) {
        HashMap<String, String> body = new HashMap<>();
        body.put("error", deviceTypeException.getMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);
    }

    @ExceptionHandler(value = {JwtException.class})
    public ResponseEntity errorHandle(final JwtException jwtException) {
        HashMap<String, String> body = new HashMap<>();
        body.put("error", jwtException.getMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);
    }

    @ExceptionHandler(value = {ProfileException.class})
    public ResponseEntity errorHandle(final ProfileException profileException) {
        HashMap<String, String> body = new HashMap<>();
        body.put("error", profileException.getMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);
    }

    @ExceptionHandler(value = {ProtocolException.class})
    public ResponseEntity errorHandle(final ProtocolException protocolException) {
        HashMap<String, String> body = new HashMap<>();
        body.put("error", protocolException.getMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);
    }

    @ExceptionHandler(value = {UnauthorizedOperationException.class})
    public ResponseEntity errorHandle(final UnauthorizedOperationException unauthorizedOperationException) {
        HashMap<String, String> body = new HashMap<>();
        body.put("error", unauthorizedOperationException.getMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);
    }

    @ExceptionHandler(value = {InternalServerErrorException.class})
    public ResponseEntity errorHandle(final InternalServerErrorException internalServerErrorException) {
        HashMap<String, String> body = new HashMap<>();
        body.put("error", internalServerErrorException.getMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);
    }

}