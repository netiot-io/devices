package io.netiot.devices.mappers;

import io.netiot.devices.entities.*;
import io.netiot.devices.models.*;
import io.netiot.devices.models.events.DeviceEvent;
import io.netiot.devices.models.events.EventType;
import io.netiot.devices.models.events.StatusEventType;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class DeviceMapper {

    private DeviceMapper() {
    }

    public static Device toEntity(final DeviceModel deviceModel) {
        Parameters parameters = Parameters.builder()
                .parametersMap(deviceModel.getDeviceParameters())
                .build();

        return Device.builder()
                .id(deviceModel.getId())
                .name(deviceModel.getName())
                .parameters(parameters)
                .activate(deviceModel.getActivate())
                .users(deviceModel.getUserIds().stream().map(userId -> DevicesUsers.builder().userId(userId).build()).collect(Collectors.toSet()))
                .build();
    }

    public static DeviceModel toModel(final Device device) {
        return DeviceModel.builder()
                .id(device.getId())
                .name(device.getName())
                .deviceParameters(device.getParameters().getParametersMap())
                .deviceTypeId(device.getDeviceType().getId())
                .profileId(device.getProfile().getId())
                .activate(device.getActivate())
                .userIds(device.getUsers().stream().map(DevicesUsers::getUserId).collect(Collectors.toList()))
                .build();
    }

    public static DeviceListModel toListModel(final List<DeviceModel> deviceModelList) {
        return DeviceListModel.builder()
                .devices(deviceModelList)
                .build();
    }

    public static DeviceProperties toDevicePropertiesModel(final Device device){
        return DeviceProperties.builder()
                .decoderCode(device.getDeviceType().getDecoder().getCode())
                .build();
    }

    public static DeviceEvent toDeviceEvent(final Device device, final EventType eventType){
        HashMap<String, String> parameters = new HashMap<>();
        Stream.of(device.getProfile().getParameters().getParametersMap(), device.getParameters().getParametersMap())
                .flatMap(map -> map.entrySet().stream())
                .forEach(entry -> parameters.put(entry.getKey(), entry.getValue()));

        StatusEventType statusEventType;
        if(device.getActivate()) {
            statusEventType = StatusEventType.ACTIVE;
        } else {
            statusEventType = StatusEventType.DISABLE;
        }

        return DeviceEvent.builder()
                .id(device.getId())
                .parameters(parameters)
                .eventType(eventType)
                .statusEventType(statusEventType)
                .build();
    }

    public static DeviceApplicationModel toApplicationModel(Device device) {
        return DeviceApplicationModel.builder()
                .id(device.getId())
                .name(device.getName())
                .decoderFields(device.getDeviceType().getDecoder().getFields())
                .deviceTypeId(device.getDeviceType().getId())
                .deviceTypeName(device.getDeviceType().getName())
                .build();
    }
}
