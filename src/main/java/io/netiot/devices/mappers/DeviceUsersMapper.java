package io.netiot.devices.mappers;

import io.netiot.devices.entities.Device;
import io.netiot.devices.entities.DevicesUsers;

import java.util.Set;
import java.util.stream.Collectors;

public final class DeviceUsersMapper {

    private DeviceUsersMapper() {
    }

    public static Set<DevicesUsers> toEntityList(final Set<Long> usersIds, final Device device) {
        return usersIds.stream().map(userId -> DevicesUsers.builder().userId(userId).device(device).build()).collect(Collectors.toSet());
    }

}