package io.netiot.devices.mappers;

import com.google.gson.Gson;
import io.netiot.devices.entities.Protocol;
import io.netiot.devices.models.ParameterModel;
import io.netiot.devices.models.ParametersWrapperModel;
import io.netiot.devices.models.ProtocolListModel;
import io.netiot.devices.models.ProtocolModel;

import java.util.List;
import java.util.Optional;

public final class ProtocolMapper {
    private static final Gson gson = new Gson();

    private ProtocolMapper() {
    }

    public static ProtocolModel toModel(final Protocol protocol) {
        List<ParameterModel> protocolParameters = Optional.ofNullable(protocol.getProtocolParameters())
                .map(protocolParametersWrapper -> gson.fromJson(protocolParametersWrapper ,ParametersWrapperModel.class)
                        .getParameterModelList())
                .orElse(null);
        List<ParameterModel> deviceParameters = Optional.ofNullable(protocol.getDeviceParameters())
                .map(deviceParametersWrapper -> gson.fromJson(deviceParametersWrapper ,ParametersWrapperModel.class)
                        .getParameterModelList())
                .orElse(null);

        return ProtocolModel.builder()
                .id(protocol.getId())
                .name(protocol.getName())
                .protocolParameters(protocolParameters)
                .deviceParameters(deviceParameters)
                .metadata(protocol.getMetadata())
                .build();
    }

    public static ProtocolListModel toListModel(final List<ProtocolModel> protocolModelList) {
        return ProtocolListModel.builder()
                .protocols(protocolModelList)
                .build();
    }

}
