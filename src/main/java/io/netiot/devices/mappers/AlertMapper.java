package io.netiot.devices.mappers;

import io.netiot.devices.entities.Alert;
import io.netiot.devices.entities.Decoder;
import io.netiot.devices.models.AlertModel;
import io.netiot.devices.models.DecoderListModel;
import io.netiot.devices.models.DecoderModel;
import io.netiot.devices.models.events.AlertEvent;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

public final class AlertMapper {

    private AlertMapper() {
    }

    public static Alert toEntity(final AlertEvent alertEvent) {
        LocalDateTime timestamp = Instant.ofEpochMilli(alertEvent.getTimestamp())
                .atZone(ZoneId.systemDefault()).toLocalDateTime();
        return Alert.builder()
                .deviceId(alertEvent.getDeviceId())
                .code(alertEvent.getCode())
                .timestamp(timestamp)
                .build();
    }

    public static AlertModel toModel(final Alert alert) {
        return AlertModel.builder()
                .id(alert.getId())
                .deviceId(alert.getDeviceId())
                .code(alert.getCode())
                .timestamp(alert.getTimestamp())
                .build();
    }

    public static DecoderListModel toListModel(final List<DecoderModel> decoderModelList) {
        return DecoderListModel.builder()
                .decoders(decoderModelList)
                .build();
    }

}
