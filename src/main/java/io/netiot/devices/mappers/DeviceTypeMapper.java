package io.netiot.devices.mappers;

import io.netiot.devices.entities.DeviceType;
import io.netiot.devices.models.DeviceTypeListModel;
import io.netiot.devices.models.DeviceTypeModel;

import java.util.List;

public final class DeviceTypeMapper {

    private DeviceTypeMapper() {
    }

    public static DeviceType toEntity(final DeviceTypeModel deviceTypeModel) {
        return DeviceType.builder()
                .id(deviceTypeModel.getId())
                .name(deviceTypeModel.getName())
                .description(deviceTypeModel.getDescription())
                .activate(deviceTypeModel.getActivate())
                .build();
    }

    public static DeviceTypeModel toModel(final DeviceType deviceType) {
        return DeviceTypeModel.builder()
                .id(deviceType.getId())
                .name(deviceType.getName())
                .description(deviceType.getDescription())
                .activate(deviceType.getActivate())
                .protocolId(deviceType.getProtocol().getId())
                .decoderId(deviceType.getDecoder().getId())
                .definedByPlatform(deviceType.getDefinedByPlatform())
                .organizationId(deviceType.getOrganizationId())
                .activate(deviceType.getActivate())
                .build();
    }

    public static DeviceTypeListModel toListModel(final List<DeviceTypeModel> deviceTypeModelList) {
        return DeviceTypeListModel.builder()
                .deviceTypes(deviceTypeModelList)
                .build();
    }

}
