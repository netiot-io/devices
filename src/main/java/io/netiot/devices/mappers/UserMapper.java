package io.netiot.devices.mappers;

import io.netiot.devices.entities.User;
import io.netiot.devices.models.UserInternalModel;

public final class UserMapper {

    private UserMapper() {
    }

    public static User toEntity(final UserInternalModel userInternalModel) {
        return User.builder()
                .id(userInternalModel.getId())
                .build();
    }

}