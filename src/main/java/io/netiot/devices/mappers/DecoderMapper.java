package io.netiot.devices.mappers;

import io.netiot.devices.entities.Decoder;
import io.netiot.devices.exceptions.DecoderException;
import io.netiot.devices.models.DecoderListModel;
import io.netiot.devices.models.DecoderModel;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static io.netiot.devices.utils.ErrorMessages.DECODER_FIELDS_ERROR_CODE;

public final class DecoderMapper {

    private DecoderMapper() {
    }

    public static Decoder toEntity(final DecoderModel decoderModel) {

        Pattern pattern = Pattern.compile("(const fields[( )*]=[( )*]\\[.*\\];)");
        Matcher matcher = pattern.matcher(decoderModel.getCode());
        if (matcher.find()) {
            String match = matcher.group(1);
            String[] fields;
            try {
                fields = match.substring(match.indexOf('[') + 1, match.length()-3).split("\\s*,\\s*");
            } catch (IndexOutOfBoundsException iob) {
                throw new DecoderException(DECODER_FIELDS_ERROR_CODE);
            }
            if (fields.length > 0) {
                List<String> processedFields = Stream.of(fields).map(field -> field.replaceAll("\"", "")
                        .replaceAll("'", "")).collect(Collectors.toList());

                return Decoder.builder()
                        .id(decoderModel.getId())
                        .name(decoderModel.getName())
                        .description(decoderModel.getDescription())
                        .fields(processedFields)
                        .code(decoderModel.getCode())
                        .definedByPlatform(decoderModel.getDefinedByPlatform())
                        .organizationId(decoderModel.getOrganizationId())
                        .build();
            }
        }
        throw new DecoderException(DECODER_FIELDS_ERROR_CODE);
    }

    public static DecoderModel toModel(final Decoder decoder) {
        return DecoderModel.builder()
                .id(decoder.getId())
                .name(decoder.getName())
                .description(decoder.getDescription())
                .code(decoder.getCode())
                .protocolId(decoder.getProtocol().getId())
                .definedByPlatform(decoder.getDefinedByPlatform())
                .organizationId(decoder.getOrganizationId())
                .build();
    }

    public static DecoderListModel toListModel(final List<DecoderModel> decoderModelList) {
        return DecoderListModel.builder()
                .decoders(decoderModelList)
                .build();
    }

}
