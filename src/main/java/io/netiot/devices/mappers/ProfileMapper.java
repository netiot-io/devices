package io.netiot.devices.mappers;

import io.netiot.devices.entities.Parameters;
import io.netiot.devices.entities.Profile;
import io.netiot.devices.models.ProfileListModel;
import io.netiot.devices.models.ProfileModel;

import java.util.List;

public final class ProfileMapper {

    private ProfileMapper() {
    }

    public static Profile toEntity(final ProfileModel profileModel) {
        Parameters parameters = Parameters.builder()
                .parametersMap(profileModel.getProtocolParameters())
                .build();

        return Profile.builder()
                .id(profileModel.getId())
                .name(profileModel.getName())
                .parameters(parameters)
                .build();
    }

    public static ProfileModel toModel(final Profile profile) {
        return ProfileModel.builder()
                .id(profile.getId())
                .name(profile.getName())
                .protocolParameters(profile.getParameters().getParametersMap())
                .organizationId(profile.getOrganizationId())
                .build();
    }

    public static ProfileListModel toListModel(final List<ProfileModel> profileModels) {
        return ProfileListModel.builder()
                .profiles(profileModels)
                .build();
    }

}
