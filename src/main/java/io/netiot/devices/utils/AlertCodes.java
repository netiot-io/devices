package io.netiot.devices.utils;

public class AlertCodes {

    public static final String DEVICE_ADDED_ACTIVE = "backend.devices.device.active";
    public static final String DEVICE_ADDED_INACTIVE = "backend.devices.device.inactive";
}
