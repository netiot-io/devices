package io.netiot.devices.utils;

public final class ErrorMessages {

    public static final String USER_NOT_EXIST_ERROR_CODE = "backend.devices.user.not.exist";

    public static final String UNAUTHORIZED_ACTION_ERROR_CODE = "backend.devices.unauthorized.action";

    public static final String INTERNAL_SERVER_ERROR_CODE = "backend.devices.internal.server.error";

    public static final String PROTOCOL_NOT_EXIST_ERROR_CODE = "backend.devices.protocol.not.exist";

    public static final String SENSOR_TYPE_NOT_ALL_SENSOR_TYPES_ARE_DEFINED_ERROR_CODE = "backend.devices.sensorType.not.all.sensorsType.are.defined";

    public static final String DECODER_PROTOCOL_FIELD_ERROR_CODE = "backend.devices.decoder.protocol.notnull.not.empty";
    public static final String DECODER_NAME_FIELD_ERROR_CODE = "backend.devices.decoder.name.notnull.not.empty";
    public static final String DECODER_FIELDS_ERROR_CODE = "backend.devices.decoder.fields.notnull.not.empty";
    public static final String DECODER_FIELD_ERROR_CODE = "backend.devices.decoder.field.notnull.not.empty";
    public static final String DECODER_ALREADY_EXIST_ERROR_CODE = "backend.devices.decoder.already.exist";
    public static final String DECODER_NOT_EXIST_ERROR_CODE = "backend.devices.decoder.not.exist";
    public static final String DECODER_NAME_ALREADY_EXIST_ERROR_CODE = "backend.devices.decoder.name.already.used";
    public static final String DECODER_DEFINED_BY_PLATFORM_ERROR_CODE = "backend.devices.decoder.defined.by.platform";
    public static final String DECODER_NOT_DEFINED_BY_PLATFORM_ERROR_CODE = "backend.devices.decoder.not.defined.by.platform";
    public static final String DECODER_DEFINED_BY_OTHER_ORGANIZATION_ERROR_CODE = "backend.devices.decoder.defined.by.other.organization";

    public static final String DEVICE_TYPE_NAME_FIELD_ERROR_CODE = "backend.devices.deviceType.name.notnull.not.empty";
    public static final String DEVICE_TYPE_PROTOCOL_FIELD_ERROR_CODE = "backend.devices.deviceType.protocol.notnull.not.empty";
    public static final String DEVICE_TYPE_DECODER_FIELD_ERROR_CODE = "backend.devices.deviceType.decoder.notnull.not.empty";
    public static final String DEVICE_TYPE_SENSOR_TYPES_FIELD_EXIST_ERROR_CODE = "backend.devices.deviceType.sensorsType.notnull.not.empty";
    public static final String DEVICE_TYPE_SENSOR_TYPES_ELEMENT_FIELD_EXIST_ERROR_CODE = "backend.devices.deviceType.sensorsType.element.notnull.not.empty";
    public static final String DEVICE_TYPE_ACTIVATE_FIELD_ERROR_CODE = "backend.devices.deviceType.activate.notnull.not.empty";
    public static final String DEVICE_TYPE_ALREADY_EXIST_ERROR_CODE = "backend.devices.deviceType.already.exist";
    public static final String DEVICE_TYPE_NOT_EXIST_ERROR_CODE = "backend.devices.deviceType.not.exist";
    public static final String DEVICE_TYPE_NAME_ALREADY_EXIST_ERROR_CODE = "backend.devices.deviceType.name.already.used";
    public static final String DEVICE_TYPE_DEFINED_BY_PLATFORM_ERROR_CODE = "backend.devices.deviceType.defined.by.platform";
    public static final String DEVICE_TYPE_NOT_DEFINED_BY_PLATFORM_ERROR_CODE = "backend.devices.deviceType.not.defined.by.platform";
    public static final String DEVICE_TYPE_DEFINED_BY_OTHER_ORGANIZATION_ERROR_CODE = "backend.devices.decoder.defined.by.other.organization";

    public static final String PROFILE_NOT_EXIST_ERROR_CODE = "backend.devices.profile.not.exist";
    public static final String PROFILE_NAME_FIELD_ERROR_CODE = "backend.devices.profile.notnull.not.empty";

    public static final String DEVICE_NAME_FIELD_ERROR_CODE = "backend.devices.device.name.notnull.not.empty";
    public static final String DEVICE_DEVICE_TYPE_ID_FIELD_ERROR_CODE = "backend.devices.deviceTypeId.name.notnull";
    public static final String DEVICE_PROFILE_ID_FIELD_ERROR_CODE = "backend.devices.profileId.name.notnull";
    public static final String DEVICE_USERS_ID_FIELD_ERROR_CODE = "backend.devices.users.ids.notnull";
    public static final String DEVICE_NOT_EXIST_ERROR_CODE = "backend.devices.device.not.exist";

    public static final String DEVICE_TYPE_OR_PROFILE_OWNED_BY_OTHER_ORGANIZATION_ERROR_CODE = "backend.devices.deviceType.or.profile.owned.by.other.organization";

}
