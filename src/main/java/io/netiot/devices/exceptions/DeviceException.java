package io.netiot.devices.exceptions;

public class DeviceException extends RuntimeException {

    public DeviceException() {
    }

    public DeviceException(String message) {
        super(message);
    }
}
