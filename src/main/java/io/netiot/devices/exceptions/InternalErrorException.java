package io.netiot.devices.exceptions;

public class InternalErrorException extends RuntimeException {

    public InternalErrorException() {
    }

    public InternalErrorException(String message) {
        super(message);
    }

}
