package io.netiot.devices.exceptions;

public class UnauthorizedOperationException extends RuntimeException {

    public UnauthorizedOperationException() {
    }

    public UnauthorizedOperationException(String message) {
        super(message);
    }
}
