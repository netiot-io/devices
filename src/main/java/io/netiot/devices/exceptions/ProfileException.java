package io.netiot.devices.exceptions;

public class ProfileException extends RuntimeException {

    public ProfileException() {
    }

    public ProfileException(String message) {
        super(message);
    }
}
