package io.netiot.devices.exceptions;

public class DeviceTypeException extends RuntimeException {

    public DeviceTypeException() {
    }

    public DeviceTypeException(String message) {
        super(message);
    }
}
