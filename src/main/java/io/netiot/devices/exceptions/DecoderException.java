package io.netiot.devices.exceptions;

public class DecoderException extends RuntimeException {

    public DecoderException() {
    }

    public DecoderException(String message) {
        super(message);
    }
}
