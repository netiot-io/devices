package io.netiot.devices.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "PHYSICAL_SENSORS")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PhysicalSensor {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PHYSICAL_SENSOR_GEN")
    @SequenceGenerator(name = "SEQ_PHYSICAL_SENSOR_GEN", sequenceName = "SEQ_PHYSICAL_SENSOR", allocationSize = 1)
    private Long id;

}
