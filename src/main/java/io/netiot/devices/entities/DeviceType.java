package io.netiot.devices.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "DEVICE_TYPES")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DeviceType {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DEVICE_TYPE_GEN")
    @SequenceGenerator(name = "SEQ_DEVICE_TYPE_GEN", sequenceName = "SEQ_DEVICE_TYPE", allocationSize = 1)
    private Long id;

    private String name;

    private String description;

    @ManyToOne(cascade= CascadeType.DETACH, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "protocol_id")
    private Protocol protocol;

    @ManyToOne(cascade= CascadeType.DETACH, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "decoder_id")
    private Decoder decoder;

    private Boolean definedByPlatform;

    private Long organizationId;

    private Boolean activate;

}
