package io.netiot.devices.entities;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "DEVICES_USERS")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(exclude = "device")
@ToString(exclude = "device")
public class DevicesUsers {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DEVICE_USERS_GEN")
    @SequenceGenerator(name = "SEQ_DEVICE_USERS_GEN", sequenceName = "SEQ_DEVICE_USERS", allocationSize = 1)
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "device_id")
    private Device device;
}
