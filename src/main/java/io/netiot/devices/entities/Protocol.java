package io.netiot.devices.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "PROTOCOLS")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Protocol {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PROTOCOL_GEN")
    @SequenceGenerator(name = "SEQ_PROTOCOL_GEN", sequenceName = "SEQ_PROTOCOL", allocationSize = 1)
    private Long id;

    private String name;

    private String protocolParameters;

    private String deviceParameters;

    @Column(name = "topic")
    private String gatewayTopic;

    private String metadata;

}
