package io.netiot.devices.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "ALERTS")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Alert {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ALERT_GEN")
    @SequenceGenerator(name = "SEQ_ALERT_GEN", sequenceName = "SEQ_ALERT", allocationSize = 1)
    private Long id;

    private Long deviceId;

    private String code;

    private LocalDateTime timestamp;
}
