package io.netiot.devices.entities;

import io.netiot.devices.converters.ParametersConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "PROFILES")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Profile {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PROFILE_GEN")
    @SequenceGenerator(name = "SEQ_PROFILE_GEN", sequenceName = "SEQ_PROFILE", allocationSize = 1)
    private Long id;

    private String name;

    @Convert(converter = ParametersConverter.class)
    private Parameters parameters;

    @ManyToOne(cascade= CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "device_type_id")
    private DeviceType deviceType;

    private Long organizationId;
}
