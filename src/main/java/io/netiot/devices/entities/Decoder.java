package io.netiot.devices.entities;

import io.netiot.devices.converters.DecoderFieldsConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "DECODERS")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Decoder {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DECODER_GEN")
    @SequenceGenerator(name = "SEQ_DECODER_GEN", sequenceName = "SEQ_DECODER", allocationSize = 1)
    private Long id;

    private String name;

    private String description;

    @Column(name = "fields")
    @Convert(converter = DecoderFieldsConverter.class)
    private List<String> fields;

    @ManyToOne(cascade= CascadeType.DETACH, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "protocol_id")
    private Protocol protocol;

    private String code;

    private Boolean definedByPlatform;

    private Long organizationId;

}
