package io.netiot.devices.entities;

import io.netiot.devices.converters.ParametersConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "DEVICES")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Device {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DEVICE_GEN")
    @SequenceGenerator(name = "SEQ_DEVICE_GEN", sequenceName = "SEQ_DEVICE", allocationSize = 1)
    private Long id;

    private String name;

    @Column(name = "device_parameters")
    @Convert(converter = ParametersConverter.class)
    private Parameters parameters;

    @ManyToOne(cascade= CascadeType.DETACH, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "device_type_id")
    private DeviceType deviceType;

    @ManyToOne(cascade= CascadeType.DETACH, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "profile_id")
    private Profile profile;

    @OneToMany(mappedBy = "device", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<DevicesUsers> users;

    private Boolean activate;

}