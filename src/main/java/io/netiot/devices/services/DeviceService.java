package io.netiot.devices.services;

import io.netiot.devices.entities.*;
import io.netiot.devices.exceptions.DeviceException;
import io.netiot.devices.exceptions.DeviceTypeException;
import io.netiot.devices.exceptions.ProfileException;
import io.netiot.devices.exceptions.UserException;
import io.netiot.devices.mappers.DeviceMapper;
import io.netiot.devices.mappers.DeviceUsersMapper;
import io.netiot.devices.models.DeviceApplicationModel;
import io.netiot.devices.models.DeviceModel;
import io.netiot.devices.models.events.DeviceEvent;
import io.netiot.devices.models.events.EventType;
import io.netiot.devices.repositories.DeviceRedisRepository;
import io.netiot.devices.repositories.DeviceRepository;
import io.netiot.devices.repositories.DevicesUsersRepository;
import io.netiot.devices.utils.AuthenticatedUserInfo;
import io.netiot.devices.validators.DeviceValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static io.netiot.devices.utils.ErrorMessages.*;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class DeviceService {

    private final AuthenticatedUserInfo authenticatedUserInfo;
    private final DeviceRepository deviceRepository;
    private final DevicesUsersRepository devicesUsersRepository;
    private final DeviceValidator deviceValidator;
    private final DeviceTypeService deviceTypeService;
    private final KafkaMessageSenderService kafkaMessageSenderService;
    private final DeviceRedisRepository deviceRedisRepository;
    private final UserService userService;

    public List<DeviceModel> getDevices() {
        Role userRole = authenticatedUserInfo.getRole();
        List<Device> devices;
        if (userRole == Role.ADMIN) {
            devices = deviceRepository.findAll();
        } else {
            devices  = deviceRepository.findByUsers_UserId(authenticatedUserInfo.getId());
        }
        return devices.stream()
                .map(DeviceMapper::toModel)
                .collect(Collectors.toList());
    }

    public DeviceModel getDevice(final Long deviceId) {
        Device device = deviceRepository.findById(deviceId)
                .orElseThrow(() -> { throw new DeviceException(DEVICE_NOT_EXIST_ERROR_CODE); });
        deviceValidator.validateOnGet(device);
        return DeviceMapper.toModel(device);
    }

    public DeviceModel saveDevice(final DeviceModel deviceModel) {
        Device device = DeviceMapper.toEntity(deviceModel);
        setDeviceBaseProperties(device, deviceModel);

        deviceValidator.validateOnSave(device);
        Device savedDevice = deviceRepository.saveAndFlush(device);

        deviceRedisRepository.save(savedDevice);
        sendDeviceEvent(savedDevice, EventType.CREATED);

        return DeviceMapper.toModel(savedDevice);
    }

    public void updateDevice(final Long deviceId, final DeviceModel deviceModel) {
        Device device = deviceRepository.findById(deviceId)
                .orElseThrow(() -> { throw new DeviceException(DEVICE_NOT_EXIST_ERROR_CODE); });
        setDeviceBaseProperties(device, deviceModel);

        device.setName(deviceModel.getName());
        device.setActivate(deviceModel.getActivate());
        Parameters parameters = Parameters.builder()
                .parametersMap(deviceModel.getDeviceParameters())
                .build();
        device.setParameters(parameters);

        deviceValidator.validateOnUpdate(device);
        deviceRepository.save(device);

        deviceRedisRepository.save(device);
        sendDeviceEvent(device, EventType.CHANGED);
    }

    public void deleteDevice(final Long deviceId) {
        Device device = deviceRepository.findById(deviceId)
                .orElseThrow(() -> { throw new DeviceException(DEVICE_NOT_EXIST_ERROR_CODE); });
        deviceValidator.validateOnDelete(device);
        deviceRepository.deleteById(deviceId);
        sendDeviceEvent(device, EventType.DELETED);
        deviceRedisRepository.delete(deviceId);
    }

    private void sendDeviceEvent(final Device device, final EventType eventType) {
        DeviceEvent deviceEvent = DeviceMapper.toDeviceEvent(device, eventType);
        String topic = device.getDeviceType().getProtocol().getGatewayTopic();
        kafkaMessageSenderService.sendDeviceEvent(deviceEvent, topic);
    }

    public String getDeviceName(final Long deviceId) {
        return deviceRepository.findById(deviceId).get().getName();
    }

    public String getDeviceTopic(final Long deviceId) {
        return deviceRepository.findById(deviceId).get().getDeviceType().getProtocol().getGatewayTopic();
    }

    public Long getDeviceId(final String deviceUUID) {
        Device dev = deviceRepository.findAll().stream().filter(device -> device.getParameters().getParametersMap()
                .get("devEui").equalsIgnoreCase(deviceUUID)).findFirst()
                .orElseThrow(() -> { throw new DeviceException(DEVICE_NOT_EXIST_ERROR_CODE); });
        return dev.getId();
    }

    public List<DeviceApplicationModel> getUserDevices(final Long userId) {
        return deviceRepository.findByUsers_UserId(userId).stream()
                .map(DeviceMapper::toApplicationModel).collect(Collectors.toList());
    }

    private void setDeviceBaseProperties(Device device, DeviceModel deviceModel) {
        DeviceType deviceType = deviceTypeService.findDeviceTypeById(deviceModel.getDeviceTypeId())
                .orElseThrow(() -> { throw new DeviceTypeException(DEVICE_TYPE_NOT_EXIST_ERROR_CODE); });
        Profile profile = deviceTypeService.findProfileById(deviceModel.getProfileId())
                .orElseThrow(() -> { throw new ProfileException(PROFILE_NOT_EXIST_ERROR_CODE); });

        List<User> parentUsers = userService.findUsersByIds(deviceModel.getUserIds())
                .orElseThrow(() -> { throw new UserException(USER_NOT_EXIST_ERROR_CODE); });

        device.setDeviceType(deviceType);
        devicesUsersRepository.deleteByDevice_Id(device.getId());
        device.setUsers(DeviceUsersMapper.toEntityList(parentUsers.stream().map(User::getId).collect(Collectors.toSet()), device));
        device.setProfile(profile);
    }
}
