package io.netiot.devices.services;

import io.netiot.devices.models.events.DeviceEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Marker;
import org.springframework.cloud.stream.binding.BinderAwareChannelResolver;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaMessageSenderService {

    private final BinderAwareChannelResolver binderAwareChannelResolver;

    public void sendDeviceEvent(final DeviceEvent deviceEvent, final String topic){
        Message message = MessageBuilder.withPayload(deviceEvent).build();
        sendMessage(message, topic);
    }

    private void sendMessage(final Message message, final String topic){
        try {
            final boolean messageSent = binderAwareChannelResolver.resolveDestination(topic)
                    .send(message);

            if (!messageSent) {
                log.error(String.format("The message could not be sent due to a non-fatal reason: %s", message.getPayload()));
            } else {
                log.info(String.format("Event sent: %s", message.getPayload()));
            }
        } catch (RuntimeException ex) {
            log.error(String.format("Non-recoverable error. Unable to send message='%s'", message.getPayload()), ex);
        }
    }

}