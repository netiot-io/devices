package io.netiot.devices.services;

import feign.FeignException;
import io.netiot.devices.entities.User;
import io.netiot.devices.exceptions.InternalErrorException;
import io.netiot.devices.feign.UsersClient;
import io.netiot.devices.mappers.UserMapper;
import io.netiot.devices.models.UserInternalModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Marker;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static io.netiot.devices.utils.ErrorMessages.INTERNAL_SERVER_ERROR_CODE;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional
public class UserService {

    private final Integer HTTP_STATUS_NOT_FOUND = 404;

    private final UsersClient usersClient;

    public Optional<List<User>> findUsersByIds(final List<Long> userIds) {
        try {
            ResponseEntity<List<UserInternalModel>> userModelResponseEntity = usersClient.getUsers(userIds);
            List<UserInternalModel> usersInternalModel = userModelResponseEntity.getBody();
            List<User> users = usersInternalModel.stream().map(UserMapper::toEntity).collect(Collectors.toList());

            return Optional.of(users);
        } catch (FeignException e) {

            if (e.status() == HTTP_STATUS_NOT_FOUND) {
                return Optional.empty();
            }

            log.error("Internal Error", e);

            throw new InternalErrorException(INTERNAL_SERVER_ERROR_CODE);
        } catch (Exception e) {
            log.error("Internal Error", e);

            throw new InternalErrorException(INTERNAL_SERVER_ERROR_CODE);
        }
    }

}
