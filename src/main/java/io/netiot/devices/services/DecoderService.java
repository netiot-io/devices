package io.netiot.devices.services;

import io.netiot.devices.entities.Decoder;
import io.netiot.devices.entities.Protocol;
import io.netiot.devices.entities.Role;
import io.netiot.devices.exceptions.DecoderException;
import io.netiot.devices.exceptions.ProtocolException;
import io.netiot.devices.mappers.DecoderMapper;
import io.netiot.devices.models.DecoderModel;
import io.netiot.devices.repositories.DecoderRepository;
import io.netiot.devices.services.helpers.DeviceTypeHelperService;
import io.netiot.devices.utils.AuthenticatedUserInfo;
import io.netiot.devices.validators.DecoderValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static io.netiot.devices.utils.ErrorMessages.DECODER_NOT_EXIST_ERROR_CODE;
import static io.netiot.devices.utils.ErrorMessages.PROTOCOL_NOT_EXIST_ERROR_CODE;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class DecoderService {

    private final AuthenticatedUserInfo authenticatedUserInfo;
    private final DecoderRepository decoderRepository;
    private final ProtocolService protocolService;
    private final DecoderValidator decoderValidator;
    private final DeviceTypeHelperService deviceTypeHelperService;

    public DecoderModel getDecoder(final Long decoderId) {
        Decoder decoder = decoderRepository.findById(decoderId)
                .orElseThrow(() -> { throw new DecoderException(DECODER_NOT_EXIST_ERROR_CODE); });
        decoderValidator.validateOnGet(decoder);
        return DecoderMapper.toModel(decoder);
    }

    public List<DecoderModel> getDecoders(final Long protocolId) {
        Role userRole = authenticatedUserInfo.getRole();
        if(userRole == Role.ADMIN){
            return decoderRepository.findByProtocolId(protocolId)
                    .stream().map(DecoderMapper::toModel).collect(Collectors.toList());
        } else {
            Long organizationId = authenticatedUserInfo.getOrganizationId();
            return decoderRepository.findByProtocolIsAndOrganizationIdOrDefinedByPlatformIsTrue(protocolId, organizationId)
                    .stream().map(DecoderMapper::toModel).collect(Collectors.toList());
        }
    }

    public List<DecoderModel> getDecoders() {
        Role userRole = authenticatedUserInfo.getRole();
        if(userRole == Role.ADMIN){
            return decoderRepository.findAll().stream().map(DecoderMapper::toModel).collect(Collectors.toList());
        } else {
            Long organizationId = authenticatedUserInfo.getOrganizationId();
            return decoderRepository.findByOrganizationIdOrDefinedByPlatformIsTrue(organizationId).stream().map(DecoderMapper::toModel).collect(Collectors.toList());
        }
    }

    public DecoderModel saveDecoder(final DecoderModel decoderModel) {
        Decoder decoder = DecoderMapper.toEntity(decoderModel);
        Protocol protocol = protocolService.getProtocol(decoderModel.getProtocolId())
                .orElseThrow(() -> { throw new ProtocolException(PROTOCOL_NOT_EXIST_ERROR_CODE); });
        decoder.setProtocol(protocol);
        decoderValidator.validateOnSave(decoder);

        setDecoderOwner(decoder);

        decoder = decoderRepository.save(decoder);
        return DecoderMapper.toModel(decoder);
    }

    public void updateDecoder(final Long decoderId, final DecoderModel decoderModel) {
        Decoder decoder = decoderRepository.findById(decoderId)
                .orElseThrow(() -> { throw new DecoderException(DECODER_NOT_EXIST_ERROR_CODE); });

        Decoder newDecoder = DecoderMapper.toEntity(decoderModel);
        decoder.setName(newDecoder.getName());
        decoder.setDescription(newDecoder.getDescription());
        decoder.setFields(newDecoder.getFields());
        decoder.setCode(newDecoder.getCode());

        Protocol protocol = protocolService.getProtocol(decoderModel.getProtocolId())
                .orElseThrow(() -> { throw new ProtocolException(PROTOCOL_NOT_EXIST_ERROR_CODE); });
        decoder.setProtocol(protocol);

        decoderValidator.validateOnUpdate(decoder);
        Decoder savedDecoder = decoderRepository.save(decoder);
        deviceTypeHelperService. updateDeviceTypesWithDecoder(savedDecoder);
    }

    private void setDecoderOwner(final Decoder decoder) {
        Role userRole = authenticatedUserInfo.getRole();
        if(userRole == Role.ADMIN){
            decoder.setDefinedByPlatform(Boolean.TRUE);
        } else {
            Long organizationId = authenticatedUserInfo.getOrganizationId();
            decoder.setOrganizationId(organizationId);
            decoder.setDefinedByPlatform(Boolean.FALSE);
        }
    }

    public void deleteDecoder(final Long decoderId) {
        Decoder decoder = decoderRepository.findById(decoderId)
                .orElseThrow(() -> { throw new DecoderException(DECODER_NOT_EXIST_ERROR_CODE); });
        decoderValidator.validateOnDelete(decoder);
        decoderRepository.deleteById(decoderId);
    }

}
