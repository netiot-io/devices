package io.netiot.devices.services;

import io.netiot.devices.entities.Alert;
import io.netiot.devices.mappers.AlertMapper;
import io.netiot.devices.models.AlertModel;
import io.netiot.devices.models.events.AlertEvent;
import io.netiot.devices.repositories.AlertRepository;
import io.netiot.devices.validators.AlertValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class AlertService {

    private final AlertRepository alertRepository;
    private final AlertValidator alertValidator;

    public void save(final AlertEvent alertEvent) {
        Alert alert = AlertMapper.toEntity(alertEvent);
        Optional<Alert> alertOptional = alertRepository.findByDeviceIdAndTimestamp(alert.getDeviceId(), alert.getTimestamp());
        if (!alertOptional.isPresent()) {
            alertRepository.save(alert);
        } else {
            log.error("AlertEvent already processed: " + alertEvent.toString());
        }
    }

    public List<AlertModel> getAlerts(final long deviceId) {
        alertValidator.validateOnGet(deviceId);
        return alertRepository.findByDeviceId(deviceId)
                .stream()
                .map(AlertMapper::toModel)
                .collect(Collectors.toList());
    }

    public List<AlertModel> getLast10Alerts(final long deviceId) {
        alertValidator.validateOnGet(deviceId);
        return alertRepository.findTop10ByDeviceIdInOrderByIdDesc(deviceId)
                .stream()
                .map(AlertMapper::toModel)
                .collect(Collectors.toList());
    }

}
