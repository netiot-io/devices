package io.netiot.devices.services;

import io.netiot.devices.entities.*;
import io.netiot.devices.exceptions.DeviceTypeException;
import io.netiot.devices.exceptions.ProfileException;
import io.netiot.devices.exceptions.ProtocolException;
import io.netiot.devices.mappers.DecoderMapper;
import io.netiot.devices.mappers.DeviceTypeMapper;
import io.netiot.devices.models.DecoderModel;
import io.netiot.devices.mappers.ProfileMapper;
import io.netiot.devices.models.DeviceTypeModel;
import io.netiot.devices.models.ProfileModel;
import io.netiot.devices.repositories.DeviceTypeRepository;
import io.netiot.devices.repositories.ProfileRepository;
import io.netiot.devices.services.helpers.DeviceHelperService;
import io.netiot.devices.utils.AuthenticatedUserInfo;
import io.netiot.devices.validators.DeviceTypeValidator;
import io.netiot.devices.validators.ProfileValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static io.netiot.devices.utils.ErrorMessages.*;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class DeviceTypeService {

    private final AuthenticatedUserInfo authenticatedUserInfo;
    private final DeviceTypeRepository deviceTypeRepository;
    private final ProfileRepository profileRepository;
    private final ProtocolService protocolService;
    private final DeviceTypeValidator deviceTypeValidator;
    private final ProfileValidator profileValidator;
    private final DecoderService decoderService;
    private final DeviceHelperService deviceHelperService;

    public DeviceTypeModel getDeviceType(final Long deviceTypeId) {
        DeviceType deviceType = deviceTypeRepository.findById(deviceTypeId)
                .orElseThrow(() -> { throw new DeviceTypeException(DEVICE_TYPE_NOT_EXIST_ERROR_CODE); });
        deviceTypeValidator.validateOnGet(deviceType);
        return DeviceTypeMapper.toModel(deviceType);
    }

    public List<DeviceTypeModel> getDeviceTypes(Long protocolId) {
        Role userRole = authenticatedUserInfo.getRole();
        List<DeviceType> deviceTypeList;
        if (userRole == Role.ADMIN) {
            deviceTypeList = deviceTypeRepository.findByProtocolId(protocolId);
        } else {
            Long organizationId = authenticatedUserInfo.getOrganizationId();
            deviceTypeList = deviceTypeRepository.findByProtocolIsAndOrganizationIdOrDefinedByPlatformIsTrue(protocolId, organizationId);
        }

        return deviceTypeList.stream()
                .map(DeviceTypeMapper::toModel)
                .collect(Collectors.toList());
    }

    public DeviceTypeModel saveDeviceType(final DeviceTypeModel deviceTypeModel) {
        DeviceType deviceType = DeviceTypeMapper.toEntity(deviceTypeModel);
        generateDeviceType(deviceType, deviceTypeModel);
        setDeviceTypeOwner(deviceType);

        deviceTypeValidator.validateOnSave(deviceType);

        DeviceType savedDeviceType = deviceTypeRepository.save(deviceType);
        return DeviceTypeMapper.toModel(savedDeviceType);
    }

    public void updateDeviceType(final Long deviceTypeId, final DeviceTypeModel deviceTypeModel) {
        DeviceType deviceType = deviceTypeRepository.findById(deviceTypeId)
                .orElseThrow(() -> { throw new DeviceTypeException(DEVICE_TYPE_NOT_EXIST_ERROR_CODE); });

        deviceType.setName(deviceTypeModel.getName());
        deviceType.setDescription(deviceTypeModel.getDescription());
        deviceType.setActivate(deviceTypeModel.getActivate());
        generateDeviceType(deviceType, deviceTypeModel);

        deviceTypeValidator.validateOnUpdate(deviceType);
        DeviceType savedDeviceType = deviceTypeRepository.save(deviceType);
        deviceHelperService.updateDevicesWithDeviceType(savedDeviceType.getId());
    }

    public void deleteDeviceType(final Long deviceTypeId) {
        DeviceType deviceType = deviceTypeRepository.findById(deviceTypeId)
                .orElseThrow(() -> { throw new DeviceTypeException(DEVICE_TYPE_NOT_EXIST_ERROR_CODE); });
        deviceTypeValidator.validateOnDelete(deviceType);
        deviceTypeRepository.deleteById(deviceType.getId());
    }

    public ProfileModel getDeviceTypeProfile(final Long deviceTypeId, final Long profileId) {
        deviceTypeRepository.findById(deviceTypeId)
                .orElseThrow(() -> {
                    throw new DeviceTypeException(DEVICE_TYPE_NOT_EXIST_ERROR_CODE);
                });
        Profile profile = profileRepository.findById(profileId)
                .orElseThrow(() -> {
                    throw new DeviceTypeException(PROFILE_NOT_EXIST_ERROR_CODE);
                });
        profileValidator.validateOnGet(profile);
        return ProfileMapper.toModel(profile);
    }

    public List<ProfileModel> getDeviceTypeProfiles(final Long deviceTypeId) {
        Role userRole = authenticatedUserInfo.getRole();

        deviceTypeRepository.findById(deviceTypeId)
                .orElseThrow(() -> {
                    throw new DeviceTypeException(DEVICE_TYPE_NOT_EXIST_ERROR_CODE);
                });

        List<Profile> profiles;
        if (userRole == Role.ADMIN) {
            profiles = profileRepository.findByDeviceType_Id(deviceTypeId);
        } else {
            Long organizationId = authenticatedUserInfo.getOrganizationId();
            profiles = profileRepository.findByDeviceType_IdAndOrganizationId(deviceTypeId, organizationId);
        }
        return profiles.stream()
                .map(ProfileMapper::toModel)
                .collect(Collectors.toList());
    }

    //TODO check if some parameters are same with other profiles parameters
    public ProfileModel saveDeviceTypeProfile(final Long deviceTypeId, final ProfileModel profileModel) {
        DeviceType deviceType = deviceTypeRepository.findById(deviceTypeId)
                .orElseThrow(() -> {
                    throw new DeviceTypeException(DEVICE_TYPE_NOT_EXIST_ERROR_CODE);
                });

        Profile profile = ProfileMapper.toEntity(profileModel);
        profile.setDeviceType(deviceType);
        Long organizationId = authenticatedUserInfo.getOrganizationId();
        profile.setOrganizationId(organizationId);
        profileValidator.validateOnSave(profile);
        Profile savedProfile = profileRepository.save(profile);
        return ProfileMapper.toModel(savedProfile);
    }

    //TODO check if some parameters are same with other profiles parameters
    public void updateDeviceTypeProfile(final Long deviceTypeId, final Long profileId, final ProfileModel profileModel) {
        deviceTypeRepository.findById(deviceTypeId)
                .orElseThrow(() -> {
                    throw new DeviceTypeException(DEVICE_TYPE_NOT_EXIST_ERROR_CODE);
                });

        Profile profile = profileRepository.findById(profileId)
                .orElseThrow(() -> {
                    throw new ProfileException(PROFILE_NOT_EXIST_ERROR_CODE);
                });

        Parameters parameters = Parameters.builder().parametersMap(profileModel.getProtocolParameters()).build();

        profile.setName(profileModel.getName());
        profile.setParameters(parameters);
        profileValidator.validateOnUpdate(profile);
        Profile savedProfile = profileRepository.save(profile);
        deviceHelperService.updateDevicesWithProfile(savedProfile);
    }

    public void deleteDeviceTypeProfile(final Long deviceTypeId, final Long profileId) {
        deviceTypeRepository.findById(deviceTypeId)
                .orElseThrow(() -> {
                    throw new DeviceTypeException(DEVICE_TYPE_NOT_EXIST_ERROR_CODE);
                });
        Profile profile = profileRepository.findById(profileId)
                .orElseThrow(() -> {
                    throw new ProfileException(PROFILE_NOT_EXIST_ERROR_CODE);
                });
        profileValidator.validateOnDelete(profile);
        profileRepository.deleteById(profileId);
    }

    private void setDeviceTypeOwner(final DeviceType deviceType) {
        Role userRole = authenticatedUserInfo.getRole();
        if(userRole == Role.ADMIN){
            deviceType.setDefinedByPlatform(Boolean.TRUE);
        } else {
            Long organizationId = authenticatedUserInfo.getOrganizationId();
            deviceType.setOrganizationId(organizationId);
            deviceType.setDefinedByPlatform(Boolean.FALSE);
        }
    }

    private void generateDeviceType(DeviceType deviceType, DeviceTypeModel deviceTypeModel) {
        Protocol protocol = protocolService.getProtocol(deviceTypeModel.getProtocolId())
                .orElseThrow(() -> { throw new ProtocolException(PROTOCOL_NOT_EXIST_ERROR_CODE); });
        DecoderModel decoderModel = decoderService.getDecoder(deviceTypeModel.getDecoderId());
        Decoder decoder = DecoderMapper.toEntity(decoderModel);

        deviceType.setProtocol(protocol);
        deviceType.setDecoder(decoder);
    }
    
    protected Optional<DeviceType> findDeviceTypeById(final Long deviceTypeId) {
        return deviceTypeRepository.findById(deviceTypeId);
    }

    protected Optional<Profile> findProfileById(final Long profileId) {
        return profileRepository.findById(profileId);
    }

}
