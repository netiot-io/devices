package io.netiot.devices.services;

import io.netiot.devices.configurations.KafkaChannels;
import io.netiot.devices.models.events.AlertEvent;
import io.netiot.devices.validators.AlertValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaMessageReceiverService {

    private final AlertService alertService;
    private final AlertValidator alertValidator;

    @StreamListener(KafkaChannels.ALERTS_INPUT)
    public void receiveEvent(final AlertEvent alertEvent) {
        if (alertValidator.validateAlertEvent(alertEvent)) {
            alertService.save(alertEvent);
            log.info("Message received: " + alertEvent.toString());
        } else {
            log.warn("AlertEvent is invalid: " + alertEvent.toString());
        }
    }

}
