package io.netiot.devices.services.helpers;

import io.netiot.devices.entities.Device;
import io.netiot.devices.entities.Profile;
import io.netiot.devices.repositories.DeviceRedisRepository;
import io.netiot.devices.repositories.DeviceRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class DeviceHelperService {

    private final DeviceRepository deviceRepository;
    private final DeviceRedisRepository deviceRedisRepository;


    public void updateDevicesWithProfile(final Profile profile) {
        List<Device> devices = deviceRepository.findByProfile_Id(profile.getId());
        devices.forEach(deviceRedisRepository::save);
    }

    public void updateDevicesWithDeviceType(final Long deviceTypeId) {
        deviceRepository.findByDeviceType_Id(deviceTypeId)
                .forEach(deviceRedisRepository::save);
    }

    public boolean checkAuthorization(final Long deviceId, final Long userId) {
        Optional<Device> device = deviceRepository.findById(deviceId);
        return device.isPresent() && device.get().getUsers().stream().anyMatch(user -> user.getUserId().compareTo(userId) == 0);
    }

}
