package io.netiot.devices.services.helpers;

import io.netiot.devices.entities.Decoder;
import io.netiot.devices.entities.DeviceType;
import io.netiot.devices.repositories.DeviceTypeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class DeviceTypeHelperService {

    private final DeviceTypeRepository deviceTypeRepository;
    private final DeviceHelperService deviceHelperService;

    public void updateDeviceTypesWithDecoder(final Decoder decoder) {
        deviceTypeRepository.findByDecoder_Id(decoder.getId())
                .stream()
                .map(DeviceType::getId)
                .forEach(deviceHelperService::updateDevicesWithDeviceType);
    }
}
