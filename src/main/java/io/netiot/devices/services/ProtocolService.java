package io.netiot.devices.services;

import io.netiot.devices.entities.Protocol;
import io.netiot.devices.mappers.ProtocolMapper;
import io.netiot.devices.models.ProtocolModel;
import io.netiot.devices.repositories.ProtocolRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class ProtocolService {

    private final ProtocolRepository protocolRepository;

    public List<ProtocolModel> getProtocols() {
        return protocolRepository.findAll().stream().map(ProtocolMapper::toModel).collect(Collectors.toList());
    }

    public Optional<Protocol> getProtocol(final String name) {
        return protocolRepository.findByName(name);
    }

    public Optional<Protocol> getProtocol(final Long protocolId) {
        return protocolRepository.findById(protocolId);
    }
}
