package io.netiot.devices.models;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.Map;

import static io.netiot.devices.utils.ErrorMessages.PROFILE_NAME_FIELD_ERROR_CODE;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ProfileModel {

    private Long id;

    @NotEmpty(message = PROFILE_NAME_FIELD_ERROR_CODE)
    private String name;

    private Map<String, String> protocolParameters;

    private Long organizationId;

}
