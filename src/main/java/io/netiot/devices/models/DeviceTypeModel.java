package io.netiot.devices.models;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.util.Set;

import static io.netiot.devices.utils.ErrorMessages.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class DeviceTypeModel {

    private Long id;

    @NotEmpty(message = DEVICE_TYPE_NAME_FIELD_ERROR_CODE)
    private String name;

    private String description;

    @NotNull(message = DEVICE_TYPE_PROTOCOL_FIELD_ERROR_CODE)
    private Long protocolId;

    @NotNull(message = DEVICE_TYPE_DECODER_FIELD_ERROR_CODE)
    private Long decoderId;

    private Boolean definedByPlatform;

    private Long organizationId;

    @NotNull(message = DEVICE_TYPE_ACTIVATE_FIELD_ERROR_CODE)
    private Boolean activate;

}
