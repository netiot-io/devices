package io.netiot.devices.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AlertModel {

    private Long id;

    private Long deviceId;

    private String code;

    private LocalDateTime timestamp;
}
