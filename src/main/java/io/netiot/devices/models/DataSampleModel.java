package io.netiot.devices.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataSampleModel {

    private Long id;

    private Long timestamp;

    private Long timestampR;

    private String values;

}

