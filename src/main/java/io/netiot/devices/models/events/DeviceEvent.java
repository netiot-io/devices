package io.netiot.devices.models.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DeviceEvent {

    private Long id;

    private EventType eventType;

    private StatusEventType statusEventType;

    private Map<String, String> parameters;

}
