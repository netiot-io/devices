package io.netiot.devices.models.events;

public enum EventType {
    CREATED, DELETED, CHANGED
}
