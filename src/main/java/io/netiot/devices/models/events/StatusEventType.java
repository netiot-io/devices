package io.netiot.devices.models.events;

public enum StatusEventType {
    ACTIVE, DISABLE
}