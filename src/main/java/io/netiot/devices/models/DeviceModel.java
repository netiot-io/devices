package io.netiot.devices.models;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

import static io.netiot.devices.utils.ErrorMessages.DEVICE_DEVICE_TYPE_ID_FIELD_ERROR_CODE;
import static io.netiot.devices.utils.ErrorMessages.DEVICE_NAME_FIELD_ERROR_CODE;
import static io.netiot.devices.utils.ErrorMessages.DEVICE_PROFILE_ID_FIELD_ERROR_CODE;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class DeviceModel {

    private Long id;

    @NotEmpty(message = DEVICE_NAME_FIELD_ERROR_CODE)
    private String name;

    private Map<String, String> deviceParameters;

    @NotNull(message = DEVICE_DEVICE_TYPE_ID_FIELD_ERROR_CODE)
    private Long deviceTypeId;

    @NotNull(message = DEVICE_PROFILE_ID_FIELD_ERROR_CODE)
    private Long profileId;

    private List<Long> userIds;

    private Boolean activate;
}
