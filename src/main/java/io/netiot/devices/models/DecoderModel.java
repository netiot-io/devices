package io.netiot.devices.models;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import static io.netiot.devices.utils.ErrorMessages.DECODER_NAME_FIELD_ERROR_CODE;
import static io.netiot.devices.utils.ErrorMessages.DECODER_PROTOCOL_FIELD_ERROR_CODE;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class DecoderModel {

    private Long id;

    @NotEmpty(message = DECODER_NAME_FIELD_ERROR_CODE)
    private String name;

    private String description;

    @NotNull(message = DECODER_PROTOCOL_FIELD_ERROR_CODE)
    private Long protocolId;

    private String code;

    private Boolean definedByPlatform;

    private Long organizationId;

}
