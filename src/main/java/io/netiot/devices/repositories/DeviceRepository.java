package io.netiot.devices.repositories;

import io.netiot.devices.entities.Device;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeviceRepository extends JpaRepository<Device, Long> {

    List<Device> findByProfile_Id(final Long profileId);

    List<Device> findByDeviceType_Id(final Long deviceTypeId);

    List<Device> findByUsers_UserId(final Long userId);

}
