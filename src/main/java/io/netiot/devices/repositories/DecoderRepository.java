package io.netiot.devices.repositories;

import io.netiot.devices.entities.Decoder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface DecoderRepository extends JpaRepository<Decoder, Long> {

    List<Decoder> findByOrganizationIdOrDefinedByPlatformIsTrue(final Long organizationId);

    List<Decoder> findByDefinedByPlatformIsTrue();

    @Query(nativeQuery = true, value = "select * from decoders d where d.protocol_id = ?1 and (d.defined_by_platform = true or d.organization_id = ?2)",
            countQuery = "select count(*) from decoders d where d.protocol_id = ?1 and (d.defined_by_platform = true or d.organization_id = ?2)")
    List<Decoder> findByProtocolIsAndOrganizationIdOrDefinedByPlatformIsTrue(final Long protocolId, final Long organizationId);

    List<Decoder> findByDefinedByPlatformIsTrueAndProtocolId(final Long protocolId);

    List<Decoder> findByProtocolId(final Long protocolId);

    Optional<Decoder> findByName(final String name);
}
