package io.netiot.devices.repositories;

import io.netiot.devices.entities.DevicesUsers;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DevicesUsersRepository extends JpaRepository<DevicesUsers, Long> {
    void deleteByDevice_Id(final Long deviceId);
}
