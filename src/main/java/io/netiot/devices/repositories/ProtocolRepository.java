package io.netiot.devices.repositories;

import io.netiot.devices.entities.Protocol;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProtocolRepository extends JpaRepository<Protocol, Long> {

    Optional<Protocol> findByName(String name);
}
