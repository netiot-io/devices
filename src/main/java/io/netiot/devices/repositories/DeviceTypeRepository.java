package io.netiot.devices.repositories;

import io.netiot.devices.entities.DeviceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface DeviceTypeRepository extends JpaRepository<DeviceType, Long> {

    @Query(nativeQuery = true, value = "select * from device_types d where d.protocol_id = ?1 and (d.defined_by_platform = true or d.organization_id = ?2)",
            countQuery = "select count(*) from device_types d where d.protocol_id = ?1 and (d.defined_by_platform = true or d.organization_id = ?2)")
    List<DeviceType> findByProtocolIsAndOrganizationIdOrDefinedByPlatformIsTrue(final Long protocolId, final Long organizationId);

    List<DeviceType> findByProtocolId(final Long protocolId);

    List<DeviceType> findByDefinedByPlatformIsTrue();

    Optional<DeviceType> findByName(final String name);

    List<DeviceType> findByDecoder_Id(final Long decoderId);

}
