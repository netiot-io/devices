package io.netiot.devices.repositories;

import io.netiot.devices.entities.Alert;
import io.netiot.devices.entities.Decoder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface AlertRepository extends JpaRepository<Alert, Long> {

    Optional<Alert> findByDeviceIdAndTimestamp(final Long deviceId, final LocalDateTime timestamp);

    List<Alert> findByDeviceId(final Long deviceId);

    List<Alert> findTop10ByDeviceIdInOrderByIdDesc(final Long deviceId);

}
