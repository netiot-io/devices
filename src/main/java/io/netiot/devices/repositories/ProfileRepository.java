package io.netiot.devices.repositories;

import io.netiot.devices.entities.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProfileRepository extends JpaRepository<Profile, Long> {

    List<Profile> findByDeviceType_IdAndOrganizationId(final Long deviceTypeId, final Long organizationId);

    List<Profile> findByDeviceType_Id(final Long deviceTypeId);

}
