package io.netiot.devices.repositories;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.netiot.devices.entities.Device;
import io.netiot.devices.mappers.DeviceMapper;
import io.netiot.devices.entities.DeviceProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class DeviceRedisRepository {

    public static final String DEVICES_REDIS_HASH = "devices";

    private final RedisTemplate<String, String> redisTemplate;
    private final Gson gson;

    public void save(final Device device) {
        DeviceProperties deviceProperties = DeviceMapper.toDevicePropertiesModel(device);
        HashOperations<String, Long, String> hashOperations = redisTemplate.opsForHash();
        String json = gson.toJson(deviceProperties);
        hashOperations.put(DEVICES_REDIS_HASH, device.getId(), json);
    }

    public void delete(final Long deviceId) {
        HashOperations<String, Long, DeviceProperties> hashOperations = redisTemplate.opsForHash();
        hashOperations.delete(DEVICES_REDIS_HASH, deviceId);
    }

}
