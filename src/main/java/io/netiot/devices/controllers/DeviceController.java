package io.netiot.devices.controllers;

import io.netiot.devices.exceptions.UnauthorizedOperationException;
import io.netiot.devices.feign.IOServerEndpoints;
import io.netiot.devices.mappers.DeviceMapper;
import io.netiot.devices.models.DeviceModel;
import io.netiot.devices.models.events.AlertEvent;
import io.netiot.devices.services.AlertService;
import io.netiot.devices.services.DeviceService;
import io.netiot.devices.utils.AlertCodes;
import io.netiot.devices.validators.ValidatorUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.netiot.devices.utils.ErrorMessages.UNAUTHORIZED_ACTION_ERROR_CODE;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/devices")
public class DeviceController {

    private final DeviceService deviceService;
    private final AlertService alertService;
    private final ValidatorUtil validatorUtil;
    private final IOServerEndpoints ioServerEndpoints;

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @GetMapping
    public ResponseEntity getDevices() {
        final List<DeviceModel> deviceTypeModelList = deviceService.getDevices();
        return ResponseEntity.ok(DeviceMapper.toListModel(deviceTypeModelList));
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @GetMapping("/{deviceId}")
    public ResponseEntity getDevice(@PathVariable(name = "deviceId") final Long deviceId) {
        return ResponseEntity.ok(deviceService.getDevice(deviceId));
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @PostMapping
    public ResponseEntity saveDevice(@RequestBody @Valid final DeviceModel deviceModel,
                                     final BindingResult bindingResult) {
        validatorUtil.checkBindingResultErrors(bindingResult);

        DeviceModel savedDeviceModel;
        if (deviceModel.getUserIds().size() == 0) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION_ERROR_CODE);
        } else {
            savedDeviceModel = deviceService.saveDevice(deviceModel);
        }

        addAlert(savedDeviceModel.getId(), savedDeviceModel.getActivate() ? AlertCodes.DEVICE_ADDED_ACTIVE :
                AlertCodes.DEVICE_ADDED_INACTIVE);
        return ResponseEntity.ok(generateMap(savedDeviceModel));
    }

    private Map<String, Object> generateMap(final DeviceModel deviceModel) {
        Map<String, Object> result = new HashMap<>();
        result.put("id", deviceModel.getId());
        return result;
    }

    private void addAlert(final Long deviceId, final String code) {
        alertService.save(new AlertEvent(deviceId, code, System.currentTimeMillis()));
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @PutMapping("/{deviceId}")
    public ResponseEntity updateDevice(@PathVariable(name = "deviceId") final Long deviceId, @RequestBody @Valid final DeviceModel deviceModel, final BindingResult bindingResult) {
        validatorUtil.checkBindingResultErrors(bindingResult);
        DeviceModel currentDevice = deviceService.getDevice(deviceId);
        deviceService.updateDevice(deviceId, deviceModel);
        if (currentDevice.getActivate() != deviceModel.getActivate()) {
            addAlert(deviceModel.getId(), deviceModel.getActivate() ? AlertCodes.DEVICE_ADDED_ACTIVE :
                    AlertCodes.DEVICE_ADDED_INACTIVE);
        }
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @DeleteMapping("/{deviceId}")
    public ResponseEntity deleteDevice(@PathVariable(name = "deviceId") final Long deviceId) {
        deviceService.deleteDevice(deviceId);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @GetMapping("/device-data")
    public ResponseEntity getDeviceData(@RequestParam(name = "device-id") final Long deviceId,
                                        @RequestParam(name = "millis-from") final Long millisFrom,
                                        @RequestParam(name = "millisTo", required = false) final Long millisTo) {
        return ResponseEntity.ok(ioServerEndpoints.getDeviceData(deviceId, millisFrom, millisTo == null ? System.currentTimeMillis() : millisTo));
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @GetMapping("/{deviceId}/device-data-count")
    public ResponseEntity getDeviceDataCount(@PathVariable(name = "deviceId") final Long deviceId) {
        return ResponseEntity.ok(ioServerEndpoints.getDeviceDataCount(deviceId));
    }

    @GetMapping("/internal")
    public ResponseEntity getUserDevices(@RequestParam(name = "userId") final Long userId) {
        return ResponseEntity.ok(deviceService.getUserDevices(userId));
    }

    @GetMapping("/internal/{deviceId}")
    public ResponseEntity getDeviceName(@PathVariable(name = "deviceId") final Long deviceId) {
        return ResponseEntity.ok(deviceService.getDeviceName(deviceId));
    }

    @GetMapping("/internal/{deviceUUID}/id")
    public ResponseEntity getDeviceId(@PathVariable(name = "deviceUUID") final String deviceUUID) {
        return ResponseEntity.ok(deviceService.getDeviceId(deviceUUID));
    }

    @GetMapping("/internal/{deviceId}/topic")
    public ResponseEntity getDeviceTopic(@PathVariable(name = "deviceId") final Long deviceId) {
        return ResponseEntity.ok(deviceService.getDeviceTopic(deviceId));
    }

}
