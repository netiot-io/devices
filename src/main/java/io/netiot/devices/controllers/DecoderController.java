package io.netiot.devices.controllers;

import io.netiot.devices.mappers.DecoderMapper;
import io.netiot.devices.models.DecoderModel;
import io.netiot.devices.services.DecoderService;
import io.netiot.devices.validators.ValidatorUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/decoders")
public class DecoderController {

    private final DecoderService decoderService;
    private final ValidatorUtil validatorUtil;

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @GetMapping
    public ResponseEntity getDecoders(@RequestParam(name = "protocolId") final Optional<Long> protocolIdOptional) {
        List<DecoderModel> decoderModels = protocolIdOptional.map(decoderService::getDecoders)
                .orElseGet(decoderService::getDecoders);
        return ResponseEntity.ok(DecoderMapper.toListModel(decoderModels));
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @PostMapping
    public ResponseEntity saveDecoder(@RequestBody @Valid final DecoderModel decoderModel, final BindingResult bindingResult) {
        validatorUtil.checkBindingResultErrors(bindingResult);
        return ResponseEntity.ok(generateMap(decoderService.saveDecoder(decoderModel)));
    }

    private Map<String, Object> generateMap(final DecoderModel decoderModel) {
        Map<String, Object> result = new HashMap<>();
        result.put("decoder_id", decoderModel.getId());
        return result;
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @PutMapping("/{decoderId}")
    public ResponseEntity updateDecoder(@PathVariable(name = "decoderId") final Long decoderId,
                                        @RequestBody @Valid final DecoderModel decoderModel, final BindingResult bindingResult) {
        validatorUtil.checkBindingResultErrors(bindingResult);
        decoderService.updateDecoder(decoderId,decoderModel);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @DeleteMapping("/{decoderId}")
    public ResponseEntity deleteDecoder(@PathVariable(name = "decoderId") final Long decoderId) {
        decoderService.deleteDecoder(decoderId);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @GetMapping("/{decoderId}")
    public ResponseEntity getDecoder(@PathVariable(name = "decoderId") final Long decoderId) {
        return ResponseEntity.ok(decoderService.getDecoder(decoderId));
    }

}
