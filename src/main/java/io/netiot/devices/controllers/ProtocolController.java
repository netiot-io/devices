package io.netiot.devices.controllers;

import io.netiot.devices.mappers.ProtocolMapper;
import io.netiot.devices.services.ProtocolService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/protocols")
public class ProtocolController {

    private final ProtocolService protocolService;

    @GetMapping
    public ResponseEntity getProtocols() {
        return ResponseEntity.ok(ProtocolMapper.toListModel(protocolService.getProtocols()));
    }
}
