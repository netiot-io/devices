package io.netiot.devices.controllers;

import io.netiot.devices.services.AlertService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/alerts")
public class AlertController {

    private final AlertService alertService;

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @PostMapping
    public ResponseEntity getAlerts(final long deviceId) {
        return ResponseEntity.ok(alertService.getAlerts(deviceId));
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @GetMapping("/{deviceId}/last-10-alerts")
    public ResponseEntity getLast10Alerts(@PathVariable(name ="deviceId") final long deviceId) {
        return ResponseEntity.ok(alertService.getLast10Alerts(deviceId));
    }
}
