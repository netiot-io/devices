package io.netiot.devices.controllers;

import io.netiot.devices.mappers.DeviceTypeMapper;
import io.netiot.devices.mappers.ProfileMapper;
import io.netiot.devices.models.DeviceTypeModel;
import io.netiot.devices.models.ProfileModel;
import io.netiot.devices.services.DeviceTypeService;
import io.netiot.devices.validators.ValidatorUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/device-types")
public class DeviceTypeController {

    private final DeviceTypeService deviceTypeService;
    private final ValidatorUtil validatorUtil;

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @GetMapping
    public ResponseEntity getDeviceTypes(@RequestParam(name = "protocolId") final Long protocolId) {
        final List<DeviceTypeModel> deviceTypeModelList = deviceTypeService.getDeviceTypes(protocolId);
        return ResponseEntity.ok(DeviceTypeMapper.toListModel(deviceTypeModelList));
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @PostMapping
    public ResponseEntity saveDeviceType(@RequestBody @Valid final DeviceTypeModel deviceTypeModel, final BindingResult bindingResult) {
        validatorUtil.checkBindingResultErrors(bindingResult);
        final DeviceTypeModel savedDeviceTypeModel = deviceTypeService.saveDeviceType(deviceTypeModel);
        return ResponseEntity.ok(generateMap(savedDeviceTypeModel));
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @PutMapping("/{deviceTypeId}")
    public ResponseEntity updateDeviceType(@PathVariable(name = "deviceTypeId") final Long deviceTypeId,
                                           @RequestBody @Valid final DeviceTypeModel deviceTypeModel,
                                           final BindingResult bindingResult) {
        validatorUtil.checkBindingResultErrors(bindingResult);
        deviceTypeService.updateDeviceType(deviceTypeId, deviceTypeModel);
        return ResponseEntity.ok().build();
    }

    private Map<String, Object> generateMap(final DeviceTypeModel deviceTypeModel) {
        Map<String, Object> result = new HashMap<>();
        result.put("id", deviceTypeModel.getId());
        return result;
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @DeleteMapping("/{deviceTypeId}")
    public ResponseEntity deleteDeviceType(@PathVariable(name = "deviceTypeId") final Long deviceTypeId) {
        deviceTypeService.deleteDeviceType(deviceTypeId);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @GetMapping("/{deviceTypeId}")
    public ResponseEntity getDeviceType(@PathVariable(name = "deviceTypeId") final Long deviceTypeId) {
        return ResponseEntity.ok(deviceTypeService.getDeviceType(deviceTypeId));
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @GetMapping("/{deviceTypeId}/profiles")
    public ResponseEntity getDeviceTypeProfiles(@PathVariable(name = "deviceTypeId") final Long deviceTypeId){
        final List<ProfileModel> profileModels = deviceTypeService.getDeviceTypeProfiles(deviceTypeId);
        return ResponseEntity.ok(ProfileMapper.toListModel(profileModels));
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @GetMapping("/{deviceTypeId}/profiles/{profileId}")
    public ResponseEntity getDeviceTypeProfile(@PathVariable(name = "deviceTypeId") final Long deviceTypeId,
                                                @PathVariable(name = "profileId") final Long profileId){
        return ResponseEntity.ok(deviceTypeService.getDeviceTypeProfile(deviceTypeId, profileId));
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @PostMapping("/{deviceTypeId}/profiles")
    public ResponseEntity saveDeviceTypeProfile(@PathVariable(name = "deviceTypeId") final Long deviceTypeId,
                                                @RequestBody @Valid ProfileModel profileModel,
                                                final BindingResult bindingResult){
        validatorUtil.checkBindingResultErrors(bindingResult);
        ProfileModel savedProfileModel = deviceTypeService.saveDeviceTypeProfile(deviceTypeId, profileModel);

        Map<String, Object> result = new HashMap<>();
        result.put("id", savedProfileModel.getId());

        return ResponseEntity.ok(result);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @PutMapping("/{deviceTypeId}/profiles/{profileId}")
    public ResponseEntity updateDeviceTypeProfile(@PathVariable(name = "deviceTypeId") final Long deviceTypeId,
                                                  @PathVariable(name = "profileId") final Long profileId,
                                                  @RequestBody @Valid ProfileModel profileModel, final BindingResult bindingResult){
        validatorUtil.checkBindingResultErrors(bindingResult);
        deviceTypeService.updateDeviceTypeProfile(deviceTypeId, profileId, profileModel);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER')")
    @DeleteMapping("/{deviceTypeId}/profiles/{profileId}")
    public ResponseEntity deleteDeviceTypeProfile(@PathVariable(name = "deviceTypeId") final Long deviceTypeId,
                                                  @PathVariable(name = "profileId") final Long profileId){
        deviceTypeService.deleteDeviceTypeProfile(deviceTypeId, profileId);
        return ResponseEntity.ok().build();
    }


}
