package io.netiot.devices.feign;

import io.netiot.devices.models.DataSampleModel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class IOServerEndpointsFallback implements IOServerEndpoints {

    @Override
    public List<DataSampleModel> getDeviceData(final Long deviceId, final Long millisFrom, final Long millisTo) {
        return new ArrayList<>();
    }

    @Override
    public Long getDeviceDataCount(final Long deviceId) {
        return 0L;
    }

}
