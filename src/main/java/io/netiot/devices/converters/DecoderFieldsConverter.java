package io.netiot.devices.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Converter
public class DecoderFieldsConverter implements AttributeConverter<List<String>, String> {

    private static final String STRING_DELIMITER = ",";

    @Override
    public String convertToDatabaseColumn(final List<String> fields) {
        return String.join(STRING_DELIMITER, fields);
    }

    @Override
    public List<String> convertToEntityAttribute(final String fields) {
        if(fields == null || fields.equals("")){
            return Collections.emptyList();
        }
        return Arrays.asList(fields.split(STRING_DELIMITER));
    }

}
