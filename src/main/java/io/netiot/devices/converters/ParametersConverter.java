package io.netiot.devices.converters;

import com.google.gson.Gson;
import io.netiot.devices.entities.Parameters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.HashMap;

@Converter
public class ParametersConverter implements AttributeConverter<Parameters, String> {

    private static final Gson gson = new Gson();

    @Override
    public String convertToDatabaseColumn(final Parameters parameters) {
        return gson.toJson(parameters.getParametersMap());
    }

    @Override
    public Parameters convertToEntityAttribute(final String parameters) {
        HashMap<String, String> parametersMap = gson.fromJson(parameters, HashMap.class);
        return Parameters.builder()
                .parametersMap(parametersMap)
                .build();
    }

}
