ALTER TABLE PROTOCOLS ADD COLUMN topic CHARACTER VARYING(50);

UPDATE PROTOCOLS SET topic = 'devices-lora' where name = 'LORA';
UPDATE PROTOCOLS SET topic = 'devices-http' where name = 'HTTP';