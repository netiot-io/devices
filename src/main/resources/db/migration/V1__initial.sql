CREATE SEQUENCE SEQ_PROTOCOL;

CREATE TABLE PROTOCOLS (
  id    BIGINT PRIMARY KEY NOT NULL,
  name  CHARACTER VARYING(20) UNIQUE NOT NULL,
  protocol_parameters TEXT,
  device_parameters TEXT
);

CREATE SEQUENCE SEQ_DECODER;

CREATE TABLE DECODERS (
  id                  BIGINT PRIMARY KEY NOT NULL,
  name                CHARACTER VARYING(20) UNIQUE NOT NULL,
  description         CHARACTER VARYING(400),
  code                TEXT NOT NULL,
  protocol_id         BIGINT NOT NULL,
  defined_by_platform BOOLEAN NOT NULL DEFAULT FALSE,
  organization_id     BIGINT,
  FOREIGN KEY (protocol_id) REFERENCES PROTOCOLS (id)
);

CREATE SEQUENCE SEQ_ORGANIZATION;

CREATE TABLE ORGANIZATIONS (
  id                BIGINT PRIMARY KEY NOT NULL,
  organization_id   BIGINT UNIQUE NOT NULL,
  organization_type CHARACTER VARYING(20) NOT NULL,
  created_by        BIGINT,
  deleted           BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE SEQUENCE SEQ_DEVICE_TYPE;

CREATE TABLE DEVICE_TYPES (
  id                  BIGINT PRIMARY KEY NOT NULL,
  name                CHARACTER VARYING(20) UNIQUE NOT NULL,
  description         CHARACTER VARYING(400),
  protocol_id         BIGINT NOT NULL,
  decoder_id          BIGINT NOT NULL,
  defined_by_platform BOOLEAN NOT NULL DEFAULT FALSE,
  organization_id     BIGINT,
  activate            BOOLEAN NOT NULL DEFAULT FALSE,
  FOREIGN KEY (decoder_id) REFERENCES DECODERS (id)
);

CREATE SEQUENCE SEQ_PHYSICAL_SENSOR;

CREATE TABLE PHYSICAL_SENSORS (
  id             BIGINT PRIMARY KEY NOT NULL,
  device_id      BIGINT UNIQUE NOT NULL,
  sensor_type_id BIGINT NOT NULL
);

CREATE SEQUENCE SEQ_PROFILE;

CREATE TABLE PROFILES (
  id                  BIGINT PRIMARY KEY NOT NULL,
  name                CHARACTER VARYING(100) NOT NULL,
  parameters          TEXT NOT NULL,
  device_type_id      BIGINT NOT NULL,
  organization_id     BIGINT,
  FOREIGN KEY (device_type_id) REFERENCES DEVICE_TYPES (id)
);

CREATE SEQUENCE SEQ_DEVICE;

CREATE TABLE DEVICES (
  id                      BIGINT PRIMARY KEY NOT NULL,
  name                    CHARACTER VARYING(100) NOT NULL,
  device_parameters       TEXT NOT NULL,
  device_type_id          BIGINT NOT NULL,
  profile_id              BIGINT NOT NULL,
  organization_id         BIGINT NOT NULL,
  created_by_organization BIGINT NOT NULL,
  activate                BOOLEAN NOT NULL DEFAULT FALSE,
  FOREIGN KEY (device_type_id) REFERENCES DEVICE_TYPES (id),
  FOREIGN KEY (profile_id) REFERENCES PROFILES (id)
);

CREATE SEQUENCE SEQ_ALERT;

CREATE TABLE ALERTS (
  id        BIGINT PRIMARY KEY NOT NULL,
  device_id BIGINT NOT NULL,
  code      CHARACTER VARYING(100) NOT NULL,
  timestamp TIMESTAMP NOT NULL
);

INSERT INTO PROTOCOLS(id, name) VALUES (1,'LORA');
INSERT INTO PROTOCOLS(id, name) VALUES (2,'HTTP');

UPDATE PROTOCOLS SET protocol_parameters = '{
  parameterModelList : [
    {id: "loraClass", name: "LORA class", required: "true", value: [
         {name: "A"}, {name: "B"}, {name: "C"}
       ]
     },
     {id: "activationMethod", name: "Activation method", required: "true", value: [
         {name: "ABP", extraFields: [
             {id:"devAddr", name: "DevAddr", required: "true"}, {id: "nwksKey", name: "NwkSKey", required: "true"},
             {id: "appsKey", name: "AppSKey", required: "true"}
           ]},
         {name: "OTAA", extraFields: [
             {id: "appEui", name: "AppEUI", required: "true"}, {id: "appKey", name: "AppKey", required: "true"}
           ]
         }
       ]
     }
  ]
}', device_parameters = '{parameterModelList:[{id: "devEui", name: "DevEUI", required: "true"}]}' where name = 'LORA';
ALTER SEQUENCE SEQ_PROTOCOL RESTART WITH 3;

INSERT INTO DECODERS(id, name, description, code, protocol_id, defined_by_platform, organization_id) VALUES (1, 'dummy public', 'dummy decoder', 'function decode(payload) {
  // decode a payload/array of bytes and return a json with your data

  /*
  * You MUST return an object that respects the formats defined in this document: https://www.google.com
  */
  let decodedPayload = {};
  //TODO: add your custom code
  return decodedPayload;
}', 1, true, 1);

INSERT INTO DECODERS(id, name, description, code, protocol_id, defined_by_platform, organization_id) VALUES (2, 'dummy private', 'dummy decoder', 'function decode(payload) {
  // decode a payload/array of bytes and return a json with your data

  /*
  * You MUST return an object that respects the formats defined in this document: https://www.google.com
  */
  let decodedPayload = {};
  //TODO: add your custom code
  return decodedPayload;
}', 1, false, 5);
ALTER SEQUENCE SEQ_DECODER RESTART WITH 3;

INSERT INTO DEVICE_TYPES (id, name, description, protocol_id, decoder_id, defined_by_platform, organization_id, activate)
VALUES (1, 'dummy', 'test desc', 1, 1, TRUE, 1, TRUE);

ALTER SEQUENCE SEQ_DEVICE_TYPE RESTART WITH 2;