FROM anapsix/alpine-java:8
ADD target/devices.jar devices.jar
ENV JAVA_OPTS=""
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /devices.jar
