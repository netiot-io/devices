# Devices
Microservice for managing the devices.

## Known paths
### alerts
`GET devices/V1/alerts/{deviceId}/last-10-alerts`

Return
```json
[
  {
    "id": 1, 
    "deviceId": 2,
    "code": "",
    "timestamp:": 1571209858597
  }
]
```

### decoders
`GET devices/V1/decoders?protocolId=`

Return
```json
[
  {
    "id": 1,
    "name": "",
    "description": "",
    "protocol_id": 2,
    "code": "",
    "defined_by_platform": true,
    "organization_id": 3
  }
]
```

`GET devices/V1/decoders/{decoderId}`

Return
```json
[
  {
    "id": 1,
    "name": "",
    "description": "",
    "protocol_id": 2,
    "code": "",
    "defined_by_platform": true,
    "organization_id": 3
  }
]
```

`POST devices/V1/decoders`

Body
```json
{
  "name": "",
  "description": "",
  "protocol_id": 2,
  "code": "",
  "defined_by_platform": true,
  "organization_id": 3
}
```

`PUT devices/V1/decoders/{decoderId}`

Body
```json
{
  "name": "",
  "description": "",
  "protocol_id": 2,
  "code": "",
  "defined_by_platform": true,
  "organization_id": 3
}
```

`DELETE devices/V1/decoders/{decoderId}`

### devices
`GET devices/V1/devices`

Return
```json
[
  {
    "id": 1,
    "name": "",
    "device_parameters": {},
    "device_type_id": 2,
    "profile_id": 3,
    "user_ids": [],
    "activate": true
  }
]
```

`GET devices/V1/devices/{deviceId}`

Return
```json
{
"id": 1,
"name": "",
"device_parameters": {},
"device_type_id": 2,
"profile_id": 3,
"user_ids": [],
"activate": true
}
```

`GET devices/V1/devices/device-data?device-id=&millis-from=&millis-to=`

Return
```json
[
  {
    "id": 1,
    "timestamp": 1571210554317,
    "timestampR": 1571210554318,
    "values": ""
  }
]
```

`GET devices/V1/devices/{deviceId}/device-data-count`

Return: long

`GET device/V1/devices/internal` - get user devices

Return
```json
[
  {
    "id": 1,
    "name": "",
    "decoder_fields": "",
    "device_type_id": 2,
    "device_type_name": ""
  }
]
```

`GET device/V1/devices/internal/{deviceId}` - get device name

Return: string

`GET device/V1/devices/internal/{deviceUUID}/id` - get device id

Return: long

`GET device/V1/devices/internal/{deviceId}/topic` - get device topic

Return: string

`POST devices/V1/devices`

Body
```json
[
  {
    "name": "",
    "device_parameters": {},
    "device_type_id": 2,
    "profile_id": 3,
    "user_ids": [],
    "activate": true
  }
]
```

`PUT devices/V1/devices/{deviceId}`

Body
```json
[
  {
    "name": "",
    "device_parameters": {},
    "device_type_id": 2,
    "profile_id": 3,
    "user_ids": [],
    "activate": true
  }
]
```

`DELETE devices/V1/devices/{deviceId}`

### devices type
`GET devices/V1/device-types?protocolId=`

Return
```json
[
  {
    "id": 1,
    "name": "",
    "description": "",
    "prtocol_id": 2,
    "decoder_id": 3,
    "defined_by_platform": true,
    "organization_id": 4,
    "activate": true
  }
]
```

`GET devices/V1/device-types/{deviceTypeId}`

Return
```json
{
    "id": 1,
    "name": "",
    "description": "",
    "prtocol_id": 2,
    "decoder_id": 3,
    "defined_by_platform": true,
    "organization_id": 4,
    "activate": true
}
```

`GET devices/V1/device-types/{deviceTypeId}/profiles`

Return
```json
[
    {
        "id": 1,
        "name": "",
        "protocol_parameters": {},
        "organization_id": 4
    }
]
```

`GET devices/V1/device-types/{deviceTypeId}/profiles/{profileId}`

Return
```json
{
    "id": 1,
    "name": "",
    "protocol_parameters": {},
    "organization_id": 4
}
```

`POST devices/V1/device-types`

Body
```json
[
  {
    "name": "",
    "description": "",
    "prtocol_id": 2,
    "decoder_id": 3,
    "defined_by_platform": true,
    "organization_id": 4,
    "activate": true
  }
]
```

`POST devices/V1/device-types/{deviceTypeId}/profiles`

Body
```json
{
    "name": "",
    "protocol_parameters": {},
    "organization_id": 4
}
```

`PUT devices/V1/device-types/{deviceTypeId}`

Body
```json
[
  {
    "name": "",
    "description": "",
    "prtocol_id": 2,
    "decoder_id": 3,
    "defined_by_platform": true,
    "organization_id": 4,
    "activate": true
  }
]
```

`PUT devices/V1/device-types/{deviceTypeId}/profiles/{profileId}`

Body
```json
{
    "name": "",
    "protocol_parameters": {},
    "organization_id": 4
}
```

`DELETE devices/V1/device-types/{deviceTypeId}`

`DELETE devices/V1/device-types/{deviceTypeId}/profiles/{profileId}`

### protocols
`GET devices/V1/protocols`

Return
```json
[
  {
    "id": 1,
    "name": "",
    "protocol_parameters": [
      {
        "id": 2,
        "name": "",
        "required": true,
        "disabled": true,
        "generate": true,
        "value": [
          {
            "name": "",
            "extra_fields": [
              {
                "id": 3,
                "name": "",
                "required": true,
                "disabled": true,
                "generate": true
              }
            ]
          }
        ]
      }   
    ],
    "device_parameters": [
      {
        "id": 2,
        "name": "",
        "required": true,
        "disabled": true,
        "generate": true,
        "value": [
          {
            "name": "",
            "extra_fields": [
              {
                "id": 3,
                "name": "",
                "required": true,
                "disabled": true,
                "generate": true
              }
            ]
          }
        ]
      }   
    ],  
    "metadata": "" 
  }
]
```

## DB
Database migration implemented with [flyway](https://flywaydb.org/)
Scrips in `resources/db/migration`.

## Feign
Communicates with the `io-server` microservice to get data and data count for devices.

`GET io-server/V1/data/device-data?deviceId=&millisFrom=&millisTo=`

Return
```json
[
  {
    "id": 1,
    "timestamp": 1571210554317,
    "timestampR": 1571210554318,
    "values": ""
  }
]
```

`GET io-server/V1/data/{deviceId}/device-data-count`

Return: long

Communicates with the `users-and-organizations` microservice to get the family (all the user's parents) of users for 
each given user id.

`GET users-and-organizations/V1/user/internal/device-users`

Return
```json
[
  {"id": 1}
]
```

## Kafka
Listens for kafka messages from the `decoder` microservice. The `decoder` sends alerts whenever something goes wrong.

## CI/CD
Compile, test, package, build docker image and push to gitlab docker registry on the topic from the config. 

## Tests
Groovy tests in `src/test/groovy`.